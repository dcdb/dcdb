%global ver %(git describe --tags 2>/dev/null|sed -r 's/([0-9]\\.[0-9]*)-([0-9]*)-.*/\\1.\\2/')

Name: dcdb-operators
Summary: DCDB operator plugins
Version: %{ver}
Release: 1

License: GPLv2+
URL: https://dcdb.it/
Source: https://gitlab.lrz.de/dcdb/dcdb/tree/master/dcdbpusher
Packager: DCDB project <info@dcdb.it>

BuildRequires: systemd-rpm-macros
BuildRequires: patchelf
Requires: dcdb-base

%global __requires_exclude_from ^%{_libdir}/dcdb/libdcdboperator_.*$

%description
DCDB operator plugins for dcdbpusher and collectagent includen OpenCV dependencies

%if 0%{getenv:DCDB_DEPLOYPATH}
%global deploypath %{getenv:DCDB_DEPLOYPATH}  
%else
%global deploypath ../../../install
%endif


%prep
#nothing to prepare

%build
#nothing to build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}/dcdb
mkdir -p %{buildroot}%{_libdir}/dcdb
mkdir -p %{buildroot}%{_bindir}
install -p -m 755 %{deploypath}/lib/libdcdboperator_*.so %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libopencv_core.so.4.1 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libopencv_ml.so.4.1 %{buildroot}%{_libdir}/dcdb
for f in  %{buildroot}%{_libdir}/dcdb/lib*; do patchelf --set-rpath "\$ORIGIN" $f; done
for f in  %{buildroot}%{_libdir}/dcdb/libdcdboperator_*.so; do install -p -m 644 %{deploypath}/etc/$(echo "${f##*/}"|sed 's/libdcdboperator_\([^\.]*\)\.so/\1.conf/') %{buildroot}%{_sysconfdir}/dcdb; done

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_libdir}/dcdb/libdcdboperator_*.so
%{_libdir}/dcdb/libopencv_core.so.4.1
%{_libdir}/dcdb/libopencv_ml.so.4.1

%config(noreplace) %{_sysconfdir}/dcdb/aggregator.conf
%config(noreplace) %{_sysconfdir}/dcdb/smoothing.conf
%config(noreplace) %{_sysconfdir}/dcdb/regressor.conf
%config(noreplace) %{_sysconfdir}/dcdb/classifier.conf
%config(noreplace) %{_sysconfdir}/dcdb/clustering.conf
%config(noreplace) %{_sysconfdir}/dcdb/cssignatures.conf
%config(noreplace) %{_sysconfdir}/dcdb/job_aggregator.conf
%config(noreplace) %{_sysconfdir}/dcdb/testeroperator.conf
%config(noreplace) %{_sysconfdir}/dcdb/filesink.conf
%config(noreplace) %{_sysconfdir}/dcdb/healthchecker.conf
%config(noreplace) %{_sysconfdir}/dcdb/jsonexporter.conf

%changelog
* Thu Mar 09 2023 Michael Ott <ott@lrz.de> 0.5
- Initial release
