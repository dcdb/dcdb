%global ver %(git describe --tags 2>/dev/null|sed -r 's/([0-9]\\.[0-9]*)-([0-9]*)-.*/\\1.\\2/')

Name: dcdb-server
Summary: Server components of the DCDB project
Version: %{ver}
Release: 1

License: GPLv2+
URL: https://dcdb.it/
Packager: DCDB project <info@dcdb.it>

Requires: dcdb-base

BuildRequires: systemd-rpm-macros
BuildRequires: sed

%description
DCDB CollectAgent and GrafanaServer components

%if 0%{getenv:DCDB_DEPLOYPATH}
%global deploypath %{getenv:DCDB_DEPLOYPATH}  
%else
%global deploypath ../../../install
%endif


%prep
#nothing to prepare

%build
#nothing to build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/dcdb
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_sysconfdir}/dcdb
install -p -m 755 %{deploypath}/bin/collectagent %{buildroot}%{_bindir}
patchelf --set-rpath "%{_libdir}/dcdb" %{buildroot}%{_bindir}/collectagent
install -p -m 755 %{deploypath}/bin/grafanaserver %{buildroot}%{_bindir}
patchelf --set-rpath "%{_libdir}/dcdb" %{buildroot}%{_bindir}/grafanaserver
install -p -m 644 %{deploypath}/etc/collectagent.conf %{buildroot}%{_sysconfdir}/dcdb
install -p -m 644 %{deploypath}/etc/grafana.conf %{buildroot}%{_sysconfdir}/dcdb
install -p -m 644 %{_sourcedir}/collectagent.service %{buildroot}%{_unitdir}
install -p -m 644 %{_sourcedir}/dcdbgrafana.service %{buildroot}%{_unitdir}

%post
%systemd_post collectagent.service
%systemd_post dcdbgrafana.service

%clean
rm -rf %{buildroot}

%preun
%systemd_preun collectagent.service
%systemd_preun dcdbgrafana.service

%postun
%systemd_postun_with_restart collectagent.service
%systemd_postun_with_restart dcdbgrafana.service

%files
%defattr(-,root,root)
%{_bindir}/collectagent
%{_bindir}/grafanaserver
%{_unitdir}/collectagent.service
%{_unitdir}/dcdbgrafana.service

%config(noreplace) %{_sysconfdir}/dcdb/collectagent.conf
%config(noreplace) %{_sysconfdir}/dcdb/grafana.conf

%changelog
* Thu Mar 09 2023 Michael Ott <ott@lrz.de> 0.5
- Initial release
