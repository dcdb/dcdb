%global ver %(git describe --tags 2>/dev/null|sed -r 's/([0-9]\\.[0-9]*)-([0-9]*)-.*/\\1.\\2/')

Name: dcdb-base
Summary: Basic DCDB libraries and tools
Version: %{ver}
Release: 1

License: GPLv2+
URL: https://dcdb.it/
Source: https://gitlab.lrz.de/dcdb/dcdb/tree/master/dcdbpusher
Packager: DCDB project <info@dcdb.it>

BuildRequires: systemd-rpm-macros
BuildRequires: patchelf

%description
DCDB command line tools, libdcdb and 3rd party libraries required by other DCDB packages.

%if 0%{getenv:DCDB_DEPLOYPATH}
%global deploypath %{getenv:DCDB_DEPLOYPATH}  
%else
%global deploypath ../../../install
%endif

%prep
#nothing to prepare

%build
#nothing to build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/dcdb
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_sysconfdir}/dcdb
mkdir -p %{buildroot}%{_sysconfdir}/profile.d
install -p -m 755 %{deploypath}/bin/dcdbconfig %{buildroot}%{_bindir}
install -p -m 755 %{deploypath}/bin/dcdbquery %{buildroot}%{_bindir}
install -p -m 755 %{deploypath}/bin/dcdbslurmjob %{buildroot}%{_bindir}
for f in  %{buildroot}%{_bindir}/dcdb*; do patchelf --set-rpath "%{_libdir}/dcdb" $f; done
install -p -m 644 %{deploypath}/etc/ca-cert.pem %{buildroot}%{_sysconfdir}/dcdb
install -p -m 644 %{deploypath}/etc/ca-key.pem %{buildroot}%{_sysconfdir}/dcdb
install -p -m 644 %{_sourcedir}/dcdb.sh %{buildroot}%{_sysconfdir}/profile.d
install -p -m 755 %{deploypath}/lib/libdcdb.so %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_atomic.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_chrono.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_date_time.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_filesystem.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_log.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_random.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_log_setup.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_regex.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_system.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libboost_thread.so.1.83.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libmosquitto.so.1 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libscylla-cpp-driver.so.2 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libuv.so.1 %{buildroot}%{_libdir}/dcdb
for f in  %{buildroot}%{_libdir}/dcdb/lib*; do patchelf --set-rpath "\$ORIGIN" $f; done

%post
%systemd_post dcdbpusher.service

%clean
rm -rf %{buildroot}

%preun
%systemd_preun dcdbpusher.service

%postun
%systemd_postun_with_restart dcdbpusher.service

%files
%defattr(-,root,root)
%{_bindir}/dcdb*
%{_libdir}/dcdb/libdcdb.so
%{_libdir}/dcdb/libboost_*.so.1.83.0
%{_libdir}/dcdb/libmosquitto.so.1
%{_libdir}/dcdb/libscylla-cpp-driver.so.2
%{_libdir}/dcdb/libuv.so.1

%config(noreplace) %{_sysconfdir}/dcdb/ca-cert.pem
%config(noreplace) %{_sysconfdir}/dcdb/ca-key.pem
%config(noreplace) %{_sysconfdir}/profile.d/dcdb.sh

%changelog
* Thu Mar 09 2023 Michael Ott <ott@lrz.de> 0.5
- Initial release
