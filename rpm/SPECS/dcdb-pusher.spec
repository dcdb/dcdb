%global ver %(git describe --tags 2>/dev/null|sed -r 's/([0-9]\\.[0-9]*)-([0-9]*)-.*/\\1.\\2/')

Name: dcdb-pusher
Summary: Pusher component of the DCDB project
Version: %{ver}
Release: 1

License: GPLv2+
URL: https://dcdb.it/
Source: https://gitlab.lrz.de/dcdb/dcdb/tree/master/dcdbpusher
Packager: DCDB project <info@dcdb.it>

Requires: dcdb-base
BuildRequires: systemd-rpm-macros

%global __requires_exclude_from ^%{_libdir}/dcdb/libdcdbplugin_.*$

%description
Pusher is the data acquisition component for the DataCenter Data Base (DCDB)
project. DCDB is a holistic monitoring solution for HPC environments. This
package bundles the Pusher framework binary as well as the following plugin
libraries: sysfs, perfevent, ipmi, pdu, bacnet, snmp, procfs, tester, gpfsmon,
opa, msr.

More information, source code, and the full DCDB project can be found at
 https://dcdb.it


%if 0%{getenv:DCDB_DEPLOYPATH}
%global deploypath %{getenv:DCDB_DEPLOYPATH}  
%else
%global deploypath ../../../install
%endif


%prep
#nothing to prepare

%build
#nothing to build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/dcdb
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_sysconfdir}/dcdb
install -p -m 755 %{deploypath}/bin/dcdbpusher %{buildroot}%{_bindir}
for f in  %{buildroot}%{_bindir}/dcdb*; do patchelf --set-rpath "%{_libdir}/dcdb" $f; done
install -p -m 755 %{_sourcedir}/dcdbpusher.sh %{buildroot}%{_bindir}
install -p -m 644 %{deploypath}/etc/dcdbpusher.conf %{buildroot}%{_sysconfdir}/dcdb
install -p -m 644 %{_sourcedir}/dcdbpusher.service %{buildroot}%{_unitdir}
for l in %{deploypath}/lib/libdcdbplugin_*.so; do
  p=$(echo $l|sed 's/.*libdcdbplugin_\([^.]*\)\.so/\1/')
  install -p -m 755 $l %{buildroot}%{_libdir}/dcdb
  patchelf --set-rpath "\$ORIGIN" %{buildroot}%{_libdir}/dcdb/libdcdbplugin_${p}.so
  install -p -m 644 %{deploypath}/etc/${p}.conf %{buildroot}%{_sysconfdir}/dcdb
done
install -p -m 755 %{deploypath}/lib/libfreeipmi.so.17 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libgcrypt.so.20 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libgpg-error.so.0 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libnetsnmpagent.so.35 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libnetsnmp.so.35 %{buildroot}%{_libdir}/dcdb
install -p -m 755 %{deploypath}/lib/libsnap7.so %{buildroot}%{_libdir}/dcdb

%post
%systemd_post dcdbpusher.service

%clean
rm -rf %{buildroot}

%preun
%systemd_preun dcdbpusher.service

%postun
%systemd_postun_with_restart dcdbpusher.service

%files
%defattr(-,root,root)
%{_bindir}/dcdbpusher
%{_bindir}/dcdbpusher.sh
%{_unitdir}/dcdbpusher.service
%{_libdir}/dcdb/libdcdbplugin*.so
%{_libdir}/dcdb/libfreeipmi.so.17
%{_libdir}/dcdb/libgcrypt.so.20
%{_libdir}/dcdb/libgpg-error.so.0
%{_libdir}/dcdb/libnetsnmpagent.so.35
%{_libdir}/dcdb/libnetsnmp.so.35
%{_libdir}/dcdb/libsnap7.so

%config(noreplace) %{_sysconfdir}/dcdb/dcdbpusher.conf
%config(noreplace) %{_sysconfdir}/dcdb/sysfs.conf
%config(noreplace) %{_sysconfdir}/dcdb/ipmi.conf
%config(noreplace) %{_sysconfdir}/dcdb/pdu.conf
%config(noreplace) %{_sysconfdir}/dcdb/bacnet.conf
%config(noreplace) %{_sysconfdir}/dcdb/snmp.conf
%config(noreplace) %{_sysconfdir}/dcdb/procfs.conf
%config(noreplace) %{_sysconfdir}/dcdb/tester.conf
%config(noreplace) %{_sysconfdir}/dcdb/gpfsmon.conf
%config(noreplace) %{_sysconfdir}/dcdb/msr.conf
%config(noreplace) %{_sysconfdir}/dcdb/rest.conf
%config(noreplace) %{_sysconfdir}/dcdb/s7plc.conf
%config(noreplace) %{_sysconfdir}/dcdb/perfevent.conf
%config(noreplace) %{_sysconfdir}/dcdb/nvml.conf
%config(noreplace) %{_sysconfdir}/dcdb/likwid.conf
%config(noreplace) %{_sysconfdir}/dcdb/modbus.conf
%config(noreplace) %{_sysconfdir}/dcdb/variorum.conf


%changelog
* Thu Mar 09 2023 Michael Ott <ott@lrz.de> 0.5
- Change install path to /usr and use patchelf to change rpath of libraries
* Tue Dec 10 2019 Micha Mueller <micha.mueller@lrz.de> 0.3-1
- Initial release
