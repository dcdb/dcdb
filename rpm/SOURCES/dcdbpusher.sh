#! /bin/bash

DCDB_CONFIG_DIR=${DCDB_CONFIG_DIR:-/etc/dcdb}
if [ -f "$DCDB_CONFIG_DIR/dcdbpusher.sh" ]; then
  source "$DCDB_CONFIG_DIR/dcdbpusher.sh"
fi

DCDBPUSHER_CONFIG=${DCDBPUSHER_CONFIG:-$DCDB_CONFIG_DIR}
if [ "$BROKER" != "" ]; then
  DCDBPUSHER_OPTIONS+=" -b $BROKER"
fi
if [ "$MQTT_PREFIX" != "" ]; then
  DCDBPUSHER_OPTIONS+=" -m $MQTT_PREFIX"
fi

exec dcdbpusher $DCDBPUSHER_OPTIONS $@ $DCDBPUSHER_CONFIG
