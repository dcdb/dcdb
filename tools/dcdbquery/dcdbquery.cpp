//================================================================================
// Name        : dcdbquery.cpp
// Author      : Axel Auweter, Daniele Tafani
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Main file of the dcdbquery command line utility
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2011-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

/* C++ standard headers */
#include <iostream>
#include <stdexcept>

/* C standard headers */
#include <cstdlib>
#include <cstring>
#include <unistd.h>

/* Custom headers */
#include "dcdb/timestamp.h"
#include "dcdb/version.h"
#include "globalconfiguration.h"
#include "query.h"
#include "version.h"

void usage(void) {
	if (isatty(fileno(stdin))) {
		/*            0---------1---------2---------3---------4---------5---------6---------7--------- */
		std::cout << "Usage:" << std::endl;
		std::cout << "  dcdbquery [-h<host>] [-u<username>] [-p<password>] [-r] [-l] [-t] [-s] [-i] <Sensor 1> "
			     "[<Sensor 2> ...] [<Start> <End>]"
			  << std::endl;
		std::cout << "  dcdbquery [-h<host>] [-u<username>] [-p<password>] [-r] [-l] [-t]  [-s] [-i] -j "
			     "<jobId> <Sensor 1> [<Sensor 2> ...]"
			  << std::endl;
		std::cout << std::endl;
		std::cout << "Parameters:" << std::endl;
		std::cout << "  <jobId>       a job to query sensors for" << std::endl;
		std::cout << "  <Sensor n>    a sensor name" << std::endl;
		std::cout << "  <Start>       start of time series" << std::endl;
		std::cout << "  <End>         end of time series" << std::endl;
		std::cout << std::endl;
		std::cout << "Options:" << std::endl;
		std::cout << "  -h<host>      Cassandra host                         [default: " << DEFAULT_CASSANDRAHOST << ":" << DEFAULT_CASSANDRAPORT << "]"
			  << endl;
		std::cout << "  -u<username>  Cassandra username                     [default: none]" << std::endl;
		std::cout << "  -p<password>  Cassandra password                     [default: none]" << std::endl;
		std::cout << "  -r            Report timestamps in numerical format" << std::endl;
		std::cout << "  -l            Report times in local time (not UTC)   [default]" << std::endl;
		std::cout << "  -t            Report times in UTC time" << std::endl;
		std::cout << "  -s            Strict sensor name checking" << std::endl;
		std::cout << "  -i            Ignore meta data" << std::endl;
	} else {
		std::cout << "Invalid request." << std::endl;
	}
	exit(EXIT_SUCCESS);
}

std::string getEnv(const char *var) {
	char *str = std::getenv(var);
	if (str != NULL) {
		return std::string(str);
	} else {
		return std::string("");
	}
}

int main(int argc, char *const argv[]) {
	std::cout << "dcdbquery " << VERSION << " (libdcdb " << DCDB::Version::getVersion() << ")" << std::endl << std::endl;

	if (argc <= 1) {
		usage();
	}

	/* Allocate our lovely helper class */
	DCDBQuery *myQuery;
	myQuery = new DCDBQuery();

	/* Get the options */
	int         ret;
	std::string jobId;

	std::string host = getEnv("DCDB_HOSTNAME") != "" ? getEnv("DCDB_HOSTNAME") : "localhost";
	std::string username = getEnv("DCDB_USERNAME");
	std::string password = getEnv("DCDB_PASSWORD");
	std::string hostname;
	uint16_t    port;

	while ((ret = getopt(argc, argv, "+h:rltsij:u:p:")) != -1) {
		switch (ret) {
			case 'h':
				host = optarg;
				break;
			case 'u':
				username = optarg;
				break;
			case 'p': {
				password = optarg;
				// What does this do? Mask the password?
				size_t pwdLen = strlen(optarg);
				memset(optarg, 'x', (pwdLen >= 3) ? 3 : pwdLen);
				if (pwdLen > 3) {
					memset(optarg + 3, 0, pwdLen - 3);
				}
				break;
			}
			case 'r':
				myQuery->setRawOutputEnabled(true);
				break;
			case 'l':
				myQuery->setLocalTimeEnabled(true);
				break;
			case 't':
				myQuery->setLocalTimeEnabled(false);
				break;
			case 's':
				myQuery->setStrictSensorCheckEnabled(true);
				break;
			case 'i':
				myQuery->setIgnoreMetadataEnabled(true);
				break;
			case 'j':
				jobId = optarg;
				break;
			default:
				usage();
				exit(EXIT_FAILURE);
		}
	}

	// Hide all option parameters and name of binary
	argv = &argv[optind];
	argc -= optind;

	// Parsing host string
	hostname = parseNetworkHost(host);
	port = atoi(parseNetworkPort(host).c_str());
	if (port == 0) {
		port = atoi(DEFAULT_CASSANDRAPORT);
	}

	/* Try to create TimeStamp objects from the arguments */
	DCDB::TimeStamp start, end;
	if (jobId.size() == 0) {
		if (argc >= 1) {
			bool local = myQuery->getLocalTimeEnabled();
			try {
				end = DCDB::TimeStamp(argv[argc - 1], local);
			} catch (std::exception &e) {
				// The last parameter could not be parsed as timestamp, let's assume it's a sensor and
				// do a fuzzy query
				end = start;
			}
			if (argc > 2) {
				if (start != end) {
					if (std::string(argv[argc - 2]) == std::string(argv[argc - 1])) {
						start = end;
					} else {
						try {
							start = DCDB::TimeStamp(argv[argc - 2], local);
						} catch (std::exception &e) {
							std::cout << "Error parsing start timestamp (" << argv[argc - 2] << "): " << e.what() << std::endl;
							exit(EXIT_FAILURE);
						}
					}
					argc -= 2;
				}
			}

			/* Ensure start < end */
			if (start > end) {
				std::cout << "Start time must be earlier than end time." << std::endl;
				exit(EXIT_FAILURE);
			}
		}
	}

	/* Build a list of sensornames */
	std::list<std::string> sensors;
	for (int arg = 0; arg < argc; arg++) {
		sensors.push_back(argv[arg]);
	}

	if (myQuery->connect(hostname, port, username, password) == 0) {
		if (jobId.size() == 0) {
			myQuery->doQuery(sensors, start, end);
		} else {
			myQuery->dojobQuery(sensors, jobId);
		}
		myQuery->disconnect();
	}

	delete myQuery;

	return 0;
}
