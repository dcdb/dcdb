//================================================================================
// Name        : query.cpp
// Author      : Axel Auweter, Daniele Tafani
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Implementation of query class of dcdbquery
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2011-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include <algorithm>
#include <iostream>
#include <list>
#include <string>

#include <cinttypes>
#include <cstdlib>

#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>

#include "dcdb/sensoroperations.h"
#include "dcdbendian.h"
#include "query.h"
#include <dcdb/jobdatastore.h>

void DCDBQuery::setLocalTimeEnabled(bool enable) {
	useLocalTime = enable;
}

bool DCDBQuery::getLocalTimeEnabled() {
	return useLocalTime;
}

void DCDBQuery::setRawOutputEnabled(bool enable) {
	useRawOutput = enable;
}

bool DCDBQuery::getRawOutputEnabled() {
	return useRawOutput;
}

void DCDBQuery::setStrictSensorCheckEnabled(bool enable) {
	strictSensorCheck = enable;
}

bool DCDBQuery::getStrictSensorCheckEnabled() {
	return strictSensorCheck;
}

void DCDBQuery::setIgnoreMetadataEnabled(bool enable) {
	ignoreMetadata = enable;
}

bool DCDBQuery::getIgnoreMetadataEnabled() {
	return ignoreMetadata;
}

int DCDBQuery::connect(std::string hostname, uint16_t port, std::string username, std::string password) {
	if (connection != nullptr) {
		return 0;
	}
	connection = new DCDB::Connection(hostname, port, username, password);
	if (!connection->connect()) {
		std::cout << "Cannot connect to database." << std::endl;
		return 1;
	}
	return 0;
}

void DCDBQuery::disconnect() {
	connection->disconnect();
	delete connection;
	connection = nullptr;
}

bool scaleAndConvert(int64_t &value, double baseScalingFactor, double scalingFactor, DCDB::Unit baseUnit, DCDB::Unit unit) {
	if (scalingFactor != 1.0 || baseScalingFactor != 1.0) {
		if (DCDB::scale(&value, scalingFactor, baseScalingFactor) == DCDB::DCDB_OP_OVERFLOW)
			return false;
	}

	/* Convert the unit if requested */
	if ((unit != DCDB::Unit_None) && (unit != baseUnit)) {
		if (!DCDB::UnitConv::convert(value, baseUnit, unit)) {
			std::cerr << "Warning, cannot convert units (" << DCDB::UnitConv::toString(baseUnit) << " -> " << DCDB::UnitConv::toString(unit) << ")"
				  << std::endl;
			return false;
		}
	}

	return true;
}

void DCDBQuery::genOutputAggregates(std::list<queryReadingsPair_t> &results) {
	/* The results list has to be organized such that it only contains readings for the same base sensor
	 * The timestamps for the readings must be the same for each queryConfig
	 */

	/* Loop over all queries and print header */
	std::cout << "Sensor,Time";
	for (auto r : results) {
		queryConfig_t q = r.first.front();
		switch (q.aggregate) {
			case DCDB::AGGREGATE_MIN:
				std::cout << ",min";
				break;
			case DCDB::AGGREGATE_MAX:
				std::cout << ",max";
				break;
			case DCDB::AGGREGATE_AVG:
				std::cout << ",avg";
				break;
			case DCDB::AGGREGATE_SUM:
				std::cout << ",sum";
				break;
			case DCDB::AGGREGATE_COUNT:
				std::cout << ",count";
				break;
			default:
				break;
		}

		std::string unitStr;
		if (q.unit != DCDB::Unit_None) {
			unitStr = DCDB::UnitConv::toString(q.unit);
		} else if (baseUnit != DCDB::Unit_None) {
			unitStr = DCDB::UnitConv::toString(baseUnit);
		}
		if (unitStr.size() > 0) {
			std::cout << " (" << unitStr << ")";
		}
	}

	/* Now loop over all readings */
	while (!results.front().second.empty()) {
		/* Use the first result to get sensor name and timestamp */
		DCDB::SensorDataStoreReading reading = results.front().second.front();
		std::cout << std::endl << reading.sensorId.getId() << ",";

		if (useRawOutput) {
			std::cout << reading.timeStamp.getRaw();
		} else {
			std::cout << reading.timeStamp.getString(useLocalTime);
		}
		/* Now print the first reading in each result set */
		for (auto &r : results) {
			queryConfig_t query = r.first.front();
			int64_t       value = r.second.front().value;

			DCDB::Unit unit;
			if (query.unit != DCDB::Unit_None) {
				unit = query.unit;
			} else {
				unit = baseUnit;
			}

			if (scaleAndConvert(value, baseScalingFactor, query.scalingFactor, baseUnit, unit)) {
				std::cout << "," << value;
			} else {
				std::cout << ",";
			}

			/* Remove the first reading in the list */
			r.second.erase(r.second.begin());
		}
		std::cout << std::endl;
	}
}

void DCDBQuery::genOutputOperators(std::list<queryReadingsPair_t> &results) {
	/* Loop over all queries and print header */
	std::cout << "Sensor,Time";
	for (auto r : results) {
		for (auto q : r.first) {
			switch (q.operation) {
				case DCDB_OP_NONE:
				case DCDB_OP_WINTERMUTE:
					std::cout << ",Value";
					break;
				case DCDB_OP_DELTA:
					std::cout << ",Delta";
					break;
				case DCDB_OP_DELTAT:
					std::cout << ",Delta_t";
					break;
				case DCDB_OP_DERIVATIVE:
					std::cout << ",Derivative";
					break;
				case DCDB_OP_INTEGRAL:
					std::cout << ",Integral";
					break;
				case DCDB_OP_RATE:
					std::cout << ",Rate";
					break;
				default:
					break;
			}

			std::string unitStr;
			if (q.unit != DCDB::Unit_None) {
				unitStr = DCDB::UnitConv::toString(q.unit);
			} else if (baseUnit != DCDB::Unit_None) {
				unitStr = DCDB::UnitConv::toString(baseUnit);
			}
			if (q.operation == DCDB_OP_DERIVATIVE) {
				if ((unitStr.back() == 's') || (unitStr.back() == 'h')) {
					unitStr.pop_back();
				} else if (unitStr.back() == 'J') {
					unitStr.pop_back();
					unitStr.append("W");
				}
			} else if (q.operation == DCDB_OP_INTEGRAL) {
				if (unitStr.back() == 'W') {
					unitStr.append("s");
				}
			}
			if (unitStr.size() > 0) {
				std::cout << " (" << unitStr << ")";
			}
		}
	}

	int64_t         prevReading = 0;
	DCDB::TimeStamp prevT((uint64_t)0);
	/* Now loop over all readings */
	for (auto r : results) {
		for (auto reading : r.second) {
			DCDB::TimeStamp ts = reading.timeStamp;

			/* Print the sensor's public name */
			std::cout << std::endl << reading.sensorId.getId() << ",";

			/* Print the time stamp */
			if (useRawOutput) {
				std::cout << ts.getRaw();
			} else {
				std::cout << ts.getString(useLocalTime);
			}

			int64_t value, result;
			/* Print the sensor value */
			for (auto query : r.first) {
				DCDB::Unit unit;
				if (query.unit != DCDB::Unit_None) {
					unit = query.unit;
				} else {
					unit = baseUnit;
				}
				value = reading.value;
				result = 0;
				bool resultOk = false;
				switch (query.operation) {
					case DCDB_OP_NONE:
					case DCDB_OP_WINTERMUTE:
						if (scaleAndConvert(value, baseScalingFactor, query.scalingFactor, baseUnit, unit)) {
							result = value;
							resultOk = true;
						}
						break;
					case DCDB_OP_DELTA:
						if (scaleAndConvert(value, baseScalingFactor, query.scalingFactor, baseUnit, unit)) {
							if ((prevT > (uint64_t)0) && (DCDB::delta(value, prevReading, &result) == DCDB::DCDB_OP_SUCCESS)) {
								resultOk = true;
							}
						}
						break;
					case DCDB_OP_DELTAT:
						if (scaleAndConvert(value, baseScalingFactor, query.scalingFactor, baseUnit, unit)) {
							if ((prevT > (uint64_t)0) &&
							    (DCDB::delta(ts.getRaw(), prevT.getRaw(), &result) == DCDB::DCDB_OP_SUCCESS)) {
								resultOk = true;
							}
						}
						break;
					case DCDB_OP_DERIVATIVE: {
						int64_t prevValue = 0;
						/*
			if ((query.sensor_mask & DELTA) != DELTA) {
			    prevValue = prevReading;
			}
			 */
						if (scaleAndConvert(value, baseScalingFactor, query.scalingFactor, baseUnit, unit) &&
						    scaleAndConvert(prevValue, baseScalingFactor, query.scalingFactor, baseUnit, unit)) {
							if ((prevT > (uint64_t)0) && DCDB::derivative(value, prevValue, ts.getRaw(), prevT.getRaw(), &result,
												      unit) == DCDB::DCDB_OP_SUCCESS) {
								resultOk = true;
							}
						}
						break;
					}
					case DCDB_OP_INTEGRAL: {
						int64_t prevValue = prevReading;
						if (scaleAndConvert(prevValue, baseScalingFactor, query.scalingFactor, baseUnit, unit)) {
							if ((prevT > (uint64_t)0) &&
							    DCDB::integral(prevValue, ts.getRaw(), prevT.getRaw(), &result, unit) == DCDB::DCDB_OP_SUCCESS) {
								resultOk = true;
							}
						}
						break;
					}
					case DCDB_OP_RATE: {
						int64_t prevValue = prevReading;
						if (scaleAndConvert(value, baseScalingFactor, query.scalingFactor, baseUnit, unit)) {
							if ((prevT > (uint64_t)0) &&
							    DCDB::rate(value, ts.getRaw(), prevT.getRaw(), &result) == DCDB::DCDB_OP_SUCCESS) {
								resultOk = true;
							}
						}
						break;
					}
					default:
						break;
				}
				if (resultOk) {
					std::cout << "," << result;
				} else {
					std::cout << ",";
				}
			}
			prevReading = value;
			prevT = ts;
		}
	}
	std::cout << std::endl;
}

void DCDBQuery::setInterval(DCDB::TimeStamp start, DCDB::TimeStamp end) {
	start_ts = start;
	end_ts = end;
}

void DCDBQuery::parseSensorSpecification(const std::string sensor, std::string &sensorName, queryConfig_t &queryCfg) {

	std::string s;
	/* Check for function first */
	boost::regex  functRegex("^([^\\(\\)]+)\\(([^\\(\\),]+)(,[0-9]+)?\\)$", boost::regex::extended);
	boost::smatch match;
	std::string   functName;
	std::string   interval;
	if (boost::regex_search(sensor, match, functRegex)) {
		functName = match[1].str();
		s = match[2].str();
		interval = match[3].str().size() ? match[3].str().substr(1) : "";
	} else {
		s = sensor;
	}

	/* Split into sensor name and potential modifier, i.e. unit conversion or scaling factor */
	boost::regex sensorRegex("([^\\@]+)\\@?([^\\@]*)", boost::regex::extended);
	std::string  modifierStr;
	if (boost::regex_search(s, match, sensorRegex)) {
		sensorName = match[1].str();
		modifierStr = match[2].str();
	}

	queryCfg = {1.0, DCDB::Unit_None, DCDB_OP_NONE, DCDB::AGGREGATE_NONE};
	if (functName.length() == 0) {
		queryCfg.operation = DCDB_OP_NONE;
	} else if (boost::iequals(functName, "delta")) {
		queryCfg.operation = DCDB_OP_DELTA;
	} else if (boost::iequals(functName, "delta_t")) {
		queryCfg.operation = DCDB_OP_DELTAT;
	} else if (boost::iequals(functName, "derivative")) {
		queryCfg.operation = DCDB_OP_DERIVATIVE;
	} else if (boost::iequals(functName, "integral")) {
		queryCfg.operation = DCDB_OP_INTEGRAL;
	} else if (boost::iequals(functName, "rate")) {
		queryCfg.operation = DCDB_OP_RATE;
	} else if (boost::iequals(functName, "min")) {
		queryCfg.aggregate = DCDB::AGGREGATE_MIN;
	} else if (boost::iequals(functName, "max")) {
		queryCfg.aggregate = DCDB::AGGREGATE_MAX;
	} else if (boost::iequals(functName, "avg")) {
		queryCfg.aggregate = DCDB::AGGREGATE_AVG;
	} else if (boost::iequals(functName, "sum")) {
		queryCfg.aggregate = DCDB::AGGREGATE_SUM;
	} else if (boost::iequals(functName, "count")) {
		queryCfg.aggregate = DCDB::AGGREGATE_COUNT;
	} else {
		queryCfg.operation = DCDB_OP_WINTERMUTE;
		queryCfg.wintermuteOp = functName;
	}

	if (queryCfg.aggregate != DCDB::AGGREGATE_NONE) {
		queryCfg.interval = 0;
		if (interval.size() > 0) {
			try {
				queryCfg.interval = stoi(interval);
			} catch (...) {}
		}
	}

	if (queryCfg.operation != DCDB_OP_UNKNOWN) {
		if (modifierStr.length() > 0) {
			boost::regex e("[0-9]*\\.?[0-9]*", boost::regex::extended);
			if (boost::regex_match(modifierStr, e)) {
				queryCfg.scalingFactor = atof(modifierStr.c_str());
			} else {
				queryCfg.unit = DCDB::UnitConv::fromString(modifierStr);
			}
		}
	}
}

void DCDBQuery::prepareQuery(std::list<std::string> sensors) {
	std::list<std::string> prefixes;
	prefixes.push_back(std::string(""));
	prepareQuery(sensors, prefixes);
}

void DCDBQuery::prepareQuery(std::list<std::string> sensors, std::list<std::string> prefixes) {
	/* Initialize the SensorConfig interface */
	DCDB::SensorConfig sensorConfig(connection);

	/* Iterate over list of sensors requested by the user */
	for (std::list<std::string>::iterator it = sensors.begin(); it != sensors.end(); it++) {
		std::string s;
		queryConfig_t queryCfg;
		parseSensorSpecification(*it, s, queryCfg);
		for (auto p : prefixes) {
			std::string sensorName = p;
			if ((sensorName.size() > 0) && (sensorName.back() != '/')) {
				sensorName.push_back('/');
			}
			sensorName.append(s);
			if (queryCfg.operation != DCDB_OP_UNKNOWN) {
				std::list<DCDB::PublicSensor> publicSensors;
				DCDB::SCError                 err = DCDB::SC_UNKNOWNERROR;
				if (!ignoreMetadata && (err = sensorConfig.getPublicSensorsByWildcard(publicSensors, sensorName.c_str())) == DCDB::SC_OK) {
					for (auto sen : publicSensors) {
						if (queryCfg.operation == DCDB_OP_WINTERMUTE) {
							if (sen.operations.find(queryCfg.wintermuteOp) != sen.operations.end()) {
								sen.name = sen.name + queryCfg.wintermuteOp;
							} else {
								std::cerr << "Unknown sensor operation: " << queryCfg.wintermuteOp << std::endl;
								continue;
							}
						}
						queries.insert(std::pair<DCDB::PublicSensor, queryConfig_t>(sen, queryCfg));
					}
				} else if (((err == DCDB::SC_UNKNOWNSENSOR) && (!strictSensorCheck)) || ignoreMetadata) {
					DCDB::PublicSensor pS;
					pS.name = sensorName;
					if (queryCfg.operation != DCDB_OP_WINTERMUTE) {
						queries.insert(std::pair<DCDB::PublicSensor, queryConfig_t>(pS, queryCfg));
					} else {
						std::cerr << "Unknown sensor operation: " << queryCfg.wintermuteOp << std::endl;
					}
				} else if ((err == DCDB::SC_UNKNOWNSENSOR) && (strictSensorCheck)) {
					std::cerr << "Unknown sensor and strict sensor name checking is enabled: " << sensorName << std::endl;
				} else {
					std::cerr << "Error #" << err << " obtaining sensor metadata: " << sensorName << std::endl;
				}
			}
		}
	}
}

void DCDBQuery::execute() {
	std::string prevSensorName;
	auto        q = queries.begin();
	while (q != queries.end()) {
		if (q->first.name != prevSensorName) {
			prevSensorName = q->first.name;

			// Find all queries for the same sensor
			std::pair<queryMap_t::iterator, queryMap_t::iterator> range = queries.equal_range(q->first);

			/* Base scaling factor and unit of the public sensor */
			baseUnit = DCDB::UnitConv::fromString(q->first.unit);
			baseScalingFactor = q->first.scaling_factor;

			std::list<queryReadingsPair_t> results;
			DCDB::Sensor                   sensor(connection, q->first);
			q = range.second;

			// Query aggregates first
			auto it = range.first;
			while (it != range.second) {
				if (it->second.aggregate != DCDB::AGGREGATE_NONE) {
					std::list<DCDB::SensorDataStoreReading> readings;
					sensor.query(readings, start_ts, end_ts, it->second.aggregate, it->second.interval * 1000000000ll);
					if (!readings.empty()) {
						results.push_back(queryReadingsPair_t(std::list<queryConfig_t>(1, it->second), readings));
					}
					// Remove the query from the list so it doesn't show up in the raw values below
					// anymore
					if (it == range.first) {
						range.first = std::next(it);
					}
					it = queries.erase(it);
					continue;
				} else {
					it++;
				}
			}
			if (!results.empty()) {
				genOutputAggregates(results);
				results.clear();
			}
			// Query raw values next
			if (range.first != range.second) {
				std::list<DCDB::SensorDataStoreReading> readings;
				sensor.query(readings, start_ts, end_ts, DCDB::AGGREGATE_NONE);
				if (!readings.empty()) {
					std::list<queryConfig_t> queries;
					for (auto it = range.first; it != range.second; it++) {
						if (it->second.aggregate == DCDB::AGGREGATE_NONE) {
							queries.push_back(it->second);
						}
					}
					results.push_back(queryReadingsPair_t(queries, readings));
				}
				genOutputOperators(results);
			}
		}
	}
}

void DCDBQuery::doQuery(std::list<std::string> sensors, DCDB::TimeStamp start, DCDB::TimeStamp end) {
	setInterval(start, end);
	prepareQuery(sensors);
	execute();
}

void DCDBQuery::dojobQuery(std::list<std::string> sensors, std::string jobId) {
	DCDB::JobDataStore jobDataStore(connection);
	DCDB::JobData      jobData;
	DCDB::JDError      err = jobDataStore.getJobById(jobData, jobId);

	if (err == DCDB::JD_OK) {
		setInterval(jobData.startTime, jobData.endTime);
		prepareQuery(sensors, jobData.nodes);
		execute();
	} else {
		std::cerr << "Job not found: " << jobId << std::endl;
	}
}

DCDBQuery::DCDBQuery() {
	connection = nullptr;
	useLocalTime = true;
	useRawOutput = false;
}
