//================================================================================
// Name        : dcdbcsvimport.cpp
// Author      : Michael Ott
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Command line utility for importing CSV data into DCDB
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2011-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include <algorithm>
#include <boost/tokenizer.hpp>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include <pthread.h>

#include "dcdb/version.h"
#include "globalconfiguration.h"
#include "timestamp.h"
#include "version.h"
#include <dcdb/c_api.h>
#include <dcdb/connection.h>
#include <dcdb/sensorconfig.h>
#include <dcdb/sensordatastore.h>
#include <dcdb/sensorid.h>
#include <dcdb/timestamp.h>

int verbose = 0;

typedef struct {
	std::string name;
	uint64_t    count;
	uint64_t    prev;
} sensor_t;

void usage(int argc, char *argv[]) {
	std::cout << "Usage: " << argv[0]
		  << " [-h<host>] [-u<username>] [-p<password>] [-t<col>] [-n<col>] [-c<col[,col,col]>] [-m "
		     "<SensorPrefix>] [-d] [-a] <CSV File>"
		  << std::endl
		  << std::endl;
	std::cout << std::endl;
	std::cout << "Parameters:" << std::endl;
	std::cout << "    <CSV File>          CSV file with sensor readings. First row has to contain sensor names" << std::endl;
	std::cout << std::endl;
	std::cout << "Options:" << std::endl;
	std::cout << "    -h<host>            Cassandra host              [default: " << DEFAULT_CASSANDRAHOST << ":" << DEFAULT_CASSANDRAPORT << "]" << endl;
	std::cout << "    -u<username>        Cassandra username          [default: none]" << std::endl;
	std::cout << "    -p<password>        Cassandra password          [default: none]" << std::endl;
	std::cout << "    -t <col>            Timestamp column in the CSV [default: 0]" << std::endl;
	std::cout << "    -n <col>            Sensor name column" << std::endl;
	std::cout << "    -c <col[,col,col]>  Column in the CSV to use    [default: all]" << std::endl;
	std::cout << "    -m <SensorPrefix>   MQTT prefix to use for sensors" << std::endl;
	std::cout << "    -v <level>          Verbosity level             [default: 0]" << std::endl;
	std::cout << "    -d                  Drop constant values" << std::endl;
	std::cout << "    -a                  Publish sensors" << std::endl;
	std::cout << std::endl;
	std::cout << "Note: To import dcdbquery output, use: -n0 -t1 -c2" << std::endl;
}

std::string createSensorName(const std::string &name, const std::string &prefix) {
	std::string sn;
	if (prefix.size()) {
		sn = prefix + "/" + name;
	} else {
		sn = name;
	}
	if (sn[0] != '/') {
		sn = "/" + sn;
	}
	std::replace(sn.begin(), sn.end(), ' ', '_');
	return sn;
}

sensor_t *createSensor(DCDB::SensorConfig &sensorConfig, const std::string &name, const std::string &prefix) {
	sensor_t *sensor = new sensor_t;
	sensor->name = createSensorName(name, prefix);
	sensor->count = 0;

	if (verbose) {
		std::cout << "Created new sensor " << sensor->name << " in database" << std::endl;
	}
	return sensor;
}

std::string getEnv(const char *var) {
	char *str = std::getenv(var);
	if (str != NULL) {
		return std::string(str);
	} else {
		return std::string("");
	}
}

int main(int argc, char **argv) {
	std::cout << "dcdbcsvimport " << VERSION << " (libdcdb " << DCDB::Version::getVersion() << ")" << std::endl << std::endl;
	/* Check command line parameters */
	if (argc < 2) {
		usage(argc, argv);
		exit(EXIT_FAILURE);
	}

	int           ret;
	int           suffixStart = 0;
	int           tsColumn = 0;
	std::set<int> columns;
	std::string   prefix;
	bool          dropConstantValues = false;
	bool          publish = false;
	int           sensorNameColumn = -1;

	std::string host = getEnv("DCDB_HOSTNAME") != "" ? getEnv("DCDB_HOSTNAME") : "localhost";
	std::string username = getEnv("DCDB_USERNAME");
	std::string password = getEnv("DCDB_PASSWORD");
	std::string hostname;
	uint16_t    port;

	while ((ret = getopt(argc, argv, "+h:u:p:t:n:c:ds:av:")) != -1) {
		switch (ret) {
			case 'h':
				host = optarg;
				break;
			case 'u':
				username = optarg;
				break;
			case 'p': {
				password = optarg;
				// What does this do? Mask the password?
				size_t pwdLen = strlen(optarg);
				memset(optarg, 'x', (pwdLen >= 3) ? 3 : pwdLen);
				if (pwdLen > 3) {
					memset(optarg + 3, 0, pwdLen - 3);
				}
				break;
			}
			case 't':
				tsColumn = atoi(optarg);
				break;
			case 'n':
				sensorNameColumn = atoi(optarg);
				break;
			case 'c': {
				std::string                                           s(optarg);
				boost::tokenizer<boost::escaped_list_separator<char>> tk(s, boost::escaped_list_separator<char>('\\', ',', '\"'));
				for (boost::tokenizer<boost::escaped_list_separator<char>>::iterator i = tk.begin(); i != tk.end(); ++i)
					columns.insert(std::stoi(*i));
			} break;
			case 'm':
				prefix = optarg;
				break;
			case 'd':
				dropConstantValues = true;
				break;
			case 'a':
				publish = true;
				break;
			case 'v':
				if (optarg) {
					verbose = atoi(optarg);
				} else {
					verbose = 1;
				}
				break;
			default:
				usage(argc, argv);
				std::cout << "Unknown parameter: " << (char)ret << std::endl;
				exit(EXIT_FAILURE);
		}
	}

	std::string csvFilename = argv[optind];

	// Parsing host string
	hostname = parseNetworkHost(host);
	port = atoi(parseNetworkPort(host).c_str());
	if (port == 0) {
		port = atoi(DEFAULT_CASSANDRAPORT);
	}

	columns.erase(tsColumn);
	if (sensorNameColumn > -1) {
		columns.erase(sensorNameColumn);
	}

	/* Connect to the data store */
	std::cout << std::endl;
	std::cout << "Connecting to the data store " << host << ":" << port << std::endl;

	DCDB::Connection *connection;
	connection = new DCDB::Connection(hostname, port, username, password);
	connection->setNumThreadsIo(4);
	connection->setQueueSizeIo(256 * 1024);

	if (!connection->connect()) {
		std::cout << "Cannot connect to database." << std::endl;
		return 1;
	}

	/* Initialize the SensorConfig and SensorDataStore interfaces */
	DCDB::SensorConfig sensorConfig(connection);
	sensorConfig.loadCache();
	DCDB::SensorDataStore sensorDataStore(connection);
	sensorDataStore.setDebugLog(true);

	std::ifstream fs;
	std::string   s;
	uint64_t      lineno = 0;

	/* Read header line from CSV to obtain sensor names and topics */
	std::cout << std::endl;
	std::cout << "Parsing CSV file: " << csvFilename << std::endl;
	if (prefix.size()) {
		std::cout << "Using MQTT prefix: " << prefix << std::endl;
	}
	std::cout << "Columns:";
	if (columns.size()) {
		std::set<int>::iterator it;
		for (it = columns.begin(); it != columns.end(); ++it) {
			std::cout << " " << *it;
		}
		std::cout << std::endl;
	} else {
		std::cout << "all" << std::endl;
	}
	std::cout << "Timestamp Column: " << tsColumn << std::endl;
	if (sensorNameColumn > -1) {
		std::cout << "Sensorname Column: " << sensorNameColumn << std::endl;
	}

	fs.open(csvFilename, std::fstream::in);

	int                               col = 0;
	std::map<int, sensor_t *>         sensorsByCol;
	std::map<std::string, sensor_t *> sensorsByName;
	sensor_t *                        sensor = nullptr;
	if (sensorNameColumn == -1) {
		std::getline(fs, s);
		lineno++;
		boost::tokenizer<boost::escaped_list_separator<char>> tk(s, boost::escaped_list_separator<char>('\\', ',', '\"'));
		for (boost::tokenizer<boost::escaped_list_separator<char>>::iterator i = tk.begin(); i != tk.end(); ++i) {
			if (col != tsColumn) {
				sensor = createSensor(sensorConfig, *i, prefix);
				sensorsByCol.insert(std::pair<int, sensor_t *>(col, sensor));
			}
			col++;
		}
	}

	/* Read actual sensor readings */
	uint64_t count = 0;
	uint64_t total = 0;
	time_t   t0 = time(NULL);
	while (std::getline(fs, s)) {
		lineno++;

		col = 0;
		DCDB::TimeStamp                                       ts(UINT64_C(0));
		boost::tokenizer<boost::escaped_list_separator<char>> tk(s, boost::escaped_list_separator<char>('\\', ',', '\"'));
		for (boost::tokenizer<boost::escaped_list_separator<char>>::iterator i = tk.begin(); i != tk.end(); ++i) {
			if (col == tsColumn) {
				try {
					ts = DCDB::TimeStamp(*i);
				} catch (std::exception &e) {
					if (verbose > 1) {
						std::cerr << "Error parsing timestamp " << *i << std::endl;
					}
				}
			} else if (col == sensorNameColumn) {
				std::map<std::string, sensor_t *>::iterator it = sensorsByName.find(createSensorName(*i, prefix));
				if (it != sensorsByName.end()) {
					sensor = it->second;
				} else {
					sensor = createSensor(sensorConfig, *i, prefix);
					sensorsByName.insert(std::pair<std::string, sensor_t *>(sensor->name, sensor));
				}
			}
			col++;
		}

		if (ts.getRaw() == 0) {
			continue;
		}

		col = 0;
		for (boost::tokenizer<boost::escaped_list_separator<char>>::iterator i = tk.begin(); i != tk.end(); ++i) {
			if (((columns.size() == 0) || (columns.find(col) != columns.end()))) {
				if (sensorNameColumn == -1) {
					sensor = sensorsByCol[col];
				}
				if (verbose >= 2) {
					std::cout << ts.getRaw() << " " << col << " " << sensor->name << " " << *i << std::endl;
				}
				try {
					DCDB::SensorId sid(sensor->name);
					uint64_t       val = std::stoll(*i);
					if (!dropConstantValues || val != sensor->prev) {
						sensorDataStore.insert(sid, ts.getRaw(), val);
						sensor->count++;
						sensor->prev = val;
						count++;
					}
				} catch (std::exception &e) {
					if (verbose > 1) {
						std::cerr << "Error parsing CSV line " << lineno << " column " << col + 1 << ": \"" << s << "\"" << std::endl;
					}
				}
				total++;
				if (total % 1000 == 0) {
					time_t t1 = time(NULL);
					if (t1 != t0) {
						std::cout << total << " " << count / (t1 - t0) << " inserts/s" << std::endl;
						t0 = t1;
						count = 0;
					}
				}
			}
			col++;
		}
	}

	std::cout << "Inserted " << total << " readings" << std::endl;

	fs.close();

	/* Create public sensor names */
	if (publish) {
		std::cout << std::endl;
		std::cout << "Publishing sensors..." << std::endl;

		if (sensorNameColumn > -1) {
			std::map<std::string, sensor_t *>::iterator it;
			for (it = sensorsByName.begin(); it != sensorsByName.end(); it++) {
				sensor_t *sensor = it->second;
				std::cout << sensor->name << " (" << sensor->count << " inserts)" << std::endl;
				sensorConfig.publishSensor(sensor->name.c_str());
			}
		} else {
			std::map<int, sensor_t *>::iterator it;
			for (it = sensorsByCol.begin(); it != sensorsByCol.end(); it++) {
				sensor_t *sensor = it->second;
				std::cout << sensor->name << " (" << sensor->count << " inserts)" << std::endl;
				sensorConfig.publishSensor(sensor->name.c_str());
			}
		}
		sensorConfig.setPublishedSensorsWritetime(getTimestamp());
	}

	/* Disconnect */
	delete connection;

	return 0;
}
