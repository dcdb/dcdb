//================================================================================
// Name        : dcdbconfig.cpp
// Author      : Axel Auweter
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Main file of the dcdbconfig command line utility
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2011-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include <cstdlib>
#include <cstring>
#include <iostream>

#include <unistd.h>

#include "cassandra.h"
#include "dcdb/version.h"
#include "globalconfiguration.h"
#include "useraction.h"
#include "version.h"

void usage(int argc, char *argv[]) {
	std::cout << "Usage: " << argv[0] << " [-h<host>] [-u<username>] [-p<password>] [-r] [-l|-t] <command> [<arguments> ... ]" << std::endl << std::endl;
	std::cout << "Valid commands are: " << std::endl;
	std::cout << "    HELP <command name> print help for given command" << std::endl;
	std::cout << "    DB                  perform low-level database functions" << std::endl;
	std::cout << "    SENSOR              list and configure sensors" << std::endl;
	std::cout << "    JOB                 list and show job information" << std::endl;
	std::cout << std::endl;
	std::cout << "Options:" << std::endl;
	std::cout << "    -h<host>            Cassandra host                         [default: " << DEFAULT_CASSANDRAHOST << ":" << DEFAULT_CASSANDRAPORT << "]"
		  << std::endl;
	std::cout << "    -u<username>        Cassandra username                     [default: none]" << std::endl;
	std::cout << "    -p<password>        Cassandra password                     [default: none]" << std::endl;
	std::cout << "    -r                  Report timestamps in numerical format" << std::endl;
	std::cout << "    -l                  Report times in local time (not UTC)   [default]" << std::endl;
	std::cout << "    -t                  Report times in UTC time" << std::endl;
}

std::string getEnv(const char *var) {
	char *str = std::getenv(var);
	if (str != NULL) {
		return std::string(str);
	} else {
		return std::string("");
	}
}

int main(int argc, char *argv[]) {
	std::cout << "dcdbconfig " << VERSION << " (libdcdb " << DCDB::Version::getVersion() << ")" << std::endl << std::endl;

	/* Check if run from command line */
	int    argcReal;
	char **argvReal;
	if (isatty(fileno(stdin))) {
		argcReal = argc;
		argvReal = (char **)argv;
	} else {
		/* Check if we are a CGI program */
		if (!getenv("QUERY_STRING")) {
			std::cout << "No terminal and no QUERY_STRING environment variable." << std::endl;
			std::cout << "Exiting." << std::endl;
			exit(EXIT_FAILURE);
		}

		/* Print content type */
		std::cout << "Content-type: text/plain" << std::endl << std::endl;

		/* Create argcReal and argvReal */
		argcReal = 1;
		argvReal = (char **)malloc(sizeof(char *));
		argvReal[0] = strdup("dcdbconfig"); /* dummy to make consistent with command line invocation */

		char *token = strtok(getenv("QUERY_STRING"), "&");
		while (token) {
			argcReal++;
			argvReal = (char **)realloc((void *)argvReal, argcReal * sizeof(char *));
			/* FIXME: should check if argvReal is NULL */
			argvReal[argcReal - 1] = token;
			token = strtok(NULL, "&");
		}
	}

	/* Check command line parameters */
	if (argcReal < 2) {
		usage(argcReal, argvReal);
		exit(EXIT_FAILURE);
	}

	int ret;

	std::string                                    host = getEnv("DCDB_HOSTNAME") != "" ? getEnv("DCDB_HOSTNAME") : "localhost";
	std::string                                    username = getEnv("DCDB_USERNAME");
	std::string                                    password = getEnv("DCDB_PASSWORD");
	std::string                                    hostname;
	uint16_t                                       port;
	std::map<UserAction::parameter_t, std::string> parameters;

	while ((ret = getopt(argcReal, argvReal, "+h:u:p:rlt")) != -1) {
		switch (ret) {
			case 'h':
				host = optarg;
				break;
			case 'u':
				username = optarg;
				break;
			case 'p': {
				password = optarg;
				// What does this do? Mask the password?
				size_t pwdLen = strlen(optarg);
				memset(optarg, 'x', (pwdLen >= 3) ? 3 : pwdLen);
				if (pwdLen > 3) {
					memset(optarg + 3, 0, pwdLen - 3);
				}
				break;
			}
			case 'r':
				parameters[UserAction::rawTimestamps] = "true";
				break;
			case 'l':
				parameters[UserAction::localTime] = "true";
				break;
			case 't':
				parameters[UserAction::localTime] = "false";
				break;
			default:
				usage(argcReal, argvReal);
				exit(EXIT_FAILURE);
		}
	}

	if (optind >= argcReal) {
		std::cout << "Missing command!" << std::endl;
		usage(argcReal, argvReal);
		exit(EXIT_FAILURE);
	}

	// Parsing host string
	hostname = parseNetworkHost(host);
	port = atoi(parseNetworkPort(host).c_str());
	if (port == 0) {
		port = atoi(DEFAULT_CASSANDRAPORT);
	}

	/* Process user command */
	if (strcasecmp(argvReal[optind], "help") == 0) {
		/* Help is special: either we do general usage or we trigger the class factory and run the printHelp()
		 * function */
		if (optind + 1 >= argcReal) {
			usage(argcReal, argvReal);
		} else {
			auto action = UserActionFactory::getAction(argvReal[optind + 1]);
			if (action) {
				action->printHelp(argcReal, argvReal);
			} else {
				std::cout << "Cannot provide help for unknown command: " << argvReal[optind + 1] << std::endl;
				exit(EXIT_FAILURE);
			}
		}
	} else {
		/* If the command is not help, we try to instantiate the respective class through the factory and
		 * process the command */
		auto action = UserActionFactory::getAction(argvReal[optind]);
		if (action) {
			action->setConnection(hostname, port, username, password);
			if (!parameters.empty()) {
				action->setParameters(parameters);
			}
			return action->executeCommand(argcReal, argvReal, optind);
		} else {
			std::cout << "Unknwon command: " << argvReal[1] << std::endl;
			usage(argcReal, argvReal);
			exit(EXIT_FAILURE);
		}
	}

	/* Shouldn't fall through here */
	return 0;
}
