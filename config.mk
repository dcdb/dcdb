DCDBSRCPATH ?= $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
DCDBBASEPATH ?= $(realpath $(DCDBSRCPATH)/..)
DCDBDEPSPATH ?= $(DCDBBASEPATH)/deps
DCDBDEPLOYPATH ?= $(DCDBBASEPATH)/install

# dcdbpusher plugins to be built
PLUGINS = sysfs ipmi pdu bacnet snmp procfs tester gpfsmon msr rest s7plc modbus

# data analytics plugins to be built
OPERATORS = aggregator smoothing regressor classifier clustering cssignatures job_aggregator testeroperator filesink healthchecker jsonexporter

DEFAULT_VERSION = $(shell grep "\#define VERSION " common/include/version.h|cut -d'"' -f 2)
GIT_VERSION = $(shell git describe --tags 2>/dev/null|sed -e 's/-\([0-9]*\)/.\1/'|tr -d '\n')
VERSION := $(if $(GIT_VERSION),$(GIT_VERSION),$(DEFAULT_VERSION))

CXXFLAGS = -std=c++17 -O2 -g -Wall -fmessage-length=0 \
           -DBOOST_DATE_TIME_POSIX_TIME_STD_CONFIG \
           -DBOOST_LOG_DYN_LINK -DVERSION=\"$(VERSION)\" \
           -Wno-unused-function \
           -Wno-unused-variable \
           -DUSE_SENSOR_CACHE

FULL_CC = $(shell which $(CC))
FULL_CXX = $(shell which $(CXX))

# Path to Intel Level Zero libraries, required for the SysMan dcdbpusher plugin
INTEL_ZE_PATH = "/opt/intel/graphics-compute-runtime/21.2.3.8471/usr"

OS = $(shell uname -s)
ARCH = $(shell uname -m)
MAKETHREADS ?= $(if $(findstring $(shell uname),Darwin),$(shell sysctl machdep.cpu.thread_count | cut -b 27-),\
               $(if $(findstring $(shell uname),Linux),$(shell cat /proc/cpuinfo | grep processor | wc -l),4))
               
ifneq ($(OS),Darwin)
	LDFLAGS = '-Wl,-rpath=$$ORIGIN/../lib:$$ORIGIN/../lib64,-rpath-link=$(DCDBDEPLOYPATH)/lib'
	PLUGINS += perfevent likwid variorum
endif


include $(DCDBSRCPATH)/dependencies.mk
ifneq (,$(wildcard $(DCDBSRCPATH)/local.mk))
        include $(DCDBSRCPATH)/local.mk
endif
OPENSSL_PATH ?= $(shell which openssl | sed 's/\(.*\)\/bin\/openssl/\1/')
ifneq (, $(wildcard $(OPENSSL_PATH)/lib64/libcrypto.*))
	OPENSSL_LIB ?= $(OPENSSL_PATH)/lib64
else
	OPENSSL_LIB ?= $(OPENSSL_PATH)/lib
endif
OPENSSL_INCL ?= $(OPENSSL_PATH)/include

$(eval $(call plugins2deps))
.DEFAULT_GOAL = all
