#include "callback.h"
#include "messaging.h"

extern bool                                             newAutoPub;
extern SensorCache                                      mySensorCache;
extern DCDB::SensorDataStore *                          mySensorDataStore;
extern DCDB::JobDataStore *                             myJobDataStore;
extern DCDB::SensorConfig *                             mySensorConfig;
extern DCDB::CaliEvtDataStore *                         myCaliEvtDataStore;
extern MetadataStore *                                  metadataStore;
extern Statistics*                                      stats;
extern logger_t                                         lg;

bool jobQueryCallback(const string &jobId, const uint64_t startTs, const uint64_t endTs, vector<qeJobData> &buffer, const bool rel, const bool range, const string &domainId) {
	std::list<JobData> tempList;
	JobData            tempData;
	qeJobData          tempQeData;
	JDError            err;
	if (range) {
		// Getting a list of jobs in the given time range
		uint64_t        now = getTimestamp();
		uint64_t        startTsInt = rel ? now - startTs : startTs;
		uint64_t        endTsInt = rel ? now - endTs : endTs;
		DCDB::TimeStamp start(startTsInt), end(endTsInt);
		err = myJobDataStore->getJobsInIntervalRunning(tempList, start, end, domainId);
		if (err != JD_OK)
			return false;
	} else {
		// Getting a single job by id
		err = myJobDataStore->getJobById(tempData, jobId, domainId);
		if (err != JD_OK)
			return false;
		tempList.push_back(tempData);
	}

	for (auto &jd : tempList) {
		tempQeData.domainId = jd.domainId;
		tempQeData.jobId = jd.jobId;
		tempQeData.userId = jd.userId;
		tempQeData.startTime = jd.startTime.getRaw();
		tempQeData.endTime = jd.endTime.getRaw();
		tempQeData.nodes = jd.nodes;
		tempQeData.modules = jd.modules;
		buffer.push_back(tempQeData);
	}
	return true;
}

bool sensorGroupQueryCallback(const std::vector<string> &names, const uint64_t startTs, const uint64_t endTs, std::vector<reading_t> &buffer, const bool rel,
			      const uint64_t tol) {
	// Returning NULL if the query engine is being updated
	if (_queryEngine->updating.load())
		return false;
	++_queryEngine->access;

	std::list<DCDB::SensorId> topics;
	sensorCache_t &           sensorMap = mySensorCache.getSensorMap();
	size_t                    successCtr = 0;
	for (const auto &name : names) {
		DCDB::SensorId sid;
		// Creating a SID to perform the query
		if (sid.mqttTopicConvert(name)) {
			mySensorCache.wait();
			if (sensorMap.count(sid) > 0 && sensorMap[sid].getView(startTs, endTs, buffer, rel, tol)) {
				// Data was found, can continue to next SID
				successCtr++;
			} else {
				// This happens only if no data was found in the local cache
				topics.push_back(sid);
			}
			mySensorCache.release();
		}
	}
	if (successCtr) {
		stats->increase(Statistics::CACHED_QUERIES, buffer.size());
	}

	// If we are here then some sensors were not found in the cache - we need to fetch data from Cassandra
	if (!topics.empty()) {
		try {
			std::list<DCDB::SensorDataStoreReading> results;
			uint64_t                                now = getTimestamp();
			// Converting relative timestamps to absolute
			uint64_t        startTsInt = rel ? now - startTs : startTs;
			uint64_t        endTsInt = rel ? now - endTs : endTs;
			DCDB::TimeStamp start(startTsInt), end(endTsInt);
			uint16_t        startWs = start.getWeekstamp(), endWs = end.getWeekstamp();
			// If timestamps are equal we perform a fuzzy query
			if (startTsInt == endTsInt) {
				topics.front().setRsvd(startWs);
				mySensorDataStore->fuzzyQuery(results, topics, start, tol, false);
			}
			// Else, we iterate over the weekstamps (if more than one) and perform range queries
			else {
#ifndef SENSORDATA_ASC
				for (uint16_t currWs = endWs; currWs >= startWs; currWs--) {
#else
				for (uint16_t currWs = startWs; currWs <= endWs; currWs++) {
#endif
					topics.front().setRsvd(currWs);
					mySensorDataStore->query(results, topics, start, end, DCDB::AGGREGATE_NONE, false);
				}
			}

			if (!results.empty()) {
				successCtr++;
				reading_t reading;
				stats->increase(Statistics::DB_QUERIES, results.size());
				for (const auto &r : results) {
					reading.value = r.value;
					reading.timestamp = r.timeStamp.getRaw();
					buffer.push_back(reading);
				}
			} else {
				stats->increase(Statistics::MISSED_QUERIES, topics.size());
			}
		} catch (const std::exception &e) {}
	}

	--_queryEngine->access;
	return successCtr > 0;
}

bool sensorQueryCallback(const string &name, const uint64_t startTs, const uint64_t endTs, std::vector<reading_t> &buffer, const bool rel, const uint64_t tol) {
	// Returning NULL if the query engine is being updated
	if (_queryEngine->updating.load())
		return false;
	std::vector<std::string> nameWrapper;
	nameWrapper.push_back(name);
	return sensorGroupQueryCallback(nameWrapper, startTs, endTs, buffer, rel, tol);
}

bool metadataQueryCallback(const string &name, SensorMetadata &buffer) {
	// Returning NULL if the query engine is being updated
	if (_queryEngine->updating.load())
		return false;
	++_queryEngine->access;

	bool local = false;
	metadataStore->wait();
	if (metadataStore->getMap().count(name)) {
		buffer = metadataStore->get(name);
		local = true;
	}
	metadataStore->release();

	if (!local) {
		// If we are here then the sensor was not found in the cache - we need to fetch data from Cassandra
		try {
			DCDB::PublicSensor publicSensor;
			if (mySensorConfig->getPublicSensorByName(publicSensor, name.c_str()) != SC_OK) {
				--_queryEngine->access;
				return false;
			}
			buffer = DCDB::PublicSensor::publicSensorToMetadata(publicSensor);
		} catch (const std::exception &e) {
			--_queryEngine->access;
			return false;
		}
	}
	--_queryEngine->access;
	return true;
}

int mqttCallback(SimpleMQTTMessage *msg) {
	/*
	 * Increment the message counter for statistics.
	 */
	stats->increase(Statistics::MSGS_RCVD);

#ifndef BENCHMARK_MODE

	uint64_t len;
	/*
	 * Decode the message and put into the database.
	 */
	if (msg->isPublish()) {
		const char *topic = msg->getTopic().c_str();
		// We check whether the topic includes the /DCDB_MAP/ keyword, indicating that the payload will contain
		// the sensor's name. In that case, we set the mappingMessage flag to true, and filter the keyword out
		// of the prefix We use strncmp as it is the most efficient way to do it
		if (strncmp(topic, DCDB_MAP, DCDB_MAP_LEN) == 0) {
			if ((len = msg->getPayloadLength()) == 0) {
				LOG(error) << "Empty sensor publish message received!";
				return 1;
			}

			string payload((char *)msg->getPayload(), len);
            DCDB::SCError err = DCDB::SC_OK;
			// If the topic includes the extended /DCDB_MAP/METADATA/ keyword, we assume a JSON metadata
			// packet is encoded
			if (strncmp(topic, DCDB_MET, DCDB_MET_LEN) == 0) {
				SensorMetadata sm;
				try {
					sm.parseJSON(payload);
				} catch (const std::exception &e) {
					LOG(error) << "Invalid metadata packed received!";
					return 1;
				}
				if (sm.isValid()) {
					err = mySensorConfig->publishSensor(sm);
					metadataStore->store(*sm.getPublicName(), sm);
				}
			} else {
				err = mySensorConfig->publishSensor(payload.c_str());
			}

			// PublishSensor does most of the error checking for us
			switch (err) {
				case DCDB::SC_INVALIDPUBLICNAME:
					LOG(error) << "Invalid sensor public name.";
					return 1;
				case DCDB::SC_INVALIDSESSION:
					LOG(error) << "Cannot reach sensor data store.";
					return 1;
				default:
					break;
			}

			newAutoPub = true;
		} else if (strncmp(topic, DCDB_CALIEVT, DCDB_CALIEVT_LEN) == 0) {
			/*
			 * Special message case. This message contains a Caliper Event data
			 * string that is encoded in the MQTT topic. Its payload consists of
			 * usual timestamp-value pairs.
			 * Data from this messages will be stored in a separate table managed
			 * by CaliEvtDataStore class.
			 */
			std::string topicStr(msg->getTopic());
			mqttPayload buf, *payload;

			len = msg->getPayloadLength();
			// TODO support case that no timestamp is given?
			// retrieve timestamp and value from the payload
			if ((len % sizeof(mqttPayload) == 0) && (len > 0)) {
				payload = (mqttPayload *)msg->getPayload();
			} else {
				// this message is malformed -> ignore
				LOG(error) << "Message malformed";
				return 1;
			}

			/*
			 * Decode message topic in actual sensor topic and string data.
			 * "/:/" is used as delimiter between topic and data.
			 */
			topicStr.erase(0, DCDB_CALIEVT_LEN);
			size_t pos = topicStr.find("/:/");
			if (pos == std::string::npos) {
				// topic is malformed -> ignore
				LOG(error) << "CaliEvt topic malformed";
				return 1;
			}
			const std::string data(topicStr, pos + 3);
			topicStr.erase(pos);

			/*
			 * We use the same MQTT-topic/SensorId infrastructure as actual sensor
			 * data readings to sort related events.
			 * Check if we can decode the event topic into a valid SensorId. If
			 * successful, store the record in the database.
			 */
			DCDB::SensorId sid;
			if (sid.mqttTopicConvert(topicStr)) {
				// std::list<DCDB::CaliEvtData> events;
				DCDB::CaliEvtData e;
				e.eventId = sid;
				e.event = data;
				for (uint64_t i = 0; i < len / sizeof(mqttPayload); i++) {
					e.timeStamp = payload[i].timestamp;

					/**
					 * We want an exhaustive list of all events ordered by their
					 * time of occurrence. Payload values should always be
					 * one. Other values currently indicate a malformed message.
					 *
					 * In the future, the value field could be used to aggregate
					 * multiple equal events that occurred in the same plugin
					 * read cycle.
					 */
					if (payload[i].value != 1) {
						LOG(error) << "CaliEvt message malformed. Value != 1";
						return 1;
					}

					myCaliEvtDataStore->insert(e, metadataStore->getTTL(topicStr));
					// events.push_back(e);
				}
				// myCaliEvtDataStore->insertBatch(events, metadataStore->getTTL(topicStr));
			} else {
				LOG(error) << "Topic could not be converted to SID";
			}
		} else if (strncmp(topic, DCDB_JOBDATA, DCDB_JOBDATA_LEN) == 0) {
			/**
			 * This message contains Slurm job data in JSON format. We need to
			 * parse the payload and store it within the JobDataStore.
			 */
			if ((len = msg->getPayloadLength()) == 0) {
				LOG(error) << "Empty job data message received!";
				return 1;
			}

			// parse JSON into JobData object
			string        payload((char *)msg->getPayload(), len);
			DCDB::JobData jd;
			try {
				boost::property_tree::iptree config;
				std::istringstream           str(payload);
				boost::property_tree::read_json(str, config);
				BOOST_FOREACH (boost::property_tree::iptree::value_type &val, config) {
					if (boost::iequals(val.first, "jobid")) {
						jd.jobId = val.second.data();
					} else if (boost::iequals(val.first, "domainid")) {
						jd.domainId = val.second.data();
					} else if (boost::iequals(val.first, "userid")) {
						jd.userId = val.second.data();
					} else if (boost::iequals(val.first, "starttime")) {
						jd.startTime = DCDB::TimeStamp((uint64_t)stoull(val.second.data()));
					} else if (boost::iequals(val.first, "endtime")) {
						jd.endTime = DCDB::TimeStamp((uint64_t)stoull(val.second.data()));
					} else if (boost::iequals(val.first, "nodes")) {
						BOOST_FOREACH (boost::property_tree::iptree::value_type &node, val.second) {
							jd.nodes.push_back(node.second.data());
						}
					} else if (boost::iequals(val.first, "modules")) {
						BOOST_FOREACH (boost::property_tree::iptree::value_type &module, val.second) {
							jd.modules.push_back(module.second.data());
						}
					}
				}
			} catch (const std::exception &e) {
				LOG(error) << "Invalid job data packet received!";
				return 1;
			}

#ifdef DEBUG
			LOG(debug) << "JobId  = " << jd.jobId;
			LOG(debug) << "UserId = " << jd.userId;
			LOG(debug) << "Start  = " << jd.startTime.getString();
			LOG(debug) << "End    = " << jd.endTime.getString();
			LOG(debug) << "Nodes: ";
			for (const auto &n : jd.nodes) {
				LOG(debug) << "    " << n;
			}
#endif

			// store JobData into Storage Backend
			// dcdbslurmjob start inserts the endTime as startTime + max job length + 1, i.e. the last digit
			// will be 1
			if ((jd.startTime != DCDB::TimeStamp((uint64_t)0)) && ((jd.endTime == DCDB::TimeStamp((uint64_t)0)) || ((jd.endTime.getRaw() & 0x1) == 1))) {
				// starting job data
				if (myJobDataStore->insertJob(jd) != DCDB::JD_OK) {
					LOG(error) << "Job data insert for job " << jd.jobId << " failed!";
					return 1;
				}
			} else {
				DCDB::JobData tmp;
				if (myJobDataStore->getJobById(tmp, jd.jobId, jd.domainId) != DCDB::JD_OK) {
					LOG(error) << "Could not retrieve job " << jd.jobId << " to be updated!";
					return 1;
				}
				
				// Update module list
				if (!jd.modules.empty()) {
					if (myJobDataStore->updateModuleList(tmp.jobId, tmp.startTime, jd.modules, jd.domainId) != DCDB::JD_OK) {
						LOG(error) << "Could not update module list of job " << tmp.jobId << "!";
					}
				}
				
				// ending job data
				if ((jd.endTime != DCDB::TimeStamp((uint64_t)0)) && ((jd.endTime.getRaw() & 0x1) != 1)) {
					if (myJobDataStore->updateEndtime(tmp.jobId, tmp.startTime, jd.endTime, jd.domainId) != DCDB::JD_OK) {
						LOG(error) << "Could not update end time of job " << tmp.jobId << "!";
						return 1;
					}
				}
			}
		} else {
			mqttPayload buf, *payload;

			len = msg->getPayloadLength();
			// In the 64 bit message case, the collect agent provides a timestamp
			if (len == sizeof(uint64_t)) {
				payload = &buf;
				payload->value = *((int64_t *)msg->getPayload());
				payload->timestamp = Messaging::calculateTimestamp();
				len = sizeof(uint64_t) * 2;
			}
			//...otherwise it just retrieves it from the MQTT message payload.
			else if ((len % sizeof(mqttPayload) == 0) && (len > 0)) {
				payload = (mqttPayload *)msg->getPayload();
			}
			//...otherwise this message is malformed -> ignore...
			else {
				LOG(error) << "Message malformed";
				return 1;
			}

			/*
			 * Check if we can decode the message topic
			 * into a valid SensorId. If successful, store
			 * the record in the database.
			 */
			DCDB::SensorId sid;
			if (sid.mqttTopicConvert(msg->getTopic())) {
#if 0
              cout << "Topic decode successful:" << endl
                  << "  Raw:            " << hex << setw(16) << setfill('0') << sid.getRaw()[0] << hex << setw(16) << setfill('0') << sid.getRaw()[1] << endl
                  << "  DeviceLocation: " << hex << setw(16) << setfill('0') << sid.getDeviceLocation() << endl
                  << "  device_id:      " << hex << setw(8) << setfill('0') << sid.getDeviceSensorId().device_id << endl
                  << "  sensor_number:  " << hex << setw(4) << setfill('0') << sid.getDeviceSensorId().sensor_number << endl << dec;

              cout << "Payload ("  << len/sizeof(mqttPayload) << " messages):"<< endl;
              for (uint64_t i=0; i<len/sizeof(mqttPayload); i++) {
                cout << "  " << i << ": ts=" << payload[i].timestamp << " val=" << payload[i].value << endl;
              }
              cout << endl;
#endif
				std::list<DCDB::SensorDataStoreReading> readings;
				for (uint64_t i = 0; i < len / sizeof(mqttPayload); i++) {
					DCDB::SensorDataStoreReading r(sid, payload[i].timestamp, payload[i].value);
					readings.push_back(r);
					mySensorCache.storeSensor(sid, payload[i].timestamp, payload[i].value);
				}
				mySensorCache.getSensorMap()[sid].updateBatchSize(uint64_t(len / sizeof(mqttPayload)));
				mySensorDataStore->insertBatch(readings, metadataStore->getTTL(msg->getTopic()));
				stats->increase(Statistics::READINGS_RCVD, readings.size());

				// mySensorCache.dump();
			} else {
				LOG(error) << "Message with empty topic received";
			}
		}
	}
#endif
	return 0;
}