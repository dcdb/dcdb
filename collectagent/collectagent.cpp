//================================================================================
// Name        : collectagent.cpp
// Author      : Axel Auweter
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Main code of the CollectAgent
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2011-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

/**
 * @defgroup ca Collect Agent
 *
 * @brief MQTT message broker in between pusher and storage backend.
 *
 * @details Collect Agent is a intermediary between one or multiple Pusher
 *          instances and one storage backend. It runs a reduced custom MQTT
 *          message server. Collect Agent receives data from Pusher
 *          via MQTT messages and stores them in the storage via libdcdb.
 */

/**
 * @file collectagent.cpp
 *
 * @brief Main function for the DCDB Collect Agent.
 *
 * @ingroup ca
 */

#include <cstdlib>
#include <signal.h>
#include <string>
#include <unistd.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include "version.h"
#include <dcdb/calievtdatastore.h>
#include <dcdb/connection.h>
#include <dcdb/jobdatastore.h>
#include <dcdb/libconfig.h>
#include <dcdb/sensor.h>
#include <dcdb/sensorconfig.h>
#include <dcdb/sensordatastore.h>
#include <dcdb/version.h>

#include "../analytics/includes/QueryEngine.h"
#include "CARestAPI.h"
#include "abrt.h"
#include "analyticscontroller.h"
#include "configuration.h"
#include "dcdbdaemon.h"
#include "sensorcache.h"
#include "simplemqttserver.h"
#include "statistics.h"
#include "callback.h"

//#define __STDC_FORMAT_MACROS
//#include <inttypes.h>

/**
 * Uncomment and recompile to activate CollectAgent's special benchmark mode.
 * In this mode, all received messages will be discarded and no data is stored
 * in the storage backend.
 */
//#define BENCHMARK_MODE

using namespace std;

bool                                             newAutoPub = false;
int                                              keepRunning;
int                                              retCode = EXIT_SUCCESS;
Statistics *                                     stats;
SensorCache                                      mySensorCache;
AnalyticsController *                            analyticsController;
DCDB::Connection *                               dcdbConn;
DCDB::SensorDataStore *                          mySensorDataStore;
DCDB::JobDataStore *                             myJobDataStore;
DCDB::SensorConfig *                             mySensorConfig;
DCDB::CaliEvtDataStore *                         myCaliEvtDataStore;
MetadataStore *                                  metadataStore;
CARestAPI *                                      httpsServer = nullptr;
DCDB::SCError                                    err;
QueryEngine *                                    _queryEngine;
boost::shared_ptr<boost::asio::io_context::work> keepAliveWork;
logger_t                                         lg;

/* Normal termination (SIGINT, CTRL+C) */
void sigHandler(int sig) {
	boost::log::sources::severity_logger<boost::log::trivial::severity_level> lg;
	if (sig == SIGINT) {
		LOG(fatal) << "Received SIGINT";
		retCode = EXIT_SUCCESS;
	} else if (sig == SIGTERM) {
		LOG(fatal) << "Received SIGTERM";
		retCode = EXIT_SUCCESS;
	} else if (sig == SIGUSR1) {
		LOG(fatal) << "Received SIGUSR1 via REST API";
		retCode = !httpsServer ? EXIT_SUCCESS : httpsServer->getReturnCode();
	}

	keepAliveWork.reset();
	keepRunning = 0;
}

/* Crash */
void abrtHandler(int sig) {
	abrt(EXIT_FAILURE, SIGNAL);
}


/*
 * Print usage information
 */
void usage() {
	/*
	     1         2         3         4         5         6         7         8
   012345678901234567890123456789012345678901234567890123456789012345678901234567890
   */
	cout << "Usage:" << endl;
	cout << "  collectagent [-d] [-s] [-x] [-a] [-m<host>] [-c<host>] [-u<username>] [-p<password>] [-t<ttl>] "
		"[-v<verbosity>] <config>"
	     << endl;
	cout << "  collectagent -h" << endl;
	cout << endl;

	cout << "Options:" << endl;
	cout << "  -m<host>      MQTT listen address     [default: " << DEFAULT_LISTENHOST << ":" << DEFAULT_LISTENPORT << "]" << endl;
	cout << "  -c<host>      Cassandra host          [default: " << DEFAULT_CASSANDRAHOST << ":" << DEFAULT_CASSANDRAPORT << "]" << endl;
	cout << "  -u<username>  Cassandra username      [default: none]" << endl;
	cout << "  -p<password>  Cassandra password      [default: none]" << endl;
	cout << "  -t<ttl>       Cassandra insert TTL    [default: " << DEFAULT_CASSANDRATTL << "]" << endl;
	cout << "  -v<level>     Set verbosity of output [default: " << DEFAULT_LOGLEVEL << "]" << endl
	     << "                Can be a number between 5 (all) and 0 (fatal)." << endl;
	cout << endl;
	cout << "  -d            Daemonize" << endl;
	cout << "  -s            Print message stats" << endl;
	cout << "  -x            Parse and print the config but do not actually start collectagent" << endl;
	cout << "  -a			   Enable sensor auto-publish" << endl;
	cout << "  -h            This help page" << endl;
	cout << endl;
}

int main(int argc, char *const argv[]) {
	cout << "CollectAgent " << VERSION << " (libdcdb " << DCDB::Version::getVersion() << ")" << endl << endl;

	try {

		// Checking if path to config is supplied
		if (argc <= 1) {
			cout << "Please specify a path to the config-directory or a config-file" << endl << endl;
			usage();
			exit(EXIT_FAILURE);
		}

		// Defining options
		const char *opts = "m:r:c:C:u:p:t:v:dDsaxh";

		// Same mechanism as in DCDBPusher - checking if help string is requested before loading config
		int ret;
		while ((ret = getopt(argc, argv, opts)) != -1) {
			switch (ret) {
				case 'h':
					usage();
					exit(EXIT_FAILURE);
					break;
				default:
					// do nothing (other options are read later on)
					break;
			}
		}

		initLogging();
		auto cmdSink = setupCmdLogger();

		Configuration config(argv[argc - 1], "collectagent.conf");
		config.readConfig();

		// References to shorten access to config parameters
		Configuration &      settings = config;
		cassandra_t &        cassandraSettings = config.cassandraSettings;
		pluginSettings_t &   pluginSettings = config.pluginSettings;
		serverSettings_t &   restAPISettings = config.restAPISettings;
		analyticsSettings_t &analyticsSettings = config.analyticsSettings;

		optind = 1;
		while ((ret = getopt(argc, argv, opts)) != -1) {
			switch (ret) {
				case 'a':
					pluginSettings.autoPublish = true;
					break;
				case 'm':
					settings.mqttListenHost = parseNetworkHost(optarg);
					settings.mqttListenPort = parseNetworkPort(optarg);
					if (settings.mqttListenPort == "")
						settings.mqttListenPort = string(DEFAULT_LISTENPORT);
					break;
				case 'c':
					cassandraSettings.host = parseNetworkHost(optarg);
					cassandraSettings.port = parseNetworkPort(optarg);
					if (cassandraSettings.port == "")
						cassandraSettings.port = string(DEFAULT_CASSANDRAPORT);
					break;
				case 'u':
					cassandraSettings.username = optarg;
					break;
				case 'p': {
					cassandraSettings.password = optarg;
					// What does this do? Mask the password?
					size_t pwdLen = strlen(optarg);
					memset(optarg, 'x', (pwdLen >= 3) ? 3 : pwdLen);
					if (pwdLen > 3) {
						memset(optarg + 3, 0, pwdLen - 3);
					}
					break;
				}
				case 't':
					cassandraSettings.ttl = stoul(optarg);
					break;
				case 'v':
					settings.logLevelCmd = stoi(optarg);
					break;
				case 'd':
				case 'D':
					settings.daemonize = 1;
					break;
				case 's':
					settings.statisticsInterval = 1;
					break;
				case 'x':
					settings.validateConfig = true;
					break;
				case 'h':
				default:
					usage();
					exit(EXIT_FAILURE);
			}
		}

		libConfig.init();
		libConfig.setTempDir(pluginSettings.tempdir);

		// set up logger to file
		if (settings.logLevelFile >= 0) {
			auto fileSink = setupFileLogger(pluginSettings.tempdir, std::string("collectagent"));
			fileSink->set_filter(boost::log::trivial::severity >= translateLogLevel(settings.logLevelFile));
		}

		// severity level may be overwritten (per option or config-file) --> set it according to globalSettings
		if (settings.logLevelCmd >= 0) {
			cmdSink->set_filter(boost::log::trivial::severity >= translateLogLevel(settings.logLevelCmd));
		}

		/*
		 * Catch SIGINT and SIGTERM signals to allow for proper server shutdowns.
		 */
		signal(SIGINT, sigHandler);
		signal(SIGTERM, sigHandler);
		signal(SIGUSR1, sigHandler);

		/*
		 * Catch critical signals to allow for backtraces
		 */
		signal(SIGABRT, abrtHandler);
		signal(SIGSEGV, abrtHandler);

		// Daemonizing the collectagent
		if (settings.daemonize)
			dcdbdaemon();

		// Setting the size of the sensor cache
		// Conversion from milliseconds to nanoseconds
		mySensorCache.setMaxHistory(uint64_t(pluginSettings.cacheInterval) * 1000000);

		// Allocate and initialize connection to Cassandra.
		dcdbConn =
		    new DCDB::Connection(cassandraSettings.host, atoi(cassandraSettings.port.c_str()), cassandraSettings.username, cassandraSettings.password);
		dcdbConn->setNumThreadsIo(cassandraSettings.numThreadsIo);
		dcdbConn->setQueueSizeIo(cassandraSettings.queueSizeIo);
		uint32_t params[2] = {cassandraSettings.coreConnPerHost, cassandraSettings.shardAwareness};
		dcdbConn->setBackendParams(params);

		int retries = 3;
		while(retries--) {
			if (!dcdbConn->connect()) {
				LOG(fatal) << "Cannot connect to Cassandra!";
				if (retries > 0) {
					LOG(info) << "Retries left: " << retries;
					sleep(10);
				} else {
					exit(EXIT_FAILURE);
				}
			} else {
				break;
			}
		}

		/*
		 * Legacy behavior: Initialize the DCDB schema in Cassandra.
		 */
		dcdbConn->initSchema();

		/*
		 * Allocate the SensorDataStore.
		 */
		mySensorDataStore = new DCDB::SensorDataStore(dcdbConn);
		mySensorConfig = new DCDB::SensorConfig(dcdbConn);
		myJobDataStore = new DCDB::JobDataStore(dcdbConn);
		myCaliEvtDataStore = new DCDB::CaliEvtDataStore(dcdbConn);

		/*
		 * Set TTL for data store inserts if TTL > 0.
		 */
		if (cassandraSettings.ttl > 0) {
			mySensorDataStore->setTTL(cassandraSettings.ttl);
			myCaliEvtDataStore->setTTL(cassandraSettings.ttl);
		}
		mySensorDataStore->setDebugLog(cassandraSettings.debugLog);
		myCaliEvtDataStore->setDebugLog(cassandraSettings.debugLog);

		// Fetching public sensor information from the Cassandra datastore
		list<DCDB::PublicSensor> publicSensors;
		if (mySensorConfig->getPublicSensorsVerbose(publicSensors) != SC_OK) {
			LOG(error) << "Failed to retrieve public sensors!";
			exit(EXIT_FAILURE);
		}

		metadataStore = new MetadataStore();
		SensorMetadata sBuf;
		for (const auto &s : publicSensors)
			if (!s.is_virtual) {
				sBuf = DCDB::PublicSensor::publicSensorToMetadata(s);
				if (sBuf.isValid())
					metadataStore->store(*sBuf.getPublicName(), sBuf);
			}
		publicSensors.clear();

		boost::asio::io_context io;
		boost::thread_group     threads;

		_queryEngine = new QueryEngine;
		_queryEngine->setFilter(analyticsSettings.filter);
		_queryEngine->setJobFilter(analyticsSettings.jobFilter);
		_queryEngine->setJobMatch(analyticsSettings.jobMatch);
		_queryEngine->setJobIDFilter(analyticsSettings.jobIdFilter);
		_queryEngine->setJobDomainId(analyticsSettings.jobDomainId);
		_queryEngine->setSensorHierarchy(analyticsSettings.hierarchy);
		_queryEngine->setQueryCallback(sensorQueryCallback);
		_queryEngine->setGroupQueryCallback(sensorGroupQueryCallback);
		_queryEngine->setMetadataQueryCallback(metadataQueryCallback);
		_queryEngine->setJobQueryCallback(jobQueryCallback);

		analyticsController = new AnalyticsController(mySensorConfig, mySensorDataStore, io);
		analyticsController->setCache(&mySensorCache);
		analyticsController->setMetadataStore(metadataStore);
		if (!analyticsController->initialize(settings))
			return EXIT_FAILURE;

		LOG_LEVEL vLogLevel = settings.validateConfig ? LOG_LEVEL::info : LOG_LEVEL::debug;
		LOG_VAR(vLogLevel) << "-----  Configuration  -----";

		// print global settings in either case
		LOG(info) << "Global Settings:";
		LOG(info) << "    MQTT-listenAddress: " << settings.mqttListenHost << ":" << settings.mqttListenPort;
		LOG(info) << "    CacheInterval:      " << int(pluginSettings.cacheInterval / 1000) << " [s]";
		LOG(info) << "    CleaningInterval:   " << settings.cleaningInterval << " [s]";
		LOG(info) << "    Threads:            " << settings.threads;
		LOG(info) << "    MessageThreads:     " << settings.messageThreads;
		LOG(info) << "    MessageSlots:       " << settings.messageSlots;
		LOG(info) << "    Daemonize:          " << (settings.daemonize ? "Enabled" : "Disabled");
		LOG(info) << "    StatisticsInterval: " << settings.statisticsInterval << " [s]";
		LOG(info) << "    StatisticsMqttPart: " << settings.statisticsMqttPart;
		LOG(info) << "    MQTT-prefix:        " << pluginSettings.mqttPrefix;
		LOG(info) << "    Auto-publish:       " << (pluginSettings.autoPublish ? "Enabled" : "Disabled");
		LOG(info) << "    Write-Dir:          " << pluginSettings.tempdir;
		LOG(info) << "    LogLevelFile:       " << settings.logLevelFile;
		LOG(info) << "    LogLevelCmd:        " << settings.logLevelCmd;
		LOG(info) << (settings.validateConfig ? "    Only validating config files." : "    ValidateConfig:     Disabled");

		LOG(info) << "Analytics Settings:";
		LOG(info) << "    Hierarchy:          " << (analyticsSettings.hierarchy != "" ? analyticsSettings.hierarchy : "none");
		LOG(info) << "    Filter:             " << (analyticsSettings.filter != "" ? analyticsSettings.filter : "none");
		LOG(info) << "    Job Filter:         " << (analyticsSettings.jobFilter != "" ? analyticsSettings.jobFilter : "none");
		LOG(info) << "    Job Match:          " << (analyticsSettings.jobMatch != "" ? analyticsSettings.jobMatch : "none");
		LOG(info) << "    Job ID Filter:      " << (analyticsSettings.jobIdFilter != "" ? analyticsSettings.jobIdFilter : "none");
		LOG(info) << "    Job Domain ID:      " << analyticsSettings.jobDomainId;

		LOG(info) << "Cassandra Driver Settings:";
		LOG(info) << "    Address:            " << cassandraSettings.host << ":" << cassandraSettings.port;
		LOG(info) << "    TTL:                " << cassandraSettings.ttl;
		LOG(info) << "    NumThreadsIO:       " << cassandraSettings.numThreadsIo;
		LOG(info) << "    QueueSizeIO:        " << cassandraSettings.queueSizeIo;
		LOG(info) << "    CoreConnPerHost:    " << cassandraSettings.coreConnPerHost;
		LOG(info) << "    ShardAwareness:     " << (cassandraSettings.shardAwareness ? "Enabled" : "Disabled");
		LOG(info) << "    DebugLog:           " << (cassandraSettings.debugLog ? "Enabled" : "Disabled");
#ifdef SimpleMQTTVerbose
		LOG(info) << "    Username:           " << cassandraSettings.username;
		LOG(info) << "    Password:           " << cassandraSettings.password;
#else
		LOG(info) << "    Username and password not printed.";
#endif

		if (restAPISettings.enabled) {

			LOG(info) << "RestAPI Settings:";
			LOG(info) << "    REST Server: " << restAPISettings.host << ":" << restAPISettings.port;
			LOG(info) << "    Certificate: " << restAPISettings.certificate;
			LOG(info) << "    Private key file: " << restAPISettings.privateKey;

			if (config.influxSettings.measurements.size() > 0) {
				LOG(info) << "InfluxDB Settings:";
				LOG(info) << "    MQTT-Prefix:  " << config.influxSettings.mqttPrefix;
				LOG(info) << "    Auto-Publish: " << (config.influxSettings.publish ? "Enabled" : "Disabled");

				for (auto &m : config.influxSettings.measurements) {
					LOG(info) << "    Measurement: " << m.first;
					LOG(info) << "        MQTT-Part:   " << m.second.mqttPart;
					LOG(info) << "        Tag:         " << m.second.tag;
					if ((m.second.tagRegex.size() > 0) && (m.second.tagSubstitution.size() > 0)) {
						if (m.second.tagSubstitution != "&") {
							LOG(info)
							    << "        TagFilter:   s/" << m.second.tagRegex.str() << "/" << m.second.tagSubstitution << "/";
						} else {
							LOG(info) << "    TagFilter:   " << m.second.tagRegex.str();
						}
					}
					if (m.second.fields.size() > 0) {
						stringstream ss;
						copy(m.second.fields.begin(), m.second.fields.end(), ostream_iterator<std::string>(ss, ","));
						string fields = ss.str();
						fields.pop_back();
						LOG(info) << "        Fields:      " << fields;
					}
				}
			}
		}
		LOG_VAR(vLogLevel) << "-----  Analytics Configuration  -----";
		for (auto &p : analyticsController->getManager()->getPlugins()) {
			LOG_VAR(vLogLevel) << "Operator Plugin \"" << p.id << "\"";
			p.configurator->printConfig(vLogLevel);
		}
		LOG_VAR(vLogLevel) << "-----  End Configuration  -----";

		if (settings.validateConfig)
			return EXIT_SUCCESS;

		LOG(info) << "Creating threads...";
		// Dummy to keep io service alive even if no tasks remain (e.g. because all sensors have been stopped
		// over REST API) Inherited from DCDB Pusher
		keepAliveWork = boost::make_shared<boost::asio::io_context::work>(io);
		// Create pool of threads which handle the sensors
		for (size_t i = 0; i < settings.threads; i++) {
			threads.create_thread(bind(static_cast<size_t (boost::asio::io_context::*)()>(&boost::asio::io_context::run), &io));
		}
		LOG(info) << "Threads created!";

		analyticsController->start();
		LOG(info) << "AnalyticsController running...";

		/*
		 * Start the MQTT Message Server.
		 */
		SimpleMQTTServer ms(settings.mqttListenHost, settings.mqttListenPort, settings.messageThreads, settings.messageSlots);

		ms.setMessageCallback(mqttCallback);
		ms.start();

		LOG(info) << "MQTT Server running...";

		/*
		 * Start the HTTP Server for the REST API
		 */
		if (restAPISettings.enabled) {
			httpsServer = new CARestAPI(restAPISettings, &config.influxSettings, &mySensorCache, mySensorDataStore, mySensorConfig,
						    analyticsController, &ms, io);
			config.readRestAPIUsers(httpsServer);
			httpsServer->start();
			LOG(info) << "HTTP Server running...";
		}

		/*
		 * Run (hopefully) forever...
		 */
		keepRunning = 1;
		uint64_t start = getTimestamp();
		uint64_t lastCleanup = start;
		uint64_t sleepInterval = (settings.statisticsInterval > 0) ? settings.statisticsInterval : 60;

		if (settings.statisticsInterval) {
			stats = new Statistics(mySensorConfig, mySensorDataStore, pluginSettings.mqttPrefix + settings.statisticsMqttPart);
			if (pluginSettings.autoPublish) {
				stats->publishSensorNames();
				newAutoPub = true;
			}
		}

		LOG(info) << "Collect Agent running...";

		while (keepRunning) {
			start = getTimestamp();
			if (NS_TO_S(start) - NS_TO_S(lastCleanup) > settings.cleaningInterval) {
				uint64_t purged = mySensorCache.clean(S_TO_NS(settings.cleaningInterval));
				lastCleanup = start;
				if (purged > 0)
					LOG(info) << "Cache: purged " << purged << " obsolete entries";
			}
			if (newAutoPub) {
				newAutoPub = false;
				mySensorConfig->setPublishedSensorsWritetime(getTimestamp());
			}

			sleep(sleepInterval);

			if ((stats != nullptr) && keepRunning) {
				stats->set(Statistics::CONNECTED_HOSTS, ms.getConnectedHostsCount(stats->getPrevTs()));
				stats->set(Statistics::ANALYTICS_INSERTS, analyticsController->getReadingCtr());
				if (restAPISettings.enabled) {
					stats->set(Statistics::REST_INSERTS, httpsServer->getInfluxCounter());
				}
				stats->publishStats();
			}
		}

		LOG(info) << "Stopping...";
		ms.stop();
		LOG(info) << "MQTT Server stopped...";
		if (restAPISettings.enabled) {
			httpsServer->stop();
			LOG(info) << "HTTP Server stopped...";
		}
		analyticsController->stop();
		delete mySensorDataStore;
		delete myJobDataStore;
		delete mySensorConfig;
		delete myCaliEvtDataStore;
		dcdbConn->disconnect();
		delete dcdbConn;
		delete metadataStore;
		delete analyticsController;
		if (restAPISettings.enabled) {
			delete httpsServer;
		}
		LOG(info) << "Collect Agent closed. Bye bye...";
	} catch (const std::runtime_error &e) {
		LOG(fatal) << e.what();
		return EXIT_FAILURE;
	} catch (const exception &e) {
		LOG(fatal) << "Exception: " << e.what();
		abrt(EXIT_FAILURE, INTERR);
	}

	return retCode;
}
