#include <string>

#include "../analytics/includes/QueryEngine.h"
#include <dcdb/sensordatastore.h>
#include <dcdb/jobdatastore.h>
#include <dcdb/calievtdatastore.h>
#include "simplemqttserver.h"
#include "sensorcache.h"
#include "statistics.h"

using namespace DCDB;
bool jobQueryCallback(const std::string &jobId, const uint64_t startTs, const uint64_t endTs, std::vector<qeJobData> &buffer, const bool rel, const bool range, const std::string &domainId);
bool sensorGroupQueryCallback(const std::vector<std::string> &names, const uint64_t startTs, const uint64_t endTs, std::vector<reading_t> &buffer, const bool rel, const uint64_t tol);
bool sensorQueryCallback(const std::string &name, const uint64_t startTs, const uint64_t endTs, std::vector<reading_t> &buffer, const bool rel, const uint64_t tol);
bool metadataQueryCallback(const std::string &name, SensorMetadata &buffer);
int mqttCallback(SimpleMQTTMessage *msg);
