//================================================================================
// Name        : statistics.cpp
// Author      : Michael Ott
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Class for keeping statistics of CollectAgent activitiy
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019-2023 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "statistics.h"

const char* statsStr[8] = {"msgsRcvd", "readingsRcvd", "cachedQueries", "missedQueries", "dbQueries", "hosts", "restInserts", "analyticsInserts"};

void Statistics::reset() {
    for (unsigned int i = 0; i < _statsCounter.size(); i++) {
        _statsCounter[i] = 0;
    }
}

void Statistics::increase(statsType type, uint64_t count ) {
    _statsCounter[type]+= count;
}

void Statistics::set(statsType type, uint64_t count ) {
    _statsCounter[type] = count;
}

void Statistics::publishSensorNames() {
    for (auto s : statsStr) {
		std::string topic = _mqttPrefix + "/" + s;
		_sensorConfig->publishSensor(topic.c_str());
    }
}

void Statistics::publishStats() {
    uint64_t ts = getTimestamp();
    uint64_t elapsed = (float)(NS_TO_S(ts) - NS_TO_S(_prevTs));
    float    aIns = ceil(((float)_statsCounter[ANALYTICS_INSERTS]) / elapsed);
    float    cacheReq = ceil(((float)_statsCounter[CACHED_QUERIES]) / elapsed);
    float    missesReq = ceil(((float)_statsCounter[MISSED_QUERIES]) / elapsed);
    float    dbReq = ceil(((float)_statsCounter[DB_QUERIES]) / elapsed);
    float    rIns = ceil(((float)_statsCounter[REST_INSERTS]) / elapsed);
    float    mIns = ceil(((float)_statsCounter[READINGS_RCVD]) / elapsed);
    float    mMsg = ceil(((float)_statsCounter[MSGS_RCVD]) / elapsed);
    LOG(info) << "Performance: MQTT [" << std::fixed << std::setprecision(0) << mIns << " ins/s|" << mMsg << " msg/s]"
	      << "   REST [" << rIns << " ins/s]   Analytics [" << aIns << " ins/s]"
	      << "   Cache [" << cacheReq << " req/s]   DB [" << dbReq << " req/s] Miss [" << missesReq << " req/s]";
    LOG(info) << "Connected hosts: " << _statsCounter[CONNECTED_HOSTS];

    for (unsigned int i = 0; i < _statsCounter.size(); i++) {
		DCDB::SensorDataStoreReading reading = DCDB::SensorDataStoreReading(DCDB::SensorId(_mqttPrefix + "/" + statsStr[i]), ts, _statsCounter[i]);
		_statsCounter[i] = 0;
		_sensorDataStore->insert(reading);
    }
    _prevTs = ts;
}