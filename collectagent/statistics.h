//================================================================================
// Name        : statistics.h
// Author      : Michael Ott
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Class for keeping statistics of CollectAgent activitiy
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019-2023 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef COLLECTAGENT_STATISTICS_H_
#define COLLECTAGENT_STATISTICS_H_

#include <string>
#include <vector>

#include <dcdb/sensorconfig.h>
#include <dcdb/sensordatastore.h>
#include "timestamp.h"

class Statistics {
      public:
	enum statsType { MSGS_RCVD = 0, READINGS_RCVD, CACHED_QUERIES, MISSED_QUERIES, DB_QUERIES, CONNECTED_HOSTS, REST_INSERTS, ANALYTICS_INSERTS };

	Statistics(DCDB::SensorConfig* sensorConfig, DCDB::SensorDataStore* sensorDataStore, std::string mqttPrefix): _sensorConfig(sensorConfig), _sensorDataStore(sensorDataStore), _mqttPrefix(mqttPrefix) {
        _statsCounter = std::vector<uint64_t>(8, 0);
        _prevTs = getTimestamp();
    }

    void setMqttPrefix(std::string mqttPrefix) {
        _mqttPrefix = mqttPrefix;
    }
	void reset();
	void increase(statsType type, uint64_t count = 1);
    void set(statsType type, uint64_t count);
    uint64_t getPrevTs() { return _prevTs; };

    void publishSensorNames();
    void publishStats();

      protected:
	std::vector<uint64_t>    _statsCounter;
    DCDB::SensorConfig*      _sensorConfig;
    DCDB::SensorDataStore*   _sensorDataStore;
    std::string              _mqttPrefix;
    uint64_t                 _prevTs;
    boost::log::sources::severity_logger<boost::log::trivial::severity_level> lg;

};
#endif