//================================================================================
// Name        : JsonExporterOperator.h
// Author      : Michael Ott
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description :
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef PROJECT_JSONEXPORTEROPERATOR_H
#define PROJECT_JSONEXPORTEROPERATOR_H


#include "../../includes/OperatorTemplate.h"
#include "sensorbase.h"
#include <math.h>
#include <algorithm>

/**
 * @brief JsonExporter operator plugin.
 *
 * @ingroup jsonexporter
 */
class JsonExporterOperator : virtual public OperatorTemplate<SensorBase> {

public:

    JsonExporterOperator(const std::string& name);
    JsonExporterOperator(const JsonExporterOperator& other);

    virtual ~JsonExporterOperator();
    
    void setFileName(std::string &s) { _fileName = s; }
    void setNodeRegex(boost::regex &regex) { _nodeRegex = regex; }
    void setNodeSubstitution(const std::string &substitution) { _nodeSubstitution = substitution; }
    void setDeviceRegex(boost::regex &regex) { _deviceRegex = regex; }
    void setDeviceSubstitution(const std::string &substitution) { _deviceSubstitution = substitution; }
    void setSingleTimestamp(const bool singleTimestamp) { _singleTimestamp = singleTimestamp; }

    std::string       getFileName() const { return _fileName; }
    boost::regex      getNodeRegex() const { return _nodeRegex; }
    const std::string getNodeSubstitution() const { return _nodeSubstitution; }
    boost::regex      getDeviceRegex() const { return _deviceRegex; }
    const std::string getDeviceSubstitution() const { return _deviceSubstitution; }
    bool              getSingleTimestamp() const { return _singleTimestamp; }

    void printConfig(LOG_LEVEL ll) override;

protected:
    virtual void compute(U_Ptr unit)	 override;
    virtual void computeAsync()	override;
    
    vector<reading_t> _buffer;
    std::string _fileName;
    boost::regex _nodeRegex;
    std::string  _nodeSubstitution;
    boost::regex _deviceRegex;
    std::string  _deviceSubstitution;
    bool         _singleTimestamp;
};


#endif //PROJECT_JSONEXPORTEROPERATOR_H
