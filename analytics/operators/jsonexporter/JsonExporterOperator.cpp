//================================================================================
// Name        : JsonExporterOperator.cpp
// Author      : Alessio Netti
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description :
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "JsonExporterOperator.h"

JsonExporterOperator::JsonExporterOperator(const std::string& name) : OperatorTemplate(name) {
	_singleTimestamp = false;
}

JsonExporterOperator::JsonExporterOperator(const JsonExporterOperator& other) : OperatorTemplate(other) {
	_singleTimestamp = other._singleTimestamp;
}

JsonExporterOperator::~JsonExporterOperator() {}

void JsonExporterOperator::printConfig(LOG_LEVEL ll) {
    LOG_VAR(ll) << "            NodeRegex:       " << _nodeRegex.str();
    LOG_VAR(ll) << "            NodeSubst:       " << _nodeSubstitution;
    LOG_VAR(ll) << "            DeviceRegex:     " << _deviceRegex.str();
    LOG_VAR(ll) << "            DeviceSubst:     " << _deviceSubstitution;
	LOG_VAR(ll) << "            SingleTimestamp: " << bool_to_str(_singleTimestamp);

    OperatorTemplate<SensorBase>::printConfig(ll);
}

void JsonExporterOperator::compute(U_Ptr unit) {
    
}

void JsonExporterOperator::computeAsync() {
    typedef struct {
	    int64_t     value;
	    uint64_t    timestamp;
	    std::string unit;

    } jsonreading_t;
    std::map<std::tuple<std::string, std::string, std::string>, jsonreading_t> readings;
    for (unsigned i = 0; i < _units.size(); i++) {
	    for (unsigned int j = 0; j < _units[i]->getInputs().size(); j++) {
		    std::string inputName = _units[i]->getInputs()[j]->getName();
		    std::string node, device;
		    boost::regex_replace(std::back_inserter(node), inputName.begin(), inputName.end(), _nodeRegex, _nodeSubstitution.c_str(), boost::regex_constants::format_sed | boost::regex_constants::format_no_copy);
		    boost::regex_replace(std::back_inserter(device), inputName.begin(), inputName.end(), _deviceRegex, _deviceSubstitution.c_str(), boost::regex_constants::format_sed | boost::regex_constants::format_no_copy);

		    // Clearing the buffer
		    _buffer.clear();
		    if (!_queryEngine->querySensor(inputName, 0, 0, _buffer) || _buffer.empty()) {
			    LOG(debug) << "Operator " + _name + ": cannot read from sensor " + inputName + "!";
		    } else {
			    size_t        pos = inputName.find_last_of("/");
			    std::string   name = pos == string::npos ? inputName : inputName.substr(pos + 1);
			    jsonreading_t r;
			    r.value = _buffer[_buffer.size() - 1].value;
			    r.timestamp = _buffer[_buffer.size() - 1].timestamp;
			    if (_units[i]->getInputs()[j]->getMetadata() && _units[i]->getInputs()[j]->getMetadata()->getUnit()) {
				    r.unit = *(_units[i]->getInputs()[j]->getMetadata()->getUnit());
			    }
			    readings[std::make_tuple(node, device, name)] = r;
		    }
	    }
    }

    if (!readings.empty()) {
	    std::string                 nodeName, deviceName;
	    boost::property_tree::ptree nodes, node, devices, device;
		uint64_t tsSum(0), tsCount(0);
	    for (auto &r : readings) {
		    if (nodeName != std::get<0>(r.first)) {
			    if (!deviceName.empty()) {
				    devices.push_back(std::make_pair("", device));
				    device.clear();
			    }
			    if (!nodeName.empty()) {
				    node.add_child("device", devices);
					if (_singleTimestamp) {
						node.add("timestamp", tsSum/tsCount);
						tsSum = tsCount = 0;
					}
				    devices.clear();
				    nodes.add_child(nodeName, node);
				    node.clear();
			    }
			    nodeName = std::get<0>(r.first);
			    // node.add("name", nodeName);
			    deviceName = "";
		    }
		    if (deviceName != std::get<1>(r.first)) {
			    if (!deviceName.empty()) {
				    devices.push_back(std::make_pair("", device));
				    device.clear();
			    }
			    deviceName = std::get<1>(r.first);
			    device.add("name", deviceName);
		    }

		    boost::property_tree::ptree reading;
		    if (_singleTimestamp) {
			    tsSum += NS_TO_S(r.second.timestamp);
			    tsCount++;
		    } else {
			    reading.add("timestamp", r.second.timestamp);
		    }
		    reading.add("value", r.second.value);
		    if (!r.second.unit.empty()) {
			    reading.add("unit", r.second.unit);
		    }
		    if (deviceName.empty()) {
			    node.push_back(std::make_pair(std::get<2>(r.first), reading));
		    } else {
			    device.push_back(std::make_pair(std::get<2>(r.first), reading));
		    }
	    }
	    if (!deviceName.empty()) {
		    devices.push_back(std::make_pair("", device));
	    }
	    node.add_child("device", devices);
		if (_singleTimestamp) {
			node.add("timestamp", tsSum/tsCount);
		}
	    nodes.add_child(nodeName, node);

	    std::ofstream outFile;
	    std::string   tmpFileName = _fileName + ".tmp";
	    outFile.open(tmpFileName);
	    if (outFile.is_open()) {
		    boost::property_tree::write_json(outFile, nodes, true);
		    outFile.close();
		    if (rename(tmpFileName.c_str(), _fileName.c_str()) != 0) {
			    LOG(error) << "Error renaming temporary output file " << tmpFileName << " => " << _fileName;
		    }
	    } else {
		    LOG(error) << "Error opening temporary output file " << tmpFileName;
	    }
    }

    if (_timer && _keepRunning && !_disabled) {
	    _scheduledTime = nextReadingTime();
	    _timer->expires_at(timestamp2ptime(_scheduledTime));
	    _pendingTasks++;
	    _timer->async_wait(bind(&JsonExporterOperator::computeAsync, this));
    }
    _pendingTasks--;
}
