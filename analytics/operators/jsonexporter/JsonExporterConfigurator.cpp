//================================================================================
// Name        : JsonExporterConfigurator.cpp
// Author      : Alessio Netti
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description :
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "JsonExporterConfigurator.h"

JsonExporterConfigurator::JsonExporterConfigurator() : OperatorConfiguratorTemplate() {
    _operatorName = "export";
    _baseName     = "sensor";
}

JsonExporterConfigurator::~JsonExporterConfigurator() {}

void JsonExporterConfigurator::sensorBase(SensorBase& s, CFG_VAL config) {
}

bool JsonExporterConfigurator::parseRegex(std::string& str, boost::regex& regex, std::string& substitution) {
    boost::regex  checkSubstitute("s([^\\\\]{1})([\\S|\\s]*)\\1([\\S|\\s]*)\\1");
    boost::smatch matchResults;
    
    if (regex_match(str, matchResults, checkSubstitute)) {
	//input has substitute format
	regex = boost::regex(matchResults[2].str(), boost::regex_constants::extended);
	substitution = matchResults[3].str();
	return true;
    } else {
	return false;
    }
}
    
bool JsonExporterConfigurator::operatorAttributes(JsonExporterOperator& op, CFG_VAL config) {
    boost::regex regex;
    std::string substitution;
    
    for (auto &val: config) {
	if(boost::iequals(val.first, "fileName")) {
	    op.setFileName(val.second.data());
	} else if(boost::iequals(val.first, "nodeRegex")) {
	    if (parseRegex(val.second.data(), regex, substitution)) {
		op.setNodeRegex(regex);
		op.setNodeSubstitution(substitution);
	    } else {
		LOG(error) << _operatorName << ": NodeRegex does not follow sed syntax";
		return false;
	    }
	} else if(boost::iequals(val.first, "deviceRegex")) {
	    if (parseRegex(val.second.data(), regex, substitution)) {
		op.setDeviceRegex(regex);
		op.setDeviceSubstitution(substitution);
	    } else {
		LOG(error) << _operatorName << ": DeviceRegex does not follow sed syntax";
		return false;
	    }
	} else if(boost::iequals(val.first, "singleTimestamp")) {
		op.setSingleTimestamp(to_bool(val.second.data()));
    }
	}

    if (!op.getTemplate() && op.getNodeSubstitution().empty()) {
	LOG(error) << _operatorName << ": NodeRegex has not been set but is required";
	return false;
    }
    return true;
}

bool JsonExporterConfigurator::unit(UnitTemplate<SensorBase>& u) {
    if(u.isTopUnit()) {
        LOG(error) << "    " << _operatorName << ": This operator type only supports flat units!";
        return false;
    }
    if(u.getOutputs().size() > 1) {
        LOG(error) << "    " << _operatorName << ": This is a json exporter, it can have only one output sensor defined!";
        return false;
    }
    return true;
}
