import dcdb
import sys
import pandas as pd

if len(sys.argv) < 4:
    print('dcdbquery.py <Sensor 1> [<Sensor 2> ...] <Start> <End>')

start = sys.argv[len(sys.argv)-2]
end = sys.argv[len(sys.argv)-1]
sensors = sys.argv[1:len(sys.argv)-2]

result = pd.DataFrame()
dcdb.connect()
for s in sensors:
    df=dcdb.query(s, start, end)
    df.insert(loc=0, column="sensor", value=s)
    df.reset_index(inplace=True)
    result = pd.concat([result, df])
dcdb.disconnect()
print(result.to_csv(columns=['sensor', 'ts', 'value'], index=False))
