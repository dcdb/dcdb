from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.query import BatchStatement
from cassandra.query import BatchType
from cassandra.query import ConsistencyLevel
import datetime
import pandas as pd
import os

_session = None
_cluster = None

version = 2

def pandas_factory(colnames, rows):
    df = pd.DataFrame(rows, columns=colnames)
    df.set_index('ts', inplace=True)
    df.index = pd.to_datetime(df.index, utc=True)
    return df

def list_factory(colnames, rows):
    list = []
    for r in rows:
        list.append(r[0])
    return list

def connect(hostname='localhost', port=9042, user='', password=''):
    # Check for DCDB_HOSTNAME environment if no hostname has been passed
    if hostname is connect.__defaults__[0]:
        hostname = os.environ.get("DCDB_HOSTNAME", "localhost")

    auth_provider = None
    if user != '' and password != '':
         auth_provider = PlainTextAuthProvider(username=user, password=password)
    global _cluster
    _cluster = Cluster(contact_points=[hostname], port=port, auth_provider=auth_provider)
    global _session
    _session = _cluster.connect()
    _session.default_fetch_size = None

def disconnect():
     global _cluster
     _cluster.shutdown
     _cluster = None
     global _session
     _session = None

def compute_ws(ts):
    return int(ts / (604800 * 1000000000))

def getTimestamp(value):
    if value.isdigit():
        i = int(value)
        if i < 31872923400:
            ts = pd.Timestamp(i, unit='s')
        elif i < 31872923400000:
            ts = pd.Timestamp(i, unit='ms')
        elif i < 31872923400000000:
            ts = pd.Timestamp(i, unit='us')
        else:
            ts = pd.Timestamp(i, unit='ns')
    else:
        ts = pd.Timestamp(value)
        if ts.tzinfo is None:
            ts = ts.tz_localize(datetime.datetime.now().astimezone().tzinfo)
    return ts

def query(sensorname, start, stop):
     _session.row_factory = pandas_factory
     start_ts = getTimestamp(start)
     stop_ts = getTimestamp(stop)
     start_ws = compute_ws(start_ts.value)
     stop_ws = compute_ws(stop_ts.value)
     query = 'SELECT ts, value FROM dcdb.sensordata WHERE sid = %s AND ws = %s AND ts >= %s AND ts <= %s ORDER BY ts ASC'

     df = pd.DataFrame()
     for ws in range(start_ws, stop_ws+1):
        result = _session.execute(query, [sensorname.ljust(16), ws, start_ts.value, stop_ts.value])
        df = pd.concat([df, result._current_rows])
     df.rename(columns={'value': sensorname}, inplace=True)       
     return df

def insert(sensorname, ts, value):
    ts = getTimestamp(ts)
    ws = compute_ws(ts.value)
    query = 'INSERT INTO dcdb.sensordata (sid, ws, ts, value) VALUES (%s, %s, %s, %s);'
    result = _session.execute(query, [sensorname.ljust(16), ws, ts.value, value])

def insertBatch(df: pd.DataFrame):
    insertQuery = _session.prepare("INSERT INTO dcdb.sensordata (sid, ws, ts, value) VALUES (?, ?, ?, ?)")
    batch = BatchStatement(consistency_level=ConsistencyLevel.QUORUM, batch_type=BatchType.UNLOGGED)

    for sensorname in df.columns:
        for (ts, value) in df[sensorname].iteritems():
            ts = pd.Timestamp(ts).value
            ws = compute_ws(ts)
            batch.add(insertQuery, (sensorname, ws, ts, value))
    _session.execute(batch)

def delete(sensorname, start, stop):
    start_ts = getTimestamp(start)
    stop_ts = getTimestamp(stop)
    start_ws = compute_ws(start_ts.value)
    stop_ws = compute_ws(stop_ts.value)
                         
    query = 'DELETE FROM dcdb.sensordata WHERE sid = %s AND ws = %s'
    for ws in range(start_ws, stop_ws+1):
        _session.execute(query, (sensorname, ws))

def getSensors():
    _session.row_factory = list_factory
    query = "SELECT name FROM dcdb_config.publishedsensors;"
    result = _session.execute(query)
    return result._current_rows
