//================================================================================
// Name        : likwidConfigurator.h
// Author      : Amir Raoofy
// Contact     :
// Copyright   :
// Description : Header file for likwid plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef LIKWID_LIKWIDCONFIGURATOR_H_
#define LIKWID_LIKWIDCONFIGURATOR_H_

#include "../../includes/ConfiguratorTemplate.h"
#include "likwidSensorGroup.h"
#include <unordered_map>
#include <mutex>

/**
 * @brief ConfiguratorTemplate specialization for this plugin.
 *
 * @ingroup likwid
 */
class likwidConfigurator : public ConfiguratorTemplate<likwidSensorBase, likwidSensorGroup> {

	typedef std::map<std::string, std::set<int>> templateCpuMap_t;
	typedef std::map<std::string, unsigned>      templateHTMap_t;

public:
    likwidConfigurator();
    virtual ~likwidConfigurator();

protected:
    /* Overwritten from ConfiguratorTemplate */
    bool sensorBase(likwidSensorBase& s, CFG_VAL config) override;
    bool sensorGroup(likwidSensorGroup& s, CFG_VAL config) override;
	bool readConfig(std::string cfgPath) override;

    //TODO implement if required
    //void global(CFG_VAL config) override;
    //void derivedSetGlobalSettings(const pluginSettings_t& pluginSettings) override;

    void printConfiguratorConfig(LOG_LEVEL ll) final override;

    private:
	/**
	 *  Takes a likwidSensorGroup and duplicates it for every CPU specified.
	 *  Assigns one CPU value to every newly constructed group and stores them
	 *  afterwards. Also sets everything for hyper-threading aggregation if
	 *  enabled.å
	 *
	 *  @param group    likwidSensorGroup which is to be customized for every CPU
	 *  @param cfg      Config tree for the group. Required to read in possibly
	 *                  differing CPU lists and default groups.
	 *  @param num_groups_multiplexing passed to configure the number of multiplexing
	 * 									groups for each group
	 */
	void customizeAndStore(likwidSensorGroup &group, CFG_VAL cfg, int& num_groups_multiplexing);

	/**
	* helper function to parse the config file and read the number of multiplexing 
	*  groups
	*  @param cfg      Config tree for the group.
	*/
	int parseAndReadNumberOfMultiplexingGroups (CFG_VAL cfg);

	std::unordered_map< int, int> _multiplexing_gid_map;
	std::mutex _gid_map_mutex;
	std::vector <uint64_t> _group_start_timestamps;
	
	templateCpuMap_t _templateCpus;
	templateHTMap_t  _templateHTs;
};

extern "C" ConfiguratorInterface* create() {
    return new likwidConfigurator;
}

extern "C" void destroy(ConfiguratorInterface* c) {
    delete c;
}

#endif /* LIKWID_LIKWIDCONFIGURATOR_H_ */
