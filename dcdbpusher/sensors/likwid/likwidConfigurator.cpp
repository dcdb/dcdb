//================================================================================
// Name        : likwidConfigurator.cpp
// Author      : Amir Raoofy
// Contact     :
// Copyright   :
// Description : Source file for likwid plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "likwidConfigurator.h"
#include <sys/sysinfo.h>

likwidConfigurator::likwidConfigurator() {
    /* 
     * TODO
     * If you want sensor or group to be named differently in the config file, you can change it here
     */
    _groupName = "group";
    _baseName = "sensor";
}

likwidConfigurator::~likwidConfigurator() {}

bool likwidConfigurator::sensorBase(likwidSensorBase& s, CFG_VAL config) {
    ADD {
        ATTRIBUTE("likwidEventName", setLikwidEventName);
		ATTRIBUTE("likwidMultiplexingScalingFlag", setLikwidMultiplexingScalingFlag);
    }

    if (s.getLikwidEventName().empty()){
        s.setLikwidEventName ( s.getName() );
    }

    s.setLikwidMultiplexingScalingFlag ( config.get_child("multiplexing_scaling_flag").get_value<string>() );

    return true;
}

bool likwidConfigurator::sensorGroup(likwidSensorGroup& s, CFG_VAL config) {
    ADD {
		ATTRIBUTE("multiplexing_gid", setMultiplexingGid);
    }
    s.setMultiplexingGid ( config.get_child("multiplexing_gid").get_value<string>() );
    return true;
}


void likwidConfigurator::printConfiguratorConfig(LOG_LEVEL ll) {
    /*
     * TODO
     * Log attributes here for debug reasons or delete this method if there are
     * not attributes to log.
     */
    LOG_VAR(ll) << "  NumSpacesAsIndention: " << 2;
}


bool likwidConfigurator::readConfig(std::string cfgPath) {
	/*
	 * Custom code, as likwid is an extra special plugin
	 */
	_cfgPath = cfgPath;

	boost::property_tree::iptree cfg;
	boost::property_tree::read_info(cfgPath, cfg);

	// read global variables (if present overwrite those from global.conf)
	readGlobal(cfg);

	int num_groups_multiplexing = parseAndReadNumberOfMultiplexingGroups(cfg);

	// read groups and templates for groups
	BOOST_FOREACH (boost::property_tree::iptree::value_type &val, cfg) {
		if (boost::iequals(val.first, _groupName)) {
			LOG(debug) << _groupName << " \"" << val.second.data() << "\"";
			if (!val.second.empty()) {
				likwidSensorGroup group(val.second.data());
				if (readSensorGroup(group, val.second)) {
					customizeAndStore(group, val.second , num_groups_multiplexing);
				} else {
					LOG(warning) << _groupName << " \"" << val.second.data() << "\" has bad values! Ignoring...";
				}
			}
		} else if (!boost::iequals(val.first, "global")) {
			LOG(error) << "\"" << val.first << "\": unknown construct!";
			return false;
		}
	}

	// we do not need them anymore
	// _templateCpus.clear();
	// read of config finished. Now we build the mqtt-topic for every sensor
	return constructSensorTopics();
}

int likwidConfigurator::parseAndReadNumberOfMultiplexingGroups (CFG_VAL cfg){
	int num_groups_multiplexing=0;
	BOOST_FOREACH (boost::property_tree::iptree::value_type &val, cfg) {
		if (boost::iequals(val.first, _groupName)) {
			num_groups_multiplexing++;
		}
	}
	return num_groups_multiplexing;
}

void likwidConfigurator::customizeAndStore(likwidSensorGroup &group, CFG_VAL cfg, int& num_groups_multiplexing) {
	// initialize set with cpuIDs
	// default cpuSet: contains all cpuIDs
	std::set<int> cpuSet;
	for (int i = 0; i < get_nprocs(); i++) {
		cpuSet.insert(i);
	}
	// check if (differing) cpus-list is given; if so, overwrite default cpuVec
	boost::optional<boost::property_tree::iptree &> cpus = cfg.get_child_optional("cpus");
	if (cpus) { // cpu list given
		cpuSet = parseCpuString(cpus.get().data());
	} else { // cpu list not given, but perhaps template counter has one
		boost::optional<boost::property_tree::iptree &> def = cfg.get_child_optional("default");
		if (def) {
			templateCpuMap_t::iterator itC = _templateCpus.find(def.get().data());
			if (itC != _templateCpus.end()) {
				cpuSet = itC->second;
			}
		}
	}
	if (cpuSet.empty()) {
		LOG(warning) << _groupName << ": Empty CPU set!";
		return;
	}

	string HTVAL;
	boost::optional<boost::property_tree::iptree &> htVal = cfg.get_child_optional("htVal");
	if (htVal) { // htVal given
		try {
			HTVAL = htVal.get().data();
		} catch (const std::exception &e) { LOG(debug) << "Could not parse htVal value"; }
	}
	
	likwidSGPtr                                    SG = std::make_shared<likwidSensorGroup>(group);
	std::vector<std::shared_ptr<likwidSensorBase>> sensors = group.getLikwidSensors();

	// duplicate sensors of likwidCounterGroup for every CPU
	// set first cpu to already existing sensors
	std::set<int>::iterator it = cpuSet.begin();

	for (const auto &s : SG->getLikwidSensors()) {
		s->setCpu(*it);
		s->setMqttSuffix(MQTTChecker::formatTopic(s->getMqttSuffix(), *it));
		if (s->getLikwidEventName().empty()){
			s->setLikwidEventName ( s->getName() );
		}
	}
	it++;

	// create additional sensors
	for (; it != cpuSet.end(); ++it) {
		for (auto s : sensors) {
			std::shared_ptr<likwidSensorBase> sensor = std::make_shared<likwidSensorBase>(*s);
			sensor->setCpu(*it);
			sensor->setMqttSuffix(MQTTChecker::formatTopic(s->getMqttSuffix(), *it));
			sensor->setLikwidEventName ( s->getName() );
			sensor->setLikwidMultiplexingScalingFlag ( s->getLikwidMultiplexingScalingFlag() );
			SG->pushBackSensor(sensor);
		}
	}

	storeSensorGroup(SG);

	std::vector<int> group_cpus;
	std::copy(cpuSet.begin(), cpuSet.end(), back_inserter(group_cpus));
	SG->setCpuIDs(group_cpus);
	SG->setHtAggregation(HTVAL);
	SG->setNumGroupsMultiplexing ( num_groups_multiplexing );
	SG->setMultiplexingGidMap (_multiplexing_gid_map);
	SG->setMultiplexingStartTimestamps (_group_start_timestamps);
	SG->setGidMapMutex (_gid_map_mutex);
	SG->setMultiplexingGid (group.getMultiplexingGid());
}