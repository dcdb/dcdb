 //================================================================================
// Name        : likwidSensorGroup.cpp
// Author      : Amir Raoofy
// Contact     :
// Copyright   :
// Description : Source file for likwid sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "likwidSensorGroup.h"

#include "timestamp.h"

#include <likwid.h>
#include <unistd.h>
#include <algorithm>
#include <sys/sysinfo.h>


likwidSensorGroup::likwidSensorGroup(const std::string &name) : SensorGroupTemplate(name), _htAggregation(0), _num_groups_multiplexing(0), _read_is_called_before(false) {
	
}

likwidSensorGroup::likwidSensorGroup(const likwidSensorGroup &other) : SensorGroupTemplate(other) {
	/*
	 * TODO
	 * Copy construct attributes
	 */
}

likwidSensorGroup::~likwidSensorGroup() {
	/*
	 * TODO
	 * Tear down attributes
	 */
}

likwidSensorGroup &likwidSensorGroup::operator=(const likwidSensorGroup &other) {
	SensorGroupTemplate::operator=(other);

	_gid = other._gid;
	_enames = other._enames;
	_mnames = other._mnames;
	_htAggregation = other._htAggregation;
	_num_groups_multiplexing = other._num_groups_multiplexing;

	return *this;
}

void likwidSensorGroup::execOnInit() {

	int i;
	int err;
	int            nevents;
	int            nmetrics;

	//  Load the topology module and print some values.
	err = topology_init();
	if (err < 0) {
		LOG(error) << "Failed to initialize LIKWID's topology module";
		return;
	}
	// CpuInfo_t contains global information like name, CPU family, ...
	CpuInfo_t info = get_cpuInfo();
	// CpuTopology_t contains information about the topology of the CPUs.
	CpuTopology_t topo = get_cpuTopology();
	// Create affinity domains. Commonly only needed when reading Uncore counters
	affinity_init();

	LOG(info) << "initializing likwid perfmon on " << info->name << " with " << topo->numHWThreads << " CPUs";

	for (i = 0; i < _cpuIDs.size(); i++) {
			LOG(info) << _cpuIDs[i];
	}

	// Must be called before perfmon_init() but only if you want to use another
	// access mode as the pre-configured one. For direct access (0) you have to
	// be root.
	// accessClient_setaccessmode(0);

	// Initialize the perfmon module.
	//err = perfmon_init(topo->numHWThreads, _cpuIDs.data());
	err = perfmon_init(_cpuIDs.size(), _cpuIDs.data());
	if (err < 0) {
		LOG(error) << "Failed to initialize LIKWID's performance monitoring module";
		topology_finalize();
		return;
	}

	// Add eventset string to the perfmon module.
	_gid = perfmon_addEventSet(_groupName.c_str());
	if (_gid < 0) {
		LOG(error) << "Failed to add event string" << _groupName << "to LIKWID's performance monitoring module";
		perfmon_finalize();
		topology_finalize();
		return;
	}

	nevents = perfmon_getNumberOfEvents(_gid);
	LOG(info) << "nevents: " << nevents;
	_enames.resize(nevents);
	for (int e = 0; e < nevents; e++) {
		_enames[e] = perfmon_getEventName(_gid, e);
		LOG(info) << "setup events; group: " << _groupName << " event: " << _enames[e];
	}

	nmetrics = perfmon_getNumberOfMetrics(_gid);
	LOG(info) << "nmetrics: " << nmetrics;
	_mnames.resize(nmetrics);
	for (int m = 0; m < nmetrics; m++) {
		_mnames[m] = perfmon_getMetricName(_gid, m);
		LOG(info) << "setup metrics; group: " << _groupName << " metric: " << _mnames[m];
	}

	if (!_htAggregation) {
		return;
	}

	// set up convenience aggregator flags
	for (auto &b : _sensorBins) {
		int cpu = b.cpu;
		int mod = cpu % _htAggregation;

		// search bin with smallest multiple of mod as CPU. This bin will then aggregate us
		for (int agg = mod; agg < get_nprocs(); agg += _htAggregation) {
			int bin = _cpuBinMapping[agg];

			if (bin != -1) {
				// found bin aggregating us (could be ourselves)
				b.aggregator = false;
				_sensorBins[bin].aggregator = true;
				if (cpu != mod) {
					for (auto &sensor : b.sensors) {
						sensor->setPublish(false);
					}
				}
				break;
			}
		}
	}

	if (_multiplexing_gid == 0){
		_group_start_timestamps->resize (getNumGroupsMultiplexing());
	}

	// TODO make sure there is no concurrent access here
	std::pair <int, int> gid_pair (_multiplexing_gid,_gid);
	std::lock_guard<std::mutex> gid_map_guard(*_gid_map_mutex);
	_multiplexing_gid_map->insert (gid_pair);
}

bool likwidSensorGroup::execOnStart() {

	int err;

	if (_multiplexing_gid == 0){
		// Setup the eventset identified by group ID (_gid).
		err = perfmon_setupCounters(_gid);
		if (err < 0) {
			LOG(error) << "Failed to setup group" << _gid << " in LIKWID's performance monitoring module";
			perfmon_finalize();
			topology_finalize();
			return false;
		}

		// Start all counters in the previously set up event set.
		err = perfmon_startCounters();
		if (err < 0) {
			LOG(error) << "Failed to start counters for group " << _gid << " for thread " << (-1 * err) - 1;
			perfmon_finalize();
			topology_finalize();
			return false;
		}
	}

	(*_group_start_timestamps) [_multiplexing_gid] = getTimestamp();

	return true;
}

void likwidSensorGroup::execOnStop() {
	int err;

	err = perfmon_stopCounters();
	if (err < 0) {
		LOG(error) << "Failed to stop counters for group " << _multiplexing_gid << " for thread " << (-1 * err) - 1;
		perfmon_finalize();
		topology_finalize();
		return;
	}

	// Uninitialize the perfmon module.
	perfmon_finalize();
	affinity_finalize();
	// Uninitialize the topology module.
	topology_finalize();

}

void likwidSensorGroup::read() {

	if (!_read_is_called_before){
		_read_is_called_before=true;
		return;
	}

	int i, j;
	int err;

	double result = 0.0;

	int gid_this_group = _multiplexing_gid_map->at(_multiplexing_gid);
	int gid_next_group = _multiplexing_gid_map->at((_multiplexing_gid + 1) % _num_groups_multiplexing);

	#ifdef DEBUG
	LOG(debug) << "read group: " << _multiplexing_gid <<  ", gid_this_group: " << gid_this_group;
	LOG(debug) << "read group: " << _multiplexing_gid <<  ", gid_next_group: " << gid_next_group;
	#endif

	// Stop all counters in the currently-active event set.
	err = perfmon_stopCounters();
	if (err < 0) {
		LOG(error) << "Failed to stop counters for group " << gid_this_group << " for thread " << (-1 * err) - 1;
		perfmon_finalize();
		topology_finalize();
		return;
	}

	reading_t reading;
	reading.timestamp = getTimestamp();

	#ifdef DEBUG
	LOG(debug) << "read group: " << _multiplexing_gid <<  ", TS: " << reading.timestamp;
	#endif

	for (auto s : _sensors) {
		//try 
		{			
			int group_id = gid_this_group;
			auto cpu_id = find (_cpuIDs.begin(), _cpuIDs.end(), s->getCpu());
			//double multiplexing_scalar = s-> getLikwidMultiplexingScalingFlag()==true? getNumGroupsMultiplexing() : 1 ;
			double multiplexing_scalar = s-> getLikwidMultiplexingScalingFlag()==true? ((_interval *1000000.0) / (reading.timestamp - (*_group_start_timestamps) [_multiplexing_gid]) ): 1 ;
				
			#ifdef DEBUG
				LOG(debug) << _groupName << "::" << s->getName() << " multiplexing_scalar: \"" << multiplexing_scalar << "\"";
			#endif

			// handling events
			auto event_id = find (_enames.begin(), _enames.end(), s->getLikwidEventName());
			if (event_id != _enames.end())
			{
				result = perfmon_getLastResult(group_id, event_id - _enames.begin(), cpu_id - _cpuIDs.begin());
				result *= multiplexing_scalar; // applying multiplexing linear scaling
				if (result != result){
					result = 0;
				}
			
			    #ifdef DEBUG
					LOG(debug) << _groupName << "::" << s->getName() << " raw reading: \"" << result << "\"";
				#endif

				reading.value = static_cast<int64_t> (result * s->getFactor());
				#ifdef DEBUG
					LOG(info) << "- event name " << s->getLikwidEventName() << " at CPU " << s->getCpu() << ": " << reading.value;
				#endif
			}

			// handling metrics
			auto metric_id = find (_mnames.begin(), _mnames.end(), s->getLikwidEventName());
			if (metric_id != _mnames.end())
			{

				result = perfmon_getLastMetric(group_id, metric_id - _mnames.begin(), cpu_id - _cpuIDs.begin());
				result *= multiplexing_scalar; // applying multiplexing linear scaling
				if (result != result){
					result = 0;
				}
			
				#ifdef DEBUG
							LOG(debug) << _groupName << "::" << s->getName() << " raw reading: \"" << result << "\"";
				#endif

				reading.value = static_cast<int64_t> (result * s->getFactor());
				#ifdef DEBUG
				LOG(info) << "- metric name " << s->getLikwidEventName() << " at CPU " << s->getCpu() << ": " << reading.value;
				#endif

			}
		
			s->storeReading(reading, 1/s->getFactor());

		} //catch (const std::exception &e) { LOG(error) << "Sensorgroup" << _groupName << " could not read value: " << e.what(); }
	}

	// Setup the eventset identified by group ID (_gid).
	err = perfmon_setupCounters(gid_next_group);
	if (err < 0) {
		LOG(error) << "Failed to setup group" << gid_next_group << " in LIKWID's performance monitoring module";
		perfmon_finalize();
		topology_finalize();
		return;
	}

	// Start all counters in the previously set up event set.
	err = perfmon_startCounters();
	if (err < 0) {
		LOG(error) << "Failed to start counters for group " << gid_next_group << " for thread " << (-1 * err) - 1;
		perfmon_finalize();
		topology_finalize();
		return;
	}

	// * store the start timestamp for the next group
	(*_group_start_timestamps) [ (_multiplexing_gid + 1) % _num_groups_multiplexing ] = getTimestamp();

	// hyper-threading aggregation logic
	if (_htAggregation) {
		for (const auto &bin : _sensorBins) {
			if (!(bin.aggregator)) {
				// this cpu bin gets aggregated
				continue;
			}

			for (size_t i = 0; i < bin.sensors.size(); i++) {
				reading_t reading;
				reading.value = 0;
				reading.timestamp = 0;

				for (int j = bin.cpu; j < get_nprocs(); j += _htAggregation) {
					int b = _cpuBinMapping[j];
					if (b != -1) {
						S_Ptr &sensor2 = _sensorBins[b].sensors[i];
						if (_sensorBins[b].latestValueValid) {
							reading.value += sensor2->getLatestValue().value;
							reading.timestamp = sensor2->getLatestValue().timestamp;
						}
					}
				}

				if (reading.timestamp != 0) {
					bin.sensors[i]->storeReadingGlobal(reading);
				}
			}
		}
	}

}

uint64_t likwidSensorGroup::nextReadingTime() {
	
	#ifdef DEBUG
	LOG(debug) << "nextReadingTime group: " << _multiplexing_gid <<  ", TS: " << getTimestamp();
	#endif

	uint64_t now = getTimestamp();
	uint64_t next;
	uint64_t group_delay = static_cast<uint64_t>( (_multiplexing_gid) * _interval / _num_groups_multiplexing);
	if (_sync) {
		uint64_t interval64 = static_cast<uint64_t>(_interval);
		uint64_t now_ms = now / 1000 / 1000;
		// synchronize all measurements with other sensors
		uint64_t waitToStart = interval64 - (now_ms % interval64);
		// less than 1 ms seconds is too small, so we wait the entire interval for the next measurement
		if (!waitToStart) {
			next = (now_ms + interval64) * 1000 * 1000 + group_delay * 1000 * 1000;
			return next;
		}
		next = (now_ms + waitToStart) * 1000 * 1000 + group_delay * 1000 * 1000;
		return next;
	} else {
		next = now + MS_TO_NS(_interval) + group_delay * 1000 * 1000;
		return next;
	}

}
///@}

void likwidSensorGroup::printGroupConfig(LOG_LEVEL ll, unsigned leadingSpaces) {
	/*
	 * TODO
	 * Log attributes here for debug reasons
	 */

	std::string leading(leadingSpaces, ' ');
	LOG_VAR(ll) << leading << "HT aggregation: " << (_htAggregation ? "true" : "false");
	LOG_VAR(ll) << leading << "number of multiplexing groups: " << _num_groups_multiplexing;

}