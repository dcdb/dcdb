//================================================================================
// Name        : likwidSensorBase.h
// Author      : Amir Raoofy
// Contact     :
// Copyright   : 
// Description : Sensor base class for likwid plugin.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

/**
 * @defgroup likwid likwid plugin
 * @ingroup  pusherplugins
 *
 * @brief Describe your plugin in one sentence
 */

#ifndef LIKWID_LIKWIDSENSORBASE_H_
#define LIKWID_LIKWIDSENSORBASE_H_

#include "sensorbase.h"

/* 
 * TODO
 * Add plugin specific includes
 */

/**
 * @brief SensorBase specialization for this plugin.
 *
 * @ingroup likwid
 */
class likwidSensorBase : public SensorBase {
public:
    likwidSensorBase(const std::string& name) :
        SensorBase(name), _config(0), _cpu(-1) {
        /*
         * TODO
         * Initialize plugin specific attributes
         */
    }

    likwidSensorBase(const likwidSensorBase& other) :
        SensorBase(other), _config(other._config), _cpu(other._cpu) {
    }

    virtual ~likwidSensorBase() {
    }

    likwidSensorBase& operator=(const likwidSensorBase& other) {
        SensorBase::operator=(other);
		_config = other._config;
		_cpu = other._cpu;
         
        return *this;
    }

    string getLikwidEventName() const { return _likwidEventName;}
    void setLikwidEventName(string likwidEventName) {_likwidEventName = likwidEventName;}

    bool getLikwidMultiplexingScalingFlag() const { return _likwidMultiplexingScalingFlag;}
    void setLikwidMultiplexingScalingFlag(string likwidMultiplexingScalingFlag) {_likwidMultiplexingScalingFlag = boost::lexical_cast<bool>(likwidMultiplexingScalingFlag);}
    void setLikwidMultiplexingScalingFlag(bool likwidMultiplexingScalingFlag) {_likwidMultiplexingScalingFlag = likwidMultiplexingScalingFlag;}

	unsigned getConfig() const { return _config; }
	int      getCpu() const { return _cpu; }

	void setConfig(unsigned config) { _config = config; }
	void setCpu(int cpu) { _cpu = cpu; }

    void printConfig(LOG_LEVEL ll, LOGGER& lg, unsigned leadingSpaces = 16) {

        LOG_VAR(ll) << setw(leadingSpaces) << "    likwidEventName:         " << getLikwidEventName();
        LOG_VAR(ll) << setw(leadingSpaces) << "    likwidMultiplexingScalingFlag:         " << getLikwidMultiplexingScalingFlag();
        LOG_VAR(ll) << setw(leadingSpaces) << "    NumSpacesAsIndention: " << 5;

        LOG_VAR(ll) << setw(leadingSpaces) << "    Config:            " << std::showbase << std::hex << _config;
		LOG_VAR(ll) << setw(leadingSpaces) << "    CPU:               " << _cpu;
    }

protected:
	protected:
	string _likwidEventName;
    bool _likwidMultiplexingScalingFlag;
	unsigned int _config;
	int          _cpu;

};

#endif /* LIKWID_LIKWIDSENSORBASE_H_ */
