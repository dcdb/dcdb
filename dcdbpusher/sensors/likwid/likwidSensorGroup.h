//================================================================================
// Name        : likwidSensorGroup.h
// Author      : Amir Raoofy
// Contact     :
// Copyright   :
// Description : Header file for likwid sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef LIKWID_LIKWIDSENSORGROUP_H_
#define LIKWID_LIKWIDSENSORGROUP_H_

#include "../../includes/SensorGroupTemplate.h"
#include "likwidSensorBase.h"

class likwidSensorGroup;

using likwidSGPtr = std::shared_ptr<likwidSensorGroup>;

/**
 * @brief SensorGroupTemplate specialization for this plugin.
 *
 * @ingroup likwid
 */
class likwidSensorGroup : public SensorGroupTemplate<likwidSensorBase> {
      public:
	likwidSensorGroup(const std::string &name);
	likwidSensorGroup(const likwidSensorGroup &other);
	virtual ~likwidSensorGroup();
	likwidSensorGroup &operator=(const likwidSensorGroup &other);

	void execOnInit() final override;
	bool execOnStart() final override;
	void execOnStop() final override;

	void printGroupConfig(LOG_LEVEL ll, unsigned leadingSpaces) final override;

	std::vector<S_Ptr> getLikwidSensors() { return _sensors; }

	std::vector<int> getCpuIDs() { return _cpuIDs; }
	void             setCpuIDs(const std::vector<int> &cpuIDs) { _cpuIDs = cpuIDs; }

	void setHtAggregation(std::string htAggregation) { _htAggregation = std::stoul(htAggregation); }

	int  getNumGroupsMultiplexing() { return _num_groups_multiplexing; }
	void setNumGroupsMultiplexing(const int &num_groups_multiplexing) { _num_groups_multiplexing = num_groups_multiplexing; }

	void setMultiplexingGid(std::string multiplexing_gid) { _multiplexing_gid = std::stoi(multiplexing_gid); }
	void setMultiplexingGid(int multiplexing_gid) { _multiplexing_gid = multiplexing_gid; }
	int  getMultiplexingGid() { return _multiplexing_gid; }

	void setMultiplexingGidMap(std::unordered_map<int, int>& multiplexing_gid_map) { _multiplexing_gid_map = &multiplexing_gid_map; }
	void setGidMapMutex(std::mutex& gid_map_mutex) { _gid_map_mutex = &gid_map_mutex; }

	void setMultiplexingStartTimestamps(std::vector <uint64_t>& group_start_timestamps) { _group_start_timestamps = &group_start_timestamps; }

      private:
	void read() final override;

	struct sensorBin {                              /**< A bin holds all sensors with same cpu. Therefore
				 all sensors of a bin belong to the same event
				 group */
		int                cpu;                 /**< Cpu of this bin */
		bool               aggregator;          /**< Flag if this bin aggregates or gets aggregate`d */
		bool               lastValid;           /**< Flag if last reading was valid */
		bool               latestValueValid;    /**< Flag if latest raw reading was valid */
		uint64_t           latest_time_enabled; /**< see perf documentation */
		uint64_t           latest_time_running; /**< see perf documentation */
		std::vector<S_Ptr> sensors;             /**< Sensors in this bin */

		sensorBin(const S_Ptr &s, int cpuVal)
		    : cpu(cpuVal), aggregator(false), lastValid(true), latestValueValid(false), latest_time_enabled(0), latest_time_running(0) {
			sensors.push_back(s);
		}
		sensorBin() = delete;
	};

	uint64_t nextReadingTime() final override;

	bool                   _read_is_called_before;          /* Flag to check if read was called before*/
	std::vector<sensorBin> _sensorBins;                     /**< Bins to sort sensors according to their _cpu. */
	std::vector<int>       _cpuBinMapping;                  /**< _cpuBinMapping[_sensorBins[i].cpu] == i */
	std::vector<int>       _cpuIDs;                         /* list of cpu ids used for this group, this will
													 be used to initialize likwid perfmon for this group */
	int                           _num_groups_multiplexing; /* number of groups in the multiplexing */
	int                           _multiplexing_gid;        /* id of a likwid group within multiplexing context */
	std::unordered_map<int, int> *_multiplexing_gid_map;    /* the mapping of multiplexing ids <key> to likwid gids <value> */
	std::mutex                   *_gid_map_mutex;           /* the mutext to protect writes to _multiplexing_gid_map */
	int                           _gid;                     /**likwid group id */
	vector<string>                _enames;                  /**likwid event names in this group */
	vector<string>                _mnames;                  /**likwid metric names in this group */

	std::vector <uint64_t> *_group_start_timestamps;				/** the vector of timestamps used for keeping the time for when counters got activated */


	unsigned _htAggregation; /**< Value for hyper-threading aggregation. Zero indicates disabled HT agg. */
};

#endif /* LIKWID_LIKWIDSENSORGROUP_H_ */
