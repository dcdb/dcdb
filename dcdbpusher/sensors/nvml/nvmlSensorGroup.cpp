//================================================================================
// Name        : nvmlSensorGroup.cpp
// Author      : Fiona Reid, Weronika Filinger, EPCC @ The University of Edinburgh
// Contact     :
// Copyright   :
// Description : Source file for nvml sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "nvmlSensorGroup.h"

#include "timestamp.h"

nvmlSensorGroup::nvmlSensorGroup(const std::string &name) : SensorGroupTemplate(name), _deviceIndex(0) {}

nvmlSensorGroup::nvmlSensorGroup(const nvmlSensorGroup &other) : SensorGroupTemplate(other), _deviceIndex(other._deviceIndex), _pciBusId(other._pciBusId) {}

nvmlSensorGroup::~nvmlSensorGroup() {}

nvmlSensorGroup &nvmlSensorGroup::operator=(const nvmlSensorGroup &other) {
	SensorGroupTemplate::operator=(other);
	_deviceIndex = other._deviceIndex;
	_pciBusId = other._pciBusId;

	return *this;
}

void nvmlSensorGroup::execOnInit() {
	nvmlReturn_t err;
	if (_pciBusId.size() > 0) {
		err = nvmlDeviceGetHandleByPciBusId(_pciBusId.c_str(), &(_device));
	} else {
		err = nvmlDeviceGetHandleByIndex(0, &(_device));
	}

	if (err != NVML_SUCCESS) {
		LOG(error) << "Sensorgroup " << _groupName << ": NVML error getting device handle: " << nvmlErrorString(err);
	}

	char uuid[128];
	if ((err = nvmlDeviceGetUUID(_device, uuid, 127)) == NVML_SUCCESS) {
		unsigned int index;
		if ((err = nvmlDeviceGetIndex(_device, &index)) == NVML_SUCCESS) {
			nvmlPciInfo_t pci;
			if ((err = nvmlDeviceGetPciInfo(_device, &pci)) == NVML_SUCCESS) {
				LOG(debug) << "Sensorgroup " << _groupName << ": got handle for device => index=" << index << " pciBusId=" << pci.busId << " uuid=" << uuid;
			}
		}
	}
}

void nvmlSensorGroup::read() {
	ureading_t reading;
	reading.timestamp = getTimestamp();
	reading.value = 0;
	nvmlReturn_t       err = NVML_SUCCESS;
	unsigned long long llval;
	unsigned int ival;
	nvmlMemory_t nvMem;
	nvmlUtilization_t nvUtil;

	try {
		for (auto s : _sensors) {
			switch (s->getFeatureType()) {
				case (GPU_ENERGY):
					err = nvmlDeviceGetTotalEnergyConsumption(_device, &llval);
					reading.value = llval;
					break;
				case (GPU_POWER):
					err = nvmlDeviceGetPowerUsage(_device, &ival);
					reading.value = ival;
					break;
				case (GPU_TEMP):
					err = nvmlDeviceGetTemperature(_device, NVML_TEMPERATURE_GPU, &ival);
					reading.value = ival;
					break;
				case (GPU_FAN):
					err = nvmlDeviceGetFanSpeed(_device, &ival);
					reading.value = ival;
					break;
				case (GPU_MEM_USED):
					err = nvmlDeviceGetMemoryInfo(_device, &nvMem);
					reading.value = nvMem.used;
					break;
				case (GPU_MEM_TOT):
					err = nvmlDeviceGetMemoryInfo(_device, &nvMem);
					reading.value = nvMem.total;
					break;
				case (GPU_MEM_FREE):
					err = nvmlDeviceGetMemoryInfo(_device, &nvMem);
					reading.value = nvMem.free;
					break;
				case (GPU_CLK_GP):
					err = nvmlDeviceGetClock(_device, NVML_CLOCK_GRAPHICS, NVML_CLOCK_ID_CURRENT, &ival);
					reading.value = ival;
					break;
				case (GPU_CLK_SM):
					err = nvmlDeviceGetClock(_device, NVML_CLOCK_SM, NVML_CLOCK_ID_CURRENT, &ival);
					reading.value = ival;
					break;
				case (GPU_CLK_MEM):
					err = nvmlDeviceGetClock(_device, NVML_CLOCK_MEM, NVML_CLOCK_ID_CURRENT, &ival);
					reading.value = ival;
					break;
				case (GPU_UTL_MEM):
					err = nvmlDeviceGetUtilizationRates(_device, &nvUtil);
					reading.value = nvUtil.memory;
					break;
				case (GPU_UTL_GPU):
					err = nvmlDeviceGetUtilizationRates(_device, &nvUtil);
					reading.value = nvUtil.gpu;
					break;
				case (GPU_ECC_ERR):
					err = nvmlDeviceGetTotalEccErrors(_device, NVML_MEMORY_ERROR_TYPE_CORRECTED, NVML_VOLATILE_ECC, &llval);
					reading.value = llval;
					break;
				case (GPU_PCIE_THRU):
					err = nvmlDeviceGetPcieThroughput(_device, NVML_PCIE_UTIL_COUNT, &ival);
					reading.value = ival;
					break;
				case (GPU_PCIE_TX):
					err = nvmlDeviceGetPcieThroughput(_device, NVML_PCIE_UTIL_TX_BYTES, &ival);
					reading.value = ival;
					break;
				case (GPU_PCIE_RX):
					err = nvmlDeviceGetPcieThroughput(_device, NVML_PCIE_UTIL_RX_BYTES, &ival);
					reading.value = ival;
					break;
				case (GPU_RUN_PRCS):
					// Passing infoCount=0 so nvmlDeviceGetComputeRunningProcesses() will return the current number of running compute
					// processes
					ival = 0;
					err = nvmlDeviceGetComputeRunningProcesses(_device, &ival, NULL);
					// If no processes are running, the above call will return NVML_SUCCESS, otherwise NVML_ERROR_INSUFFICIENT_SIZE
					reading.value = ival;
					if (err == NVML_ERROR_INSUFFICIENT_SIZE) {
						err = NVML_SUCCESS;
					}
					break;
				case (GPU_THROTTLE_REASON):
					err = nvmlDeviceGetCurrentClocksThrottleReasons(_device, &llval);
					reading.value = llval;
					break;
			}
			s->storeReading(reading);
			if (err != NVML_SUCCESS) {
				LOG(debug) << "Sensorgroup " << _groupName << " could not read " << s->getName() << ": " << nvmlErrorString(err);
			}
#ifdef DEBUG
			LOG(debug) << _groupName << "::" << s->getName() << " raw reading: \"" << reading.value << "\"";
#endif
		}
	} catch (const std::exception &e) { LOG(error) << "Sensorgroup " << _groupName << " could not read value: " << e.what(); }
}

void nvmlSensorGroup::printGroupConfig(LOG_LEVEL ll, unsigned int leadingSpaces) {
	std::string leading(leadingSpaces, ' ');
	if (_pciBusId.size() > 0) {
		LOG_VAR(ll) << leading << "pciBusId:     " << _pciBusId;
	} else {
		LOG_VAR(ll) << leading << "deviceIndex:  " << _deviceIndex;
	}
}
