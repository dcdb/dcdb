//================================================================================
// Name        : modbusSensorGroup.cpp
// Author      : Michael Ott
// Contact     :
// Copyright   : 
// Description : Source file for modbus sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "modbusSensorGroup.h"
#include <modbus/modbus.h>
#include "timestamp.h"

modbusSensorGroup::modbusSensorGroup(const std::string& name) :
    SensorGroupTemplateEntity(name) {
    _slaveId = MODBUS_TCP_SLAVE;
}

modbusSensorGroup::modbusSensorGroup(const modbusSensorGroup& other) :
    SensorGroupTemplateEntity(other) {
    _slaveId = other._slaveId;
}

modbusSensorGroup::~modbusSensorGroup() {
    /* 
     * TODO
     * Tear down attributes
     */
}

modbusSensorGroup& modbusSensorGroup::operator=(const modbusSensorGroup& other) {
    SensorGroupTemplate::operator=(other);
    _slaveId = other._slaveId;

    return *this;
}

void modbusSensorGroup::execOnInit() {
    /* 
     * TODO
     * Implement one time initialization logic for this group here
     * (e.g. allocate memory for buffer) or remove this method if not
     * required.
     */
}

bool modbusSensorGroup::execOnStart() {
    /* 
     * TODO
     * Implement logic before the group starts polling here
     * (e.g. open a file descriptor) or remove this method if not required.
     */
    return true;
}

void modbusSensorGroup::execOnStop() {
    /* 
     * TODO
     * Implement logic when the group stops polling here
     * (e.g. close a file descriptor) or remove this method if not required.
     */
}

void modbusSensorGroup::read() {
    reading_t reading;
    reading.timestamp = getTimestamp();

    try {
	    if (_entity->connect() == 0) {
		    for (auto s : _sensors) {
			    uint16_t buf[4] = {0, 0, 0, 0};
			    bool     err = true;
			    uint64_t val = 0;
			    switch (s->getType()) {
				    case modbusSensorBase::registerType::INT16:
					    err = _entity->read(s->getRegister(), 1, _slaveId, buf) != 1;
					    val = buf[0];
					    break;
				    case modbusSensorBase::registerType::INT32:
					    err = _entity->read(s->getRegister(), 2, _slaveId, buf) != 2;
					    val = (((uint64_t)buf[0]) << 16) | buf[1];
					    break;
				    case modbusSensorBase::registerType::INT64:
					    err = _entity->read(s->getRegister(), 4, _slaveId, buf) != 4;
					    val = (((uint64_t)buf[0]) << 48) | (((uint64_t)buf[1]) << 32) | (((uint64_t)buf[2]) << 16) | buf[3];
					    break;
				    case modbusSensorBase::registerType::FLOAT: {
					    err = _entity->read(s->getRegister(), 2, _slaveId, buf) != 2;
					    float f = 0.0;
					    switch (_entity->getByteOrder()) {
						    case modbusServer::ABCD:
							    f = modbus_get_float_abcd(buf);
							    break;
						    case modbusServer::BADC:
							    f = modbus_get_float_badc(buf);
							    break;
						    case modbusServer::CDAB:
							    f = modbus_get_float_cdab(buf);
							    break;
						    case modbusServer::DCBA:
							    f = modbus_get_float_dcba(buf);
							    break;
						    case modbusServer::undefined:
							    LOG(error)
								<< _groupName << "::" << s->getName() << " ByteOrder is undefined. This shouldn't happen.";
							    break;
					    }
					    val = f;
					    break;
				    }
				    case modbusSensorBase::registerType::undefined:
					    LOG(error) << _groupName << "::" << s->getName() << " Type is undefined. This shouldn't happen.";
					    break;
			    }
			    if (!err) {
				    reading.value = val;
				    s->storeReading(reading);
#ifdef DEBUG
				    LOG(debug) << _groupName << "::" << s->getName() << " raw reading: \"" << reading.value << "\"";
#endif
			    }
		    }
	    }
    } catch (const std::exception &e) { 
        LOG(error) << "Sensorgroup " << _groupName << ": " << e.what();
    }
}

void modbusSensorGroup::printGroupConfig(LOG_LEVEL ll, unsigned int leadingSpaces) {
   	std::string leading(leadingSpaces, ' ');
    LOG_VAR(ll) << leading << "SlaveId:      " << (int) _slaveId;
}
