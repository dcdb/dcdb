//================================================================================
// Name        : modbusConfigurator.h
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Header file for modbus plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef MODBUS_MODBUSCONFIGURATOR_H_
#define MODBUS_MODBUSCONFIGURATOR_H_

#include "../../includes/ConfiguratorTemplateEntity.h"
#include "modbusSensorGroup.h"
#include "modbusServer.h"

/**
 * @brief ConfiguratorTemplate specialization for this plugin.
 *
 * @ingroup modbus
 */
class modbusConfigurator : public ConfiguratorTemplateEntity<modbusSensorBase, modbusSensorGroup, modbusServer> {

public:
    modbusConfigurator();
    virtual ~modbusConfigurator();

protected:
    /* Overwritten from ConfiguratorTemplate */
    bool sensorBase(modbusSensorBase& s, CFG_VAL config) override;
    bool sensorGroup(modbusSensorGroup& s, CFG_VAL config) override;
    bool sensorEntity(modbusServer& s, CFG_VAL config) override;

    //TODO implement if required
    //void global(CFG_VAL config) override;
    //void derivedSetGlobalSettings(const pluginSettings_t& pluginSettings) override;

    void printConfiguratorConfig(LOG_LEVEL ll) final override;
};

extern "C" ConfiguratorInterface* create() {
    return new modbusConfigurator;
}

extern "C" void destroy(ConfiguratorInterface* c) {
    delete c;
}

#endif /* MODBUS_MODBUSCONFIGURATOR_H_ */
