//================================================================================
// Name        : modbusConfigurator.cpp
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Source file for modbus plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "modbusConfigurator.h"

modbusConfigurator::modbusConfigurator() {
    /* 
     * TODO
     * If you want sensor or group to be named differently in the config file, you can change it here
     */
    _entityName = "server";
    _groupName = "group";
    _baseName = "sensor";
}

modbusConfigurator::~modbusConfigurator() {}

bool modbusConfigurator::sensorBase(modbusSensorBase& s, CFG_VAL config) {
    ADD {
         ATTRIBUTE("register", setRegister);
         ATTRIBUTE("type", setType);
    }
    return true;
}

bool modbusConfigurator::sensorGroup(modbusSensorGroup& s, CFG_VAL config) {
    ADD {
        ATTRIBUTE("SlaveId", setSlaveId);
    }
    return true;
}

bool modbusConfigurator::sensorEntity(modbusServer& s, CFG_VAL config) {
    ADD {
		ATTRIBUTE("Host", setHost);
		ATTRIBUTE("Port", setPort);
		ATTRIBUTE("ByteOrder", setByteOrder);
    }

    if (s.getHost().empty()) {
        s.setHost(s.getName());
    }
    return true;
}

void modbusConfigurator::printConfiguratorConfig(LOG_LEVEL ll) {
}
