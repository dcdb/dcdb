//================================================================================
// Name        : modbusSensorGroup.h
// Author      : Michael Ott
// Contact     :
// Copyright   : 
// Description : Header file for modbus sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef MODBUS_MODBUSSENSORGROUP_H_
#define MODBUS_MODBUSSENSORGROUP_H_

#include "../../includes/SensorGroupTemplateEntity.h"
#include "modbusSensorBase.h"

/**
 * @brief SensorGroupTemplate specialization for this plugin.
 *
 * @ingroup modbus
 */
class modbusSensorGroup : public SensorGroupTemplateEntity<modbusSensorBase, modbusServer> {
public:
    modbusSensorGroup(const std::string& name);
    modbusSensorGroup(const modbusSensorGroup& other);
    virtual ~modbusSensorGroup();
    modbusSensorGroup& operator=(const modbusSensorGroup& other);

    void execOnInit()  final override;
    bool execOnStart() final override;
    void execOnStop()  final override;

   	void setSlaveId(const std::string &slaveId) { _slaveId = stoi(slaveId); }
    int getSlaveId() const { return _slaveId; }
     
    void printGroupConfig(LOG_LEVEL ll, unsigned int leadingSpaces) final override;

private:
    void read() final override;

    uint8_t _slaveId;
};

#endif /* MODBUS_MODBUSSENSORGROUP_H_ */
