//================================================================================
// Name        : modbusServer.cpp
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Source file for modbusServer class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "modbusServer.h"

modbusServer::modbusServer(const std::string& name) :
    EntityInterface(name) {
	_host = "";
	_port = 502;
    _byteOrder = ABCD;
    _mb = nullptr;
}

modbusServer::modbusServer(const modbusServer& other) :
    EntityInterface(other) {
    _host = other._host;
    _port = other._port;
    _byteOrder = other._byteOrder;
}

modbusServer::~modbusServer() {
    if (_mb != nullptr) {
        modbus_free(_mb);
        _mb = nullptr;
    }
}

modbusServer& modbusServer::operator=(const modbusServer& other) {
    EntityInterface::operator=(other);
    _host = other._host;
    _port = other._port;
    _byteOrder = other._byteOrder;

    return *this;
}

int modbusServer::connect() {
    if (_mb) {
        return 0;
    }
    _mb = modbus_new_tcp(_host.c_str(), _port);
    int ret = modbus_connect(_mb);
    if (ret == -1) {
        modbus_free(_mb);
        _mb = nullptr;
        throw std::runtime_error("Error connecting to " + _host + ": " + std::string(modbus_strerror(errno)));
    } else {
        return ret;
    }
}

void modbusServer::disconnect() {
	modbus_close(_mb);
    modbus_free(_mb);
    _mb = nullptr;
}

int modbusServer::read(int reg, int len, uint8_t slaveId, uint16_t* buf) {
    _mtx.lock();
    if (modbus_set_slave(_mb, slaveId) != 0) {
        throw std::runtime_error("Error setting Modbus Slave ID to " + std::to_string(slaveId) + ": " + std::string(modbus_strerror(errno)));
        return -1;
    }
    int ret =  modbus_read_registers(_mb, reg, len, buf);
    _mtx.unlock();
    if (ret == -1) {
        throw std::runtime_error("Read error: " + std::string(modbus_strerror(errno)));
    }
    return ret;
}

void modbusServer::printEntityConfig(LOG_LEVEL ll, unsigned int leadingSpaces) {
	std::string leading(leadingSpaces, ' ');
	LOG_VAR(ll) << leading << "Host:         " << _host;
	LOG_VAR(ll) << leading << "Port:         " << _port;
	LOG_VAR(ll) << leading << "ByteOrder:    " << getByteOrderString();
}
