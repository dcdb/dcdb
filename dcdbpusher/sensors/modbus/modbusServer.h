//================================================================================
// Name        : modbusServer.h
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Header file for modbusServer class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef MODBUSSERVER_H_
#define MODBUSSERVER_H_

#include <modbus/modbus.h>
#include <boost/algorithm/string/predicate.hpp>
#include <mutex>
#include "../../includes/EntityInterface.h"

/**
 * @brief This class handles modbus Server.
 *
 * @ingroup modbus
 */
class modbusServer : public EntityInterface {

public:
	enum byteOrder { undefined = 0, ABCD, BADC, CDAB, DCBA };

    modbusServer(const std::string& name);
    modbusServer(const modbusServer& other);
    virtual ~modbusServer();
    modbusServer& operator=(const modbusServer& other);

    int connect();
    void disconnect();
    int read(int reg, int len, uint8_t slaveId, uint16_t* buf);

	void setHost(const std::string &host) { _host = host; }
	void setPort(const std::string &port) { _port = stoi(port); }
	void setByteOrder(const std::string &byteOrder) {
		if (boost::iequals(byteOrder, "ABCD")) {
			_byteOrder = ABCD;
		} else if (boost::iequals(byteOrder, "BADC")) {
			_byteOrder = BADC;
		} else if (boost::iequals(byteOrder, "CDAB")) {
			_byteOrder = CDAB;
		} else if (boost::iequals(byteOrder, "DCBA")) {
			_byteOrder = DCBA;
		} else {
			_byteOrder = undefined;
		}
	}

    std::string getHost() const { return _host; }
    int         getPort() const { return _port; }
	byteOrder   getByteOrder() const { return _byteOrder; }
	std::string getByteOrderString() const {
		switch (_byteOrder) {
			case ABCD:
				return std::string("ABCD");
			case BADC:
				return std::string("BADC");
			case CDAB:
				return std::string("CDAB");
			case DCBA:
				return std::string("DCBA");
			default:
				return std::string("undefined");
		}
	}

    void printEntityConfig(LOG_LEVEL ll, unsigned int leadingSpaces) final override;

private:
	std::string _host;
	int         _port;
    byteOrder   _byteOrder;
    modbus_t*   _mb;
	std::mutex  _mtx; 
};

#endif /* MODBUSSERVER_H_ */
