//================================================================================
// Name        : modbusSensorBase.h
// Author      : Michael Ott
// Contact     :
// Copyright   : 
// Description : Sensor base class for modbus plugin.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

/**
 * @defgroup modbus modbus plugin
 * @ingroup  pusherplugins
 *
 * @brief Describe your plugin in one sentence
 */

#ifndef MODBUS_MODBUSSENSORBASE_H_
#define MODBUS_MODBUSSENSORBASE_H_

#include "sensorbase.h"

#include "modbusServer.h"

/* 
 * TODO
 * Add plugin specific includes
 */

/**
 * @brief SensorBase specialization for this plugin.
 *
 * @ingroup modbus
 */
class modbusSensorBase : public SensorBase {
public:
   	enum registerType { undefined = 0, INT16, INT32, INT64, FLOAT };

    modbusSensorBase(const std::string& name) : SensorBase(name) {
        _type = undefined;
    }

    modbusSensorBase(const modbusSensorBase& other) : SensorBase(other) {
        _type = other._type;
		_reg = other._reg;
    }

    virtual ~modbusSensorBase() {
        /*
         * TODO
         * If necessary, deconstruct plugin specific attributes
         */
    }

    modbusSensorBase& operator=(const modbusSensorBase& other) {
        SensorBase::operator=(other);
        _type = other._type;
		_reg = other._reg;
         
        return *this;
    }

	void setRegister(const std::string &reg) { _reg = stoi(reg); }
	void setType(const std::string &type) {
		if (boost::iequals(type, "INT16")) {
			_type = INT16;
		} else if (boost::iequals(type, "INT32")) {
			_type = INT32;
		} else if (boost::iequals(type, "INT64")) {
			_type = INT64;
		} else if (boost::iequals(type, "FLOAT")) {
			_type = FLOAT;
		} else {
			_type = undefined;
		}
	}

	int          getRegister() const { return _reg; }
	registerType getType() const { return _type; }
	std::string  getTypeString() const {
		switch (_type) {
			case INT16:
				return std::string("INT16");
			case INT32:
				return std::string("INT32");
			case INT64:
				return std::string("INT64");
			case FLOAT:
				return std::string("FLOAT");
			default:
				return std::string("undefined");
		}
	}

	void printConfig(LOG_LEVEL ll, LOGGER &lg, unsigned leadingSpaces = 16) {
		std::string leading(leadingSpaces + 4, ' ');
		LOG_VAR(ll) << leading << "Register:          " << _reg;
		LOG_VAR(ll) << leading << "Type:              " << getTypeString();
    }

protected:
    int          _reg;
    registerType _type;
};

#endif /* MODBUS_MODBUSSENSORBASE_H_ */
