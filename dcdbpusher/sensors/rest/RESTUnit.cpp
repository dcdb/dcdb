//================================================================================
// Name        : RESTUnit.cpp
// Author      : Michael Ott
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Source file for RESTUnit class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2018-2021 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "RESTUnit.h"
#include "globalconfiguration.h"

#include <iostream>

#include <openssl/bio.h>
#include <openssl/err.h>

RESTUnit::RESTUnit(const std::string &name) : EntityInterface(name), _ctx(ssl::context::tlsv12_client), _ssl(false), _type(undefined) {}

RESTUnit::RESTUnit(const RESTUnit &other)
    : EntityInterface(other), _ctx(ssl::context::tlsv12_client), _baseURL(other._baseURL), _hostname(other._hostname), _port(other._port), _path(other._path),
      _authEndpoint(other._authEndpoint), _authData(other._authData), _ssl(false), _type(other._type) {}

RESTUnit::~RESTUnit() {}

RESTUnit &RESTUnit::operator=(const RESTUnit &other) {
	EntityInterface::operator=(other);
	_baseURL = other._baseURL;
	_hostname = other._hostname;
	_port = other._port;
	_path = other._path;
	_authEndpoint = other._authEndpoint;
	_authData = other._authData;
	_ssl = other._ssl;
	_type = other._type;

	return *this;
}

void RESTUnit::execOnInit(boost::asio::io_service &io) {}

bool RESTUnit::connect(beast::ssl_stream<beast::tcp_stream> &stream) {
	tcp::resolver                         resolver(_ioc);
	auto const                            hosts = resolver.resolve(_hostname, _port);
	try {
		beast::get_lowest_layer(stream).connect(hosts);
	} catch (const std::exception &e) {
		LOG(error) << "Error while connecting to " << _hostname << ":" << _port << ": " << e.what();
		return false;
	}

	if (_ssl) {
		if (!SSL_set_tlsext_host_name(stream.native_handle(), _hostname.c_str())) {
			beast::error_code ec{static_cast<int>(::ERR_get_error()), net::error::get_ssl_category()};
			throw beast::system_error{ec};
		}

		stream.handshake(ssl::stream_base::client);
	}

	return true;
}

void RESTUnit::disconnect(beast::ssl_stream<beast::tcp_stream> &stream) {
	if (_ssl) {
		try {
			stream.shutdown();
		} catch (const std::exception &e) {
			LOG(error) << "Error while shutting down SSL connection to " << _hostname << ":" << _port << ": " << e.what();
		}
	}
	beast::get_lowest_layer(stream).close();
}

bool RESTUnit::authenticate() {
	if ((_authData.size() == 0) || (_cookie.size() > 0)) {
		return true;
	}

	beast::ssl_stream<beast::tcp_stream> stream(_ioc, _ctx);
	if (!connect(stream)) {
		return false;
	}

	http::request<http::string_body> auth{http::verb::post, _path + _authEndpoint, 11};
	auth.set(http::field::host, _hostname);
	auth.body() = _authData;
	auth.prepare_payload();

	beast::flat_buffer                buffer;
	http::response<http::string_body> res;
	try {
		if (_ssl) {
			http::write(stream, auth);
		} else {
			http::write(beast::get_lowest_layer(stream), auth);
		}
	} catch (const std::exception &e) {
		LOG(error) << "Error sending authentication request to " << _hostname << ":" << _port << ": " << e.what();
		return false;
	}

	try {
		if (_ssl) {
			http::read(stream, buffer, res);
		} else {
			http::read(beast::get_lowest_layer(stream), buffer, res);
		}
	} catch (const std::exception &e) {
		LOG(error) << "Error retrieving authentication response from " << _hostname << ":" << _port << ": " << e.what();
		return false;
	}
	disconnect(stream);

	_cookie = std::string(res[http::field::set_cookie]);
	if (_cookie.size() > 0) {
		LOG(debug) << "Authenticated to " << _hostname << ", received cookie: " << _cookie;
		return true;
	} else {
		LOG(info) << "Could not authenticate to " << _hostname << ", no cookie received.";
		return false;
	}
}

bool RESTUnit::sendRequest(const std::string &endpoint, const std::string &request, std::string &response) {
	beast::ssl_stream<beast::tcp_stream> stream(_ioc, _ctx);
	if (!authenticate() || !connect(stream)) {
		return false;
	}

	http::request<http::string_body> req{http::verb::get, _path + endpoint, 11};
	req.set(http::field::host, _hostname);
	if (_cookie.size() > 0) {
		req.set(http::field::cookie, _cookie);
	}
	req.body() = request;
	req.prepare_payload();

	try {
		if (_ssl) {
			http::write(stream, req);
		} else {
			http::write(beast::get_lowest_layer(stream), req);
		}
	} catch (const std::exception &e) {
		LOG(error) << "Error sending request to " << _hostname << ":" << _port << ": " << e.what();
		return false;
	}

	beast::flat_buffer                buffer;
	http::response<http::string_body> res;
	try {
		if (_ssl) {
			http::read(stream, buffer, res);
		} else {
			http::read(beast::get_lowest_layer(stream), buffer, res);
		}
	} catch (const std::exception &e) {
		LOG(error) << "Error retrieving response " << _hostname << ":" << _port << ": " << e.what();
		return false;
	}

	disconnect(stream);

	if (res.body().size() > 0) {
		response = res.body();
		return true;
	} else {
		LOG(debug) << "Received empty HTTP response, clearing cookies" << std::endl << res;
		_cookie.clear();
		return false;
	}
}

void RESTUnit::printEntityConfig(LOG_LEVEL ll, unsigned int leadingSpaces) {
	std::string leading(leadingSpaces, ' ');
	LOG_VAR(ll) << leading << "BaseURL:      " << getBaseURL();
	LOG_VAR(ll) << leading << "Type:         " << getTypeString();
	if (getAuthEndpoint().size()) {
		LOG_VAR(ll) << leading << "AuthEndPoint: " << getAuthEndpoint();
		LOG_VAR(ll) << leading << "AuthData:     " << getAuthEndpoint();
	}
}
