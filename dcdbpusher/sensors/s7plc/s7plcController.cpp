//================================================================================
// Name        : s7plcController.cpp
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Source file for s7plcController class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2020 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "s7plcController.h"

s7plcController::s7plcController(const std::string &name) : EntityInterface(name) {
	_host = "";
	_rack = 0;
	_slot = 2;
	_client = Cli_Create();
}

s7plcController::s7plcController(const s7plcController &other) : EntityInterface(other) {
	/*
	 * TODO
	 * Copy construct attributes
	 */
}

s7plcController::~s7plcController() {
	Cli_Destroy(&_client);
}

s7plcController &s7plcController::operator=(const s7plcController &other) {
	EntityInterface::operator=(other);
	/*
	 * TODO
	 * Implement assignment operator here
	 */

	return *this;
}

/*
 * TODO
 * Implement own methods
 */

int s7plcController::connect() {
	return Cli_ConnectTo(_client, _host.c_str(), _rack, _slot);
}

int s7plcController::disconnect() {
	return Cli_Disconnect(_client);
}

int s7plcController::read(int DB, int offset, int size, void *buf) {
	uint32_t tmp32;
	int      ret = Cli_DBRead(_client, DB, offset, sizeof(tmp32), &tmp32);
	if (ret != 0) {
		char buf[1024];
		Cli_ErrorText(ret, buf, 1024);
		throw std::runtime_error("Error reading DB: " + std::string(buf));
	}

	if (size == 4) {
		tmp32 = ntohl(tmp32);
		memcpy(buf, &tmp32, 4);
	} else if (size == 2) {
		uint16_t tmp16;
		memcpy(&tmp16, &tmp32, 2);
		tmp16 = ntohl(tmp16);
		memcpy(buf, &tmp16, 2);
	} else {
		memcpy(buf, &tmp32, 1);
	}

	return ret;
}

void s7plcController::execOnInit(boost::asio::io_service &io) {
	/*
	 * TODO
	 * Implement logic to initialize the entity here or remove method if not required.
	 * Will be called exactly once at startup.
	 */
}

void s7plcController::printEntityConfig(LOG_LEVEL ll, unsigned int leadingSpaces) {
	std::string leading(leadingSpaces, ' ');
	LOG_VAR(ll) << leading << "Host: " << _host;
	LOG_VAR(ll) << leading << "Rack: " << _rack;
	LOG_VAR(ll) << leading << "Slot: " << _slot;
}
