//================================================================================
// Name        : s7plcSensorGroup.h
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Header file for s7plc sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2020 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef S7PLC_S7PLCSENSORGROUP_H_
#define S7PLC_S7PLCSENSORGROUP_H_

#include "../../includes/SensorGroupTemplateEntity.h"
#include "s7plcSensorBase.h"

/**
 * @brief SensorGroupTemplate specialization for this plugin.
 *
 * @ingroup s7plc
 */
class s7plcSensorGroup : public SensorGroupTemplateEntity<s7plcSensorBase, s7plcController> {
      public:
	s7plcSensorGroup(const std::string &name);
	s7plcSensorGroup(const s7plcSensorGroup &other);
	virtual ~s7plcSensorGroup();
	s7plcSensorGroup &operator=(const s7plcSensorGroup &other);

	void execOnInit() final override;
	bool execOnStart() final override;
	void execOnStop() final override;

	/*
	 * TODO
	 * Add getter and setters for group attributes if required
	 */
	void printGroupConfig(LOG_LEVEL ll, unsigned int leadingSpaces) final override;

      private:
	void read() final override;
};

#endif /* S7PLC_S7PLCSENSORGROUP_H_ */
