//================================================================================
// Name        : s7plcController.h
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Header file for s7plcController class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2020 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef S7PLCCONTROLLER_H_
#define S7PLCCONTROLLER_H_

#include "../../includes/EntityInterface.h"
#include "snap7.h"

/**
 * @brief This class handles s7plc Controller.
 *
 * @ingroup s7plc
 */
class s7plcController : public EntityInterface {

      public:
	s7plcController(const std::string &name);
	s7plcController(const s7plcController &other);
	virtual ~s7plcController();
	s7plcController &operator=(const s7plcController &other);

	void setHost(const std::string &host) { _host = host; }
	void setRack(const std::string &rack) { _rack = stoi(rack); }
	void setSlot(const std::string &slot) { _slot = stoi(slot); }

	int connect();
	int disconnect();

	int read(int DB, int offset, int size, void *buf);

	void execOnInit(boost::asio::io_service &io) final override;

	void printEntityConfig(LOG_LEVEL ll, unsigned int leadingSpaces) final override;

      private:
	std::string _host;
	int         _rack;
	int         _slot;

	S7Object _client;
};

#endif /* S7PLCCONTROLLER_H_ */
