//================================================================================
// Name        : s7plcConfigurator.h
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Header file for s7plc plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2020 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef S7PLC_S7PLCCONFIGURATOR_H_
#define S7PLC_S7PLCCONFIGURATOR_H_

#include "../../includes/ConfiguratorTemplateEntity.h"
#include "s7plcController.h"
#include "s7plcSensorGroup.h"

/**
 * @brief ConfiguratorTemplate specialization for this plugin.
 *
 * @ingroup s7plc
 */
class s7plcConfigurator : public ConfiguratorTemplateEntity<s7plcSensorBase, s7plcSensorGroup, s7plcController> {

      public:
	s7plcConfigurator();
	virtual ~s7plcConfigurator();

      protected:
	/* Overwritten from ConfiguratorTemplate */
	bool sensorBase(s7plcSensorBase &s, CFG_VAL config) override;
	bool sensorGroup(s7plcSensorGroup &s, CFG_VAL config) override;
	bool sensorEntity(s7plcController &s, CFG_VAL config) override;

	// TODO implement if required
	// void global(CFG_VAL config) override;
	// void derivedSetGlobalSettings(const pluginSettings_t& pluginSettings) override;

	void printConfiguratorConfig(LOG_LEVEL ll) final override;
};

extern "C" ConfiguratorInterface *create() {
	return new s7plcConfigurator;
}

extern "C" void destroy(ConfiguratorInterface *c) {
	delete c;
}

#endif /* S7PLC_S7PLCCONFIGURATOR_H_ */
