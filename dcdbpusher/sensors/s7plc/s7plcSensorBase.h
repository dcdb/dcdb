//================================================================================
// Name        : s7plcSensorBase.h
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Sensor base class for s7plc plugin.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2020 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

/**
 * @defgroup s7plc s7plc plugin
 * @ingroup  pusherplugins
 *
 * @brief Describe your plugin in one sentence
 */

#ifndef S7PLC_S7PLCSENSORBASE_H_
#define S7PLC_S7PLCSENSORBASE_H_

#include "s7plcController.h"
#include "sensorbase.h"

/*
 * TODO
 * Add plugin specific includes
 */

/**
 * @brief SensorBase specialization for this plugin.
 *
 * @ingroup s7plc
 */
class s7plcSensorBase : public SensorBase {
      public:
	s7plcSensorBase(const std::string &name) : SensorBase(name) {
		_db = 0;
		_offset = 0;
		_type = sensorType::undefined;
	}

	s7plcSensorBase(const s7plcSensorBase &other) : SensorBase(other), _db(other._db), _offset(other._offset), _type(other._type) {}

	virtual ~s7plcSensorBase() {
		/*
		 * TODO
		 * If necessary, deconstruct plugin specific attributes
		 */
	}

	s7plcSensorBase &operator=(const s7plcSensorBase &other) {
		SensorBase::operator=(other);
		_db = other._db;
		_offset = other._offset;
		_type = other._type;

		return *this;
	}

	enum sensorType { undefined = 0, BIT, BYTE, INT, DINT, WORD, DWORD, REAL };

	void setDB(const std::string &db) { _db = stoi(db); }
	void setOffset(const std::string &offset) { _offset = stoi(offset); }
	void setType(const std::string &type) {
		if (boost::iequals(type, "BIT")) {
			_type = BIT;
		} else if (boost::iequals(type, "BYTE")) {
			_type = BYTE;
		} else if (boost::iequals(type, "INT")) {
			_type = INT;
		} else if (boost::iequals(type, "DINT")) {
			_type = DINT;
		} else if (boost::iequals(type, "WORD")) {
			_type = WORD;
		} else if (boost::iequals(type, "DWORD")) {
			_type = DWORD;
		} else if (boost::iequals(type, "REAL")) {
			_type = REAL;
		} else {
			_type = undefined;
		}
	}

	int         getDB() { return _db; }
	int         getOffset() { return _offset; }
	sensorType  getType() { return _type; }
	std::string getTypeString() const {
		switch (_type) {
			case BIT:
				return std::string("BIT");
			case BYTE:
				return std::string("BYTE");
			case INT:
				return std::string("INT");
			case DINT:
				return std::string("DINT");
			case WORD:
				return std::string("WORD");
			case DWORD:
				return std::string("DWORD");
			case REAL:
				return std::string("REAL");
			default:
				return std::string("undefined");
		}
	}

	void printConfig(LOG_LEVEL ll, LOGGER &lg, unsigned leadingSpaces = 16) {
		std::string leading(leadingSpaces + 4, ' ');
		LOG_VAR(ll) << leading << "DB:     " << _db;
		LOG_VAR(ll) << leading << "Offset: " << _offset;
		LOG_VAR(ll) << leading << "Type:   " << getTypeString();
	}

      protected:
	int        _db;
	int        _offset;
	sensorType _type;
};

#endif /* S7PLC_S7PLCSENSORBASE_H_ */
