//================================================================================
// Name        : s7plcConfigurator.cpp
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Source file for s7plc plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2020 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "s7plcConfigurator.h"

s7plcConfigurator::s7plcConfigurator() {
	/*
	 * TODO
	 * If you want sensor or group to be named differently in the config file, you can change it here
	 */
	_entityName = "controller";
	_groupName = "group";
	_baseName = "sensor";
}

s7plcConfigurator::~s7plcConfigurator() {}

bool s7plcConfigurator::sensorBase(s7plcSensorBase &s, CFG_VAL config) {
	ADD {
		ATTRIBUTE("DB", setDB);
		ATTRIBUTE("Offset", setOffset);
		ATTRIBUTE("Type", setType);
	}
	return true;
}

bool s7plcConfigurator::sensorGroup(s7plcSensorGroup &s, CFG_VAL config) {
	ADD {}
	return true;
}

bool s7plcConfigurator::sensorEntity(s7plcController &s, CFG_VAL config) {
	ADD {
		/*
		 * TODO
		 * Add ATTRIBUTE macros for Controller attributes
		 */
		ATTRIBUTE("Host", setHost);
		ATTRIBUTE("Rack", setRack);
		ATTRIBUTE("Slot", setSlot);
	}
	return true;
}

void s7plcConfigurator::printConfiguratorConfig(LOG_LEVEL ll) {
	/*
	 * TODO
	 * Log attributes here for debug reasons or delete this method if there are
	 * not attributes to log.
	 */
}
