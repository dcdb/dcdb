//================================================================================
// Name        : s7plcSensorGroup.cpp
// Author      : Michael Ott
// Contact     :
// Copyright   :
// Description : Source file for s7plc sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2020 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "s7plcSensorGroup.h"

#include "timestamp.h"

s7plcSensorGroup::s7plcSensorGroup(const std::string &name) : SensorGroupTemplateEntity(name) {
	/*
	 * TODO
	 * Construct attributes
	 */
}

s7plcSensorGroup::s7plcSensorGroup(const s7plcSensorGroup &other) : SensorGroupTemplateEntity(other) {
	/*
	 * TODO
	 * Copy construct attributes
	 */
}

s7plcSensorGroup::~s7plcSensorGroup() {
	/*
	 * TODO
	 * Tear down attributes
	 */
}

s7plcSensorGroup &s7plcSensorGroup::operator=(const s7plcSensorGroup &other) {
	SensorGroupTemplate::operator=(other);
	/*
	 * TODO
	 * Implement assignment operator
	 */

	return *this;
}

void s7plcSensorGroup::execOnInit() {
	/*
	 * TODO
	 * Implement one time initialization logic for this group here
	 * (e.g. allocate memory for buffer) or remove this method if not
	 * required.
	 */
}

bool s7plcSensorGroup::execOnStart() {
	/*
	 * TODO
	 * Implement logic before the group starts polling here
	 * (e.g. open a file descriptor) or remove this method if not required.
	 */
	return true;
}

void s7plcSensorGroup::execOnStop() {
	/*
	 * TODO
	 * Implement logic when the group stops polling here
	 * (e.g. close a file descriptor) or remove this method if not required.
	 */
}

void s7plcSensorGroup::read() {
	reading_t reading;
	reading.timestamp = getTimestamp();

	if (_entity->connect() == 0) {
		for (auto s : _sensors) {
			try {
				int      ret = 1;
				uint64_t val = 0;
				switch (s->getType()) {
					case s7plcSensorBase::sensorType::BYTE:
						uint8_t b;
						ret = _entity->read(s->getDB(), s->getOffset(), 1, &b);
						val = b;
						break;
					case s7plcSensorBase::sensorType::INT:
						int16_t i;
						ret = _entity->read(s->getDB(), s->getOffset(), 2, &i);
						val = i;
						break;
					case s7plcSensorBase::sensorType::DINT:
						int32_t di;
						ret = _entity->read(s->getDB(), s->getOffset(), 4, &di);
						val = di;
						break;
					case s7plcSensorBase::sensorType::WORD:
						uint16_t w;
						ret = _entity->read(s->getDB(), s->getOffset(), 2, &w);
						val = w;
						break;
					case s7plcSensorBase::sensorType::DWORD:
						uint32_t dw;
						ret = _entity->read(s->getDB(), s->getOffset(), 4, &dw);
						val = dw;
						break;
					case s7plcSensorBase::sensorType::REAL:
						float f;
						ret = _entity->read(s->getDB(), s->getOffset(), 4, &f);
						// Apply factor to account for decimal places
						val = f * s->getFactor();
						break;
					default:
						break;
				}

				if (ret == 0) {
					reading.value = val;
					if (s->getType() == s7plcSensorBase::sensorType::REAL) {
						// We have already applied the factor above, so dont apply it again in
						// storeReading()
						s->storeReading(reading, 1 / s->getFactor());
					} else {
						s->storeReading(reading);
					}
#ifdef DEBUG
					LOG(debug) << _groupName << "::" << s->getName() << " raw reading: \"" << reading.value << "\"";
#endif
				}
			} catch (const std::exception &e) { LOG(error) << "Sensorgroup" << _groupName << " could not read value: " << e.what(); }
		}
	}
}

void s7plcSensorGroup::printGroupConfig(LOG_LEVEL ll, unsigned int leadingSpaces) {
	/*
	 * TODO
	 * Log attributes here for debug reasons
	 * Printing the name of associated _entity may be a good idea.
	 */
	std::string leading(leadingSpaces, ' ');
	// LOG_VAR(ll) << leading << "DB: " << _db;
}
