//================================================================================
// Name        : VariorumSensorGroup.cpp
// Author      : Amir Raoofy
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Source file for Variorum sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2018-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "VariorumSensorGroup.h"
#include "timestamp.h"
#include <cstring>
#include <functional>

//#include <jansson.h>
#include <stdio.h>
#include <stdlib.h>
#include <variorum.h>
#include <variorum_topology.h>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

using namespace std;

VariorumSensorGroup::VariorumSensorGroup(const std::string &name)
    : SensorGroupTemplate(name),
      _value(0),
      _numSensors(0) {}

// Overriding assignment operator so that sensors are not copy-constructed
VariorumSensorGroup &VariorumSensorGroup::operator=(const VariorumSensorGroup &other) {
	SensorGroupInterface::operator=(other);
	_sensors.clear();
	_baseSensors.clear();

	_numSensors = other._numSensors;
	_value = other._value;

	return *this;
}

VariorumSensorGroup::~VariorumSensorGroup() {}

void VariorumSensorGroup::read() {

	// We don't care too much if _value overflows
	reading_t reading;
	//reading.value = _value++;
	//reading.timestamp = getTimestamp();

	int   ret;
	int   num_sockets = 0;
	char *s = NULL;

	/* Determine number of sockets */
	num_sockets = variorum_get_num_sockets();

	if (num_sockets <= 0) {
		LOG(error) << "Variorum Plugin: HWLOC returned an invalid number of sockets!";
		return;
	}

	/* Allocate string based on number of sockets on the platform */
	/* String allocation below assumes the following:
		* Upper bound of 180 characters for hostname, timestamp and node power.
		* Upper bound of 150 characters for per-socket information */
	s = (char *)malloc((num_sockets * 150 + 180) * sizeof(char));

	ret = variorum_get_node_power_json(&s);
	if (ret != 0) {
		LOG(error) << "Variorum Plugin: JSON get node power failed!";
		free(s);
		return;
	}

	boost::property_tree::iptree pt;
	std::istringstream str(s);
	boost::property_tree::read_json(str, pt);

	reading.timestamp = pt.get<uint64_t>("timestamp");

	for (auto s : _sensors) {
#ifdef DEBUG
		LOG(debug) << _groupName << "::" << s->getName() << ": \"" << reading.value << "\"";
#endif
		try {
			float value = pt.get<float>(s->getJsonName());
			reading.value = value * s->getFactor();
			s->storeReading(reading, 1/s->getFactor());

		} catch (const std::exception &e) {
		      LOG(warning) << "Variorum Plugin: error retrieving the json key: " << s->getJsonName();
	      }
	}

	/* Deallocate */
	free(s);

}

void VariorumSensorGroup::printGroupConfig(LOG_LEVEL ll, unsigned leadingSpaces) {
}
