//================================================================================
// Name        : VariorumSensorBase.h
// Author      : Amir Raoofy
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Sensor base class for Variorum plugin.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2018-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

/**
 * @defgroup variorum Variorum plugin
 * @ingroup  pusherplugins
 *
 * @brief Variorum plugin.
 */

#ifndef VARIORUM_VARIORUMSENSORBASE_H_
#define VARIORUM_VARIORUMSENSORBASE_H_

#include "sensorbase.h"

/**
 * @brief SensorBase specialization for this plugin.
 *
 * @ingroup variorum
 */
class VariorumSensorBase : public SensorBase {
    
	public:

    VariorumSensorBase(const std::string& name) :
        SensorBase(name) {
        /*
         * TODO
         * Initialize plugin specific attributes
         */
    }

	VariorumSensorBase(const VariorumSensorBase& other) :
        SensorBase(other) {
        /*
         * TODO
         * Copy construct plugin specific attributes
         */
    }

	virtual ~VariorumSensorBase() {}

	VariorumSensorBase& operator=(const VariorumSensorBase& other) {
        SensorBase::operator=(other);
        /*
         * TODO
         * Implement assignment operator for plugin specific attributes
         */
         
        return *this;
    }

	void setJsonName(string jsonName) { _jsonName = jsonName; }
    string getJsonName() const { return _jsonName; }

	void printConfig(LOG_LEVEL ll, LOGGER &lg, unsigned leadingSpaces = 16) {
		std::string leading(leadingSpaces, ' ');
		LOG_VAR(ll) << leading << "    jsonName:         " << getJsonName();
	}

	protected:
	string _jsonName;
	/*
     * TODO
     * Add plugin specific attributes here
     */
};

#endif /* VARIORUM_VARIORUMSENSORBASE_H_ */
