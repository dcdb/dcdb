//================================================================================
// Name        : VariorumConfigurator.cpp
// Author      : Amir Raoofy
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Source file for Variorum plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2018-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "VariorumConfigurator.h"

VariorumConfigurator::VariorumConfigurator() {
    _groupName = "group";
    _baseName = "sensor";
}

VariorumConfigurator::~VariorumConfigurator() {}

bool VariorumConfigurator::checkConfig() {
    if (_sensorGroups.size() > 1) {
        LOG(error) << "Variorum Plugin: " << _sensorGroups.size() << " sensor groups have been defined, but only 1 is supported.";
        return false;
    }
    return true;
}
bool VariorumConfigurator::sensorBase(VariorumSensorBase &s, CFG_VAL config) {
    ADD {
		ATTRIBUTE("JsonName", setJsonName);
	}
    if (s.getJsonName().empty()){
        s.setJsonName (s.getName());
    }
    
    return true;
}

bool VariorumConfigurator::sensorGroup(VariorumSensorGroup &s, CFG_VAL config) {
    ADD {
        /* 
         * TODO
         * Add ATTRIBUTE macros for sensorGroup attributes
         */
    }
    return true;
}

void VariorumConfigurator::printConfiguratorConfig(LOG_LEVEL ll) {
    /*
     * TODO
     * Log attributes here for debug reasons or delete this method if there are
     * not attributes to log.
     */
}
