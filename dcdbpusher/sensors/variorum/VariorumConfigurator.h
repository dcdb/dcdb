//================================================================================
// Name        : VariorumConfigurator.h
// Author      : Amir Raoofy
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Header file for Variorum plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2018-2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef VARIORUMCONFIGURATOR_H_
#define VARIORUMCONFIGURATOR_H_

#include "../../includes/ConfiguratorTemplate.h"
#include "VariorumSensorGroup.h"

/**
 * @brief ConfiguratorTemplate specialization for this plugin.
 *
 * @ingroup variorum
 */
class VariorumConfigurator : public ConfiguratorTemplate<VariorumSensorBase, VariorumSensorGroup> {

      public:
	VariorumConfigurator();
	virtual ~VariorumConfigurator();

      protected:
	/* Overwritten from ConfiguratorTemplate */
	bool checkConfig() override;
	bool sensorBase(VariorumSensorBase &s, CFG_VAL config) override;
	bool sensorGroup(VariorumSensorGroup &s, CFG_VAL config) override;

    void printConfiguratorConfig(LOG_LEVEL ll) final override;

};

extern "C" ConfiguratorInterface *create() {
	return new VariorumConfigurator;
}

extern "C" void destroy(ConfiguratorInterface *c) {
	delete c;
}

#endif /* VARIORUMCONFIGURATOR_H_ */
