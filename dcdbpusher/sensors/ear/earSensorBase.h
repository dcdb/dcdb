//================================================================================
// Name        : earSensorBase.h
// Author      : Michael Ott (ott@lrz.de)
// Contact     :
// Copyright   :
// Description : Sensor base class for ear plugin.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

/**
 * @defgroup ear ear plugin
 * @ingroup  pusherplugins
 *
 * @brief Describe your plugin in one sentence
 */

#ifndef EAR_EARSENSORBASE_H_
#define EAR_EARSENSORBASE_H_

#include "sensorbase.h"

/**
 * @brief SensorBase specialization for this plugin.
 *
 * @ingroup ear
 */
class earSensorBase : public SensorBase {
      public:
	enum metric_t {
		dc_power = 0,
		dram_power,
		pck_power,
		edp,
		gbs,
		io_mbs,
		tpi,
		cpi,
		gflops,
		time,
		flops_64f,
		flops_128f,
		flops_256f,
		flops_512f,
		flops_64d,
		flops_128d,
		flops_256d,
		flops_512d,
		l1_misses,
		l2_misses,
		l3_misses,
		instructions,
		cycles,
		avg_f,
		avg_imc_f,
		def_f,
		perc_mpi
	};

	earSensorBase(const std::string &name) : SensorBase(name) {
		/*
		 * TODO
		 * Initialize plugin specific attributes
		 */
	}

	earSensorBase(const earSensorBase &other) : SensorBase(other) {
		/*
		 * TODO
		 * Copy construct plugin specific attributes
		 */
	}

	virtual ~earSensorBase() {
		/*
		 * TODO
		 * If necessary, deconstruct plugin specific attributes
		 */
	}

	earSensorBase &operator=(const earSensorBase &other) {
		SensorBase::operator=(other);
		/*
		 * TODO
		 * Implement assignment operator for plugin specific attributes
		 */

		return *this;
	}

	void     setType(metric_t type) { _type = type; }
	metric_t getType() const { return _type; }

	void printConfig(LOG_LEVEL ll, LOGGER &lg, unsigned leadingSpaces = 16) {
		std::string leading(leadingSpaces, ' ');
		LOG_VAR(ll) << leading << "Type:     " << (int)_type;
	}

      protected:
	metric_t _type;
	/*
	 * TODO
	 * Add plugin specific attributes here
	 */
};

#endif /* EAR_EARSENSORBASE_H_ */
