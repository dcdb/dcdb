//================================================================================
// Name        : earConfigurator.cpp
// Author      : Michael Ott (ott@lrz.de)
// Contact     :
// Copyright   :
// Description : Source file for ear plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "earConfigurator.h"
#include "earSensorBase.h"
#include <boost/algorithm/string.hpp>

earConfigurator::earConfigurator() {
	_groupName = "group";
	_baseName = "sensor";
	_metricMap["dc_power"] = earSensorBase::dc_power;
	_metricMap["dram_power"] = earSensorBase::dram_power;
	_metricMap["pck_power"] = earSensorBase::pck_power;
	_metricMap["edp"] = earSensorBase::edp;
	_metricMap["gbs"] = earSensorBase::gbs;
	_metricMap["io_mbs"] = earSensorBase::io_mbs;
	_metricMap["tpi"] = earSensorBase::tpi;
	_metricMap["cpi"] = earSensorBase::cpi;
	_metricMap["gflops"] = earSensorBase::gflops;
	_metricMap["time"] = earSensorBase::time;
	_metricMap["flops_64f"] = earSensorBase::flops_64f;
	_metricMap["flops_128f"] = earSensorBase::flops_128f;
	_metricMap["flops_256f"] = earSensorBase::flops_256f;
	_metricMap["flops_512f"] = earSensorBase::flops_512f;
	_metricMap["flops_64d"] = earSensorBase::flops_64d;
	_metricMap["flops_128d"] = earSensorBase::flops_128d;
	_metricMap["flops_256d"] = earSensorBase::flops_256d;
	_metricMap["flops_512d"] = earSensorBase::flops_512d;
	_metricMap["l1_misses"] = earSensorBase::l1_misses;
	_metricMap["l2_misses"] = earSensorBase::l2_misses;
	_metricMap["l3_misses"] = earSensorBase::l3_misses;
	_metricMap["instructions"] = earSensorBase::instructions;
	_metricMap["cycles"] = earSensorBase::cycles;
	_metricMap["avg_f"] = earSensorBase::avg_f;
	_metricMap["avg_imc_f"] = earSensorBase::avg_imc_f;
	_metricMap["def_f"] = earSensorBase::def_f;
	_metricMap["perc_mpi"] = earSensorBase::perc_mpi;
}

earConfigurator::~earConfigurator() {}

bool earConfigurator::sensorBase(earSensorBase &s, CFG_VAL config) {
	std::string name = boost::algorithm::to_lower_copy(s.getName());
	auto        metric = _metricMap.find(name);
	if (metric != _metricMap.end()) {
		s.setType(metric->second);
	} else {
		LOG(error) << "Unknown metric: " << s.getName();
		return false;
	}
	ADD {
		/*
		 * TODO
		 * Add ATTRIBUTE macros for sensorBase attributes
		 */
		// ATTRIBUTE("attributeName", attributeSetter);
	}
	return true;
}

bool earConfigurator::sensorGroup(earSensorGroup &s, CFG_VAL config) {
	ADD {
		/*
		 * TODO
		 * Add ATTRIBUTE macros for sensorGroup attributes
		 */
	}
	return true;
}

void earConfigurator::printConfiguratorConfig(LOG_LEVEL ll) {
	/*
	 * TODO
	 * Log attributes here for debug reasons or delete this method if there are
	 * not attributes to log.
	 */
}
