//================================================================================
// Name        : earSensorGroup.h
// Author      : Michael Ott (ott@lrz.de)
// Contact     :
// Copyright   :
// Description : Header file for ear sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef EAR_EARSENSORGROUP_H_
#define EAR_EARSENSORGROUP_H_

#include "../../includes/SensorGroupTemplate.h"
#include "earSensorBase.h"

/**
 * @brief SensorGroupTemplate specialization for this plugin.
 *
 * @ingroup ear
 */
class earSensorGroup : public SensorGroupTemplate<earSensorBase> {
      public:
	earSensorGroup(const std::string &name);
	earSensorGroup(const earSensorGroup &other);
	virtual ~earSensorGroup();
	earSensorGroup &operator=(const earSensorGroup &other);

	void execOnInit() final override;
	bool execOnStart() final override;
	void execOnStop() final override;

	bool initShmem();

	/*
	 * TODO
	 * Add getter and setters for group attributes if required
	 */

	void printGroupConfig(LOG_LEVEL ll, unsigned leadingSpaces = 16) final override;

      private:
	typedef struct shmem_data shmem_data_t;

	void read() final override;

	int           _fd;
	shmem_data_t *_shmem;
	int           _lastIndex;
	uint64_t      _lastTs;
};

#endif /* EAR_EARSENSORGROUP_H_ */
