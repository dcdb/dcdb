//================================================================================
// Name        : earSensorGroup.cpp
// Author      : Michael Ott (ott@lrz.de)
// Contact     :
// Copyright   :
// Description : Source file for ear sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2022 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "earSensorGroup.h"

#include "timestamp.h"
#include <common/types/loop.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>

#define MAX_ENTRIES 2
typedef struct shmem_data {
	int index;
	struct measurement {
		uint64_t timestamp;
		loop_t   loop;
	} measurements[MAX_ENTRIES];
} shmem_data_t;

earSensorGroup::earSensorGroup(const std::string &name) : SensorGroupTemplate(name) {
	_shmem = (shmem_data_t *)-1;
	_lastIndex = 0;
	_lastTs = 0;
}

earSensorGroup::earSensorGroup(const earSensorGroup &other) : SensorGroupTemplate(other) {}

earSensorGroup::~earSensorGroup() {
	/*
	 * TODO
	 * Tear down attributes
	 */
}

earSensorGroup &earSensorGroup::operator=(const earSensorGroup &other) {
	SensorGroupTemplate::operator=(other);
	/*
	 * TODO
	 * Implement assignment operator
	 */

	return *this;
}

void earSensorGroup::execOnInit() {}

bool earSensorGroup::execOnStart() {
	/*
	 * TODO
	 * Implement logic before the group starts polling here
	 * (e.g. open a file descriptor) or remove this method if not required.
	 */
	return true;
}

void earSensorGroup::execOnStop() {
	if (munmap(_shmem, sizeof(shmem_data_t)) == -1) {
		LOG(error) << "Error unmapping shared memory: " << strerror(errno);
		return;
	}
	close(_fd);
	_fd = -1;
	_shmem = NULL;
}

bool earSensorGroup::initShmem() {
	_fd = shm_open("/ear_dcdb.dat", O_RDONLY, 0);
	if (_fd == -1) {
		LOG(debug) << "Error opening shmem object: " << strerror(errno);
		return false;
	}
	_shmem = (shmem_data_t *)mmap(0, sizeof(shmem_data_t), PROT_READ, MAP_SHARED, _fd, 0);
	if (_shmem == (shmem_data_t *)-1) {
		LOG(debug) << "Error mapping shared memory: " << strerror(errno);
		return false;
	}

	LOG(debug) << "shmem successfully mapped";
	return true;
}

void earSensorGroup::read() {
	if ((_shmem == (shmem_data_t *)-1) && !initShmem()) {
		return;
	}

#ifdef DEBUG
	LOG(debug) << "index=" << _shmem->index << " ts0=" << _shmem->measurements[0].timestamp << " ts1=" << _shmem->measurements[1].timestamp;
#endif
	if ((_lastIndex == _shmem->index) || _shmem->measurements[_shmem->index].timestamp < _lastTs) {
		return;
	}
	_lastIndex = _shmem->index;

	reading_t reading;
	reading.timestamp = _shmem->measurements[_lastIndex].timestamp;
	_lastTs = reading.timestamp;
	loop_t *loop = &_shmem->measurements[_lastIndex].loop;

	for (auto s : _sensors) {
		switch (s->getType()) {
			case earSensorBase::dc_power:
				reading.value = loop->signature.DC_power * s->getFactor();
				break;
			case earSensorBase::dram_power:
				reading.value = loop->signature.DRAM_power * s->getFactor();
				break;
			case earSensorBase::pck_power:
				reading.value = loop->signature.PCK_power * s->getFactor();
				break;
			case earSensorBase::edp:
				reading.value = loop->signature.EDP * s->getFactor();
				break;
			case earSensorBase::gbs:
				reading.value = loop->signature.GBS * s->getFactor();
				break;
			case earSensorBase::io_mbs:
				reading.value = loop->signature.IO_MBS * s->getFactor();
				break;
			case earSensorBase::tpi:
				reading.value = loop->signature.TPI * s->getFactor();
				break;
			case earSensorBase::cpi:
				reading.value = loop->signature.CPI * s->getFactor();
				break;
			case earSensorBase::gflops:
				reading.value = loop->signature.Gflops * s->getFactor();
				break;
			case earSensorBase::time:
				reading.value = loop->signature.time;
				break;
			case earSensorBase::flops_64f:
				reading.value = loop->signature.FLOPS[0];
				break;
			case earSensorBase::flops_128f:
				reading.value = loop->signature.FLOPS[1];
				break;
			case earSensorBase::flops_256f:
				reading.value = loop->signature.FLOPS[2];
				break;
			case earSensorBase::flops_512f:
				reading.value = loop->signature.FLOPS[3];
				break;
			case earSensorBase::flops_64d:
				reading.value = loop->signature.FLOPS[4];
				break;
			case earSensorBase::flops_128d:
				reading.value = loop->signature.FLOPS[5];
				break;
			case earSensorBase::flops_256d:
				reading.value = loop->signature.FLOPS[6];
				break;
			case earSensorBase::flops_512d:
				reading.value = loop->signature.FLOPS[7];
				break;
			case earSensorBase::l1_misses:
				reading.value = loop->signature.L1_misses;
				break;
			case earSensorBase::l2_misses:
				reading.value = loop->signature.L2_misses;
				break;
			case earSensorBase::l3_misses:
				reading.value = loop->signature.L3_misses;
				break;
			case earSensorBase::instructions:
				reading.value = loop->signature.instructions;
				break;
			case earSensorBase::cycles:
				reading.value = loop->signature.cycles;
				break;
			case earSensorBase::avg_f:
				reading.value = loop->signature.avg_f;
				break;
			case earSensorBase::avg_imc_f:
				reading.value = loop->signature.avg_imc_f;
				break;
			case earSensorBase::def_f:
				reading.value = loop->signature.def_f;
				break;
			case earSensorBase::perc_mpi:
				reading.value = loop->signature.perc_MPI * s->getFactor();
				break;
		}

		if ((s->getType() <= earSensorBase::gflops) || (s->getType() == earSensorBase::perc_mpi)) {
			// We have already applied the factor above, so dont apply it again in storeReading()
			s->storeReading(reading, 1 / s->getFactor());
		} else {
			s->storeReading(reading);
		}
#ifdef DEBUG
		LOG(debug) << _groupName << "::" << s->getName() << " raw reading: \"" << reading.value << "\"";
#endif
	}
}

void earSensorGroup::printGroupConfig(LOG_LEVEL ll, unsigned leadingSpaces) {
	std::string leading(leadingSpaces, ' ');
}
