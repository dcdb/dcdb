//================================================================================
// Name        : SysmanConfigurator.h
// Author      : Alessio Netti
// Contact     :
// Copyright   :
// Description : Header file for sysman plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef SYSMAN_SYSMANCONFIGURATOR_H_
#define SYSMAN_SYSMANCONFIGURATOR_H_

#include "../../includes/ConfiguratorTemplate.h"
#include "SysmanSensorGroup.h"

/**
 * @brief ConfiguratorTemplate specialization for this plugin.
 *
 * @ingroup sysman
 */
class SysmanConfigurator : public ConfiguratorTemplate<SysmanSensorBase, SysmanSensorGroup> {

	typedef std::map<std::string, ZES_FEATURE> zesFeatureMap_t;

      public:
	SysmanConfigurator();
	virtual ~SysmanConfigurator();

      protected:
	void sensorBase(SysmanSensorBase &s, CFG_VAL config) override;
	void sensorGroup(SysmanSensorGroup &sGroup, CFG_VAL config) override;

      private:
	zesFeatureMap_t _zesFeatureMap;
};

extern "C" ConfiguratorInterface *create() {
	return new SysmanConfigurator;
}

extern "C" void destroy(ConfiguratorInterface *c) {
	delete c;
}

#endif /* SYSMAN_SYSMANCONFIGURATOR_H_ */
