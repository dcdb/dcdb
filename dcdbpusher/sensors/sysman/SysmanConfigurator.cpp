//================================================================================
// Name        : SysmanConfigurator.cpp
// Author      : Alessio Netti
// Contact     :
// Copyright   :
// Description : Source file for sysman plugin configurator class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "SysmanConfigurator.h"

SysmanConfigurator::SysmanConfigurator() {
	_zesFeatureMap["energy"] = ZES_ENERGY;
	_zesFeatureMap["frequency"] = ZES_FREQUENCY;
	_zesFeatureMap["throttle_time"] = ZES_THROTTLE;
	_zesFeatureMap["voltage"] = ZES_VOLTAGE;
	_zesFeatureMap["temperature"] = ZES_TEMP;
	_zesFeatureMap["fan_speed"] = ZES_FAN;
	_zesFeatureMap["pci_throughput"] = ZES_PCI_THRU;
	_zesFeatureMap["pci_packets"] = ZES_PCI_PACKETS;
	_zesFeatureMap["pci_tx"] = ZES_PCI_TX;
	_zesFeatureMap["pci_rx"] = ZES_PCI_RX;
	_zesFeatureMap["psu_current"] = ZES_PSU_CURRENT;
	_zesFeatureMap["psu_temperature"] = ZES_PSU_TEMP;
	_zesFeatureMap["mem_free"] = ZES_MEM_FREE;
	_zesFeatureMap["mem_bandwidth_read"] = ZES_MEM_BANDWIDTH_R;
	_zesFeatureMap["mem_bandwidth_write"] = ZES_MEM_BANDWIDTH_W;
	_zesFeatureMap["busy_time"] = ZES_BUSY_TIME;

	_groupName = "group";
	_baseName = "sensor";
}

SysmanConfigurator::~SysmanConfigurator() {}

void SysmanConfigurator::sensorBase(SysmanSensorBase &s, CFG_VAL config) {
	BOOST_FOREACH (boost::property_tree::iptree::value_type &val, config) {
		if (boost::iequals(val.first, "feature")) {
			zesFeatureMap_t::iterator it = _zesFeatureMap.find(val.second.data());
			if (it != _zesFeatureMap.end()) {
				s.setFeatureType(it->second);
			} else {
				LOG(warning) << "  Feature \"" << val.second.data() << "\" not supported!";
			}
		} else if (boost::iequals(val.first, "domainID")) {
			s.setDomain(val.second.data());
		} else if (boost::iequals(val.first, "subDeviceID")) {
			s.setSubDeviceID(stoi(val.second.data()));
		}
	}
}

void SysmanConfigurator::sensorGroup(SysmanSensorGroup &sGroup, CFG_VAL config) {
	BOOST_FOREACH (boost::property_tree::iptree::value_type &val, config) {
		if (boost::iequals(val.first, "driverID")) {
			sGroup.setDriverID((size_t)stoi(val.second.data()));
		} else if (boost::iequals(val.first, "deviceID")) {
			sGroup.setDeviceID((size_t)stoi((val.second.data())));
		}
	}
}
