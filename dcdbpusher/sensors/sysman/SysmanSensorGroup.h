//================================================================================
// Name        : SysmanSensorGroup.h
// Author      : Alessio Netti
// Contact     :
// Copyright   :
// Description : Header file for sysman sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#ifndef SYSMAN_SYSMANSENSORGROUP_H_
#define SYSMAN_SYSMANSENSORGROUP_H_

#include "../../includes/SensorGroupTemplate.h"
#include "SysmanSensorBase.h"
#include <memory>
#include <stdlib.h>

/**
 * @brief SensorGroupTemplate specialization for this plugin.
 *
 * @ingroup sysman
 */
class SysmanSensorGroup : public SensorGroupTemplate<SysmanSensorBase> {

      public:
	SysmanSensorGroup(const std::string &name);
	SysmanSensorGroup(const SysmanSensorGroup &other);

	virtual ~SysmanSensorGroup();
	SysmanSensorGroup &operator=(const SysmanSensorGroup &other);

	void execOnInit() final override;
	bool execOnStart() final override;
	void execOnStop() final override;

	size_t getDriverID() { return _driverID; }
	size_t getDeviceID() { return _deviceID; }

	void setDriverID(size_t did) { _driverID = did; }
	void setDeviceID(size_t did) { _deviceID = did; }

	bool setupHandle(SysmanSensorBase &s);

	void printGroupConfig(LOG_LEVEL ll, unsigned int leadingSpaces) final override;

      private:
	void        read() final override;
	std::string ze_result_string(const ze_result_t result);

	bool checkSubDevice(ze_bool_t onSS, uint32_t idSS, short sensorSS);

	std::shared_ptr<zes_device_handle_t> _deviceHandle;
	size_t                               _driverID;
	size_t                               _deviceID;
};

#endif /* SYSMAN_SYSMANSENSORGROUP_H_ */
