//================================================================================
// Name        : SysmanSensorBase.h
// Author      : Alessio Netti
// Contact     :
// Copyright   :
// Description : Sensor base class for sysman plugin.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

/**
 * @defgroup sysman sysman plugin
 * @ingroup  pusherplugins
 *
 * Collect data from the sysman interface for Intel GPUs
 */

#ifndef SYSMAN_SYSMANSENSORBASE_H_
#define SYSMAN_SYSMANSENSORBASE_H_

#include "sensorbase.h"
#include "ze_api.h"
#include "zes_api.h"
#include "zet_api.h"

enum ZES_FEATURE {
	ZES_ENERGY = 0,
	ZES_FREQUENCY = 1,
	ZES_VOLTAGE = 2,
	ZES_THROTTLE = 3,
	ZES_TEMP = 4,
	ZES_FAN = 5,
	ZES_PCI_THRU = 6,
	ZES_PCI_PACKETS = 7,
	ZES_PCI_TX = 8,
	ZES_PCI_RX = 9,
	ZES_PSU_CURRENT = 10,
	ZES_PSU_TEMP = 11,
	ZES_MEM_FREE = 12,
	ZES_MEM_BANDWIDTH_R = 13,
	ZES_MEM_BANDWIDTH_W = 14,
	ZES_BUSY_TIME = 15,
	ZES_INVALID = 16
};

/**
 * @brief
 *
 *
 * @ingroup sysman
 */
class SysmanSensorBase : public SensorBase {
      public:
	SysmanSensorBase(const std::string &name) : SensorBase(name) {
		_featureType = ZES_INVALID;
		_subDeviceID = -1;
		_domain = "global";
		_handle = NULL;
	}

	SysmanSensorBase(const SysmanSensorBase &other)
	    : SensorBase(other), _featureType(other._featureType), _subDeviceID(other._subDeviceID), _domain(other._domain), _handle(NULL) {}

	SysmanSensorBase &operator=(const SysmanSensorBase &other) {
		SensorBase::operator=(other);
		_featureType = other._featureType;
		_subDeviceID = other._subDeviceID;
		_domain = other._domain;
		_handle = NULL;
		return *this;
	}

	virtual ~SysmanSensorBase() { resetHandle(); }

	int         getFeatureType() const { return _featureType; }
	short       getSubDeviceID() const { return _subDeviceID; }
	std::string getDomain() const { return _domain; }
	bool        hasHandle() const { return _handle; }

	void setFeatureType(int featureType) { _featureType = static_cast<ZES_FEATURE>(featureType); }
	void setSubDeviceID(size_t s) { _subDeviceID = s; }
	void setDomain(std::string dom) { _domain = dom; }
	void setHandle(void *h) { _handle = h; }

	void resetHandle() {
		if (!_handle) {
			return;
		}

		if (_featureType == ZES_ENERGY) {
			zes_pwr_handle_t *pwrHandle = (zes_pwr_handle_t *)_handle;
			delete pwrHandle;
		} else if (_featureType == ZES_FREQUENCY || _featureType == ZES_VOLTAGE || _featureType == ZES_THROTTLE) {
			zes_freq_handle_t *freqHandle = (zes_freq_handle_t *)_handle;
			delete freqHandle;
		} else if (_featureType == ZES_TEMP) {
			zes_temp_handle_t *tempHandle = (zes_temp_handle_t *)_handle;
			delete tempHandle;
		} else if (_featureType == ZES_FAN) {
			zes_fan_handle_t *fanHandle = (zes_fan_handle_t *)_handle;
			delete fanHandle;
		} else if (_featureType == ZES_PCI_THRU || _featureType == ZES_PCI_PACKETS || _featureType == ZES_PCI_TX || _featureType == ZES_PCI_RX) {
			zes_device_handle_t *devHandle = (zes_device_handle_t *)_handle;
			delete devHandle;
		} else if (_featureType == ZES_PSU_CURRENT || _featureType == ZES_PSU_TEMP) {
			zes_psu_handle_t *psuHandle = (zes_psu_handle_t *)_handle;
			delete psuHandle;
		} else if (_featureType == ZES_MEM_FREE || _featureType == ZES_MEM_BANDWIDTH_R || _featureType == ZES_MEM_BANDWIDTH_W) {
			zes_mem_handle_t *memHandle = (zes_mem_handle_t *)_handle;
			delete memHandle;
		}

		_handle = NULL;
	}

	ze_result_t sampleSensor(uint64_t ts) {
		if (!_handle) {
			return ZE_RESULT_ERROR_UNKNOWN;
		}

		ze_result_t status = ZE_RESULT_SUCCESS;
		reading_t   r;
		r.value = 0;
		r.timestamp = ts;

		if (_featureType == ZES_ENERGY) {
			zes_power_energy_counter_t val;
			zes_pwr_handle_t *         pwrHandle = (zes_pwr_handle_t *)_handle;
			if ((status = zesPowerGetEnergyCounter(*pwrHandle, &val)) == ZE_RESULT_SUCCESS) {
				r.value = val.energy;
			}
		} else if (_featureType == ZES_FREQUENCY || _featureType == ZES_VOLTAGE) {
			zes_freq_state_t   freqState;
			zes_freq_handle_t *freqHandle = (zes_freq_handle_t *)_handle;
			if ((status = zesFrequencyGetState(*freqHandle, &freqState)) == ZE_RESULT_SUCCESS) {
				r.value = _featureType == ZES_FREQUENCY ? freqState.actual : freqState.currentVoltage;
			}
		} else if (_featureType == ZES_THROTTLE) {
			zes_freq_throttle_time_t throttleState;
			zes_freq_handle_t *      freqHandle = (zes_freq_handle_t *)_handle;
			if ((status = zesFrequencyGetThrottleTime(*freqHandle, &throttleState)) == ZE_RESULT_SUCCESS) {
				r.value = throttleState.throttleTime;
			}
		} else if (_featureType == ZES_TEMP) {
			double             temperature = 0.0;
			zes_temp_handle_t *tempHandle = (zes_temp_handle_t *)_handle;
			if ((status = zesTemperatureGetState(*tempHandle, &temperature)) == ZE_RESULT_SUCCESS) {
				r.value = (int64_t)temperature;
			}
		} else if (_featureType == ZES_FAN) {
			int32_t               fSpeed = 0;
			zes_fan_speed_units_t fUnit = ZES_FAN_SPEED_UNITS_RPM;
			zes_fan_handle_t *    fanHandle = (zes_fan_handle_t *)_handle;
			if ((status = zesFanGetState(*fanHandle, fUnit, &fSpeed)) == ZE_RESULT_SUCCESS) {
				r.value = (int64_t)fSpeed;
			}
		} else if (_featureType == ZES_PCI_THRU || _featureType == ZES_PCI_PACKETS || _featureType == ZES_PCI_TX || _featureType == ZES_PCI_RX) {
			zes_pci_stats_t      pciStats;
			zes_device_handle_t *devHandle = (zes_device_handle_t *)_handle;
			if ((status = zesDevicePciGetStats(*devHandle, &pciStats)) == ZE_RESULT_SUCCESS) {
				if (_featureType == ZES_PCI_THRU) {
					r.value = (int64_t)pciStats.speed.maxBandwidth;
				} else if (_featureType == ZES_PCI_PACKETS) {
					r.value = (int64_t)pciStats.packetCounter;
				} else if (_featureType == ZES_PCI_TX) {
					r.value = (int64_t)pciStats.txCounter;
				} else if (_featureType == ZES_PCI_RX) {
					r.value = (int64_t)pciStats.rxCounter;
				}
			}
		} else if (_featureType == ZES_PSU_CURRENT || _featureType == ZES_PSU_TEMP) {
			zes_psu_state_t   psuState;
			zes_psu_handle_t *psuHandle = (zes_psu_handle_t *)_handle;
			if ((status = zesPsuGetState(*psuHandle, &psuState)) == ZE_RESULT_SUCCESS) {
				if (_featureType == ZES_PSU_CURRENT) {
					r.value = (int64_t)psuState.current;
				} else if (_featureType == ZES_PSU_TEMP) {
					r.value = (int64_t)psuState.temperature;
				}
			}
		} else if (_featureType == ZES_MEM_FREE) {
			zes_mem_state_t   memState;
			zes_mem_handle_t *memHandle = (zes_mem_handle_t *)_handle;
			if ((status = zesMemoryGetState(*memHandle, &memState)) == ZE_RESULT_SUCCESS) {
				r.value = (int64_t)memState.free;
			}
		} else if (_featureType == ZES_MEM_BANDWIDTH_R || _featureType == ZES_MEM_BANDWIDTH_W) {
			zes_mem_bandwidth_t memBw;
			zes_mem_handle_t *  memHandle = (zes_mem_handle_t *)_handle;
			if ((status = zesMemoryGetBandwidth(*memHandle, &memBw)) == ZE_RESULT_SUCCESS) {
				r.value = _featureType == ZES_MEM_BANDWIDTH_R ? (int64_t)memBw.readCounter : (int64_t)memBw.writeCounter;
			}
		} else if (_featureType == ZES_BUSY_TIME) {
			zes_engine_stats_t   engineStats;
			zes_engine_handle_t *engineHandle = (zes_engine_handle_t *)_handle;
			if ((status = zesEngineGetActivity(*engineHandle, &engineStats)) == ZE_RESULT_SUCCESS) {
				r.value = (int64_t)engineStats.activeTime;
			}
		}

		// std::cout << _name << " : " << r.value << std::endl;
		if (status == ZE_RESULT_SUCCESS) {
			storeReading(r);
		}
		return status;
	}

	void printConfig(LOG_LEVEL ll, LOGGER &lg, unsigned leadingSpaces = 16) {
		std::string leading(leadingSpaces, ' ');
		std::string feature("invalid");

		if (_featureType == ZES_ENERGY) {
			feature = "energy";
		} else if (_featureType == ZES_FREQUENCY) {
			feature = "frequency";
		} else if (_featureType == ZES_VOLTAGE) {
			feature = "voltage";
		} else if (_featureType == ZES_THROTTLE) {
			feature = "throttle_time";
		} else if (_featureType == ZES_TEMP) {
			feature = "temperature";
		} else if (_featureType == ZES_FAN) {
			feature = "fan_speed";
		} else if (_featureType == ZES_PCI_THRU) {
			feature = "pci_throughput";
		} else if (_featureType == ZES_PCI_PACKETS) {
			feature = "pci_packets";
		} else if (_featureType == ZES_PCI_TX) {
			feature = "pci_tx";
		} else if (_featureType == ZES_PCI_RX) {
			feature = "pci_rx";
		} else if (_featureType == ZES_PSU_CURRENT) {
			feature = "psu_current";
		} else if (_featureType == ZES_PSU_TEMP) {
			feature = "psu_temperature";
		} else if (_featureType == ZES_MEM_FREE) {
			feature = "mem_free";
		} else if (_featureType == ZES_MEM_BANDWIDTH_R) {
			feature = "mem_bandwidth_read";
		} else if (_featureType == ZES_MEM_BANDWIDTH_W) {
			feature = "mem_bandwidth_write";
		} else if (_featureType == ZES_BUSY_TIME) {
			feature = "busy_time";
		}

		LOG_VAR(ll) << leading << "    Feature type:  " << feature;
		LOG_VAR(ll) << leading << "    Subdevice ID:  " << _subDeviceID;
		LOG_VAR(ll) << leading << "    Domain:        " << _domain;
	}

      protected:
	ZES_FEATURE _featureType;
	short       _subDeviceID;
	std::string _domain;
	void *      _handle;
};

#endif /* SYSMAN_SYSMANSENSORBASE_H_ */
