//================================================================================
// Name        : SysmanSensorGroup.cpp
// Author      : Alessio Netti
// Contact     :
// Copyright   :
// Description : Source file for sysman sensor group class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2019 Leibniz Supercomputing Centre
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//================================================================================

#include "SysmanSensorGroup.h"
#include "timestamp.h"

SysmanSensorGroup::SysmanSensorGroup(const std::string &name) : SensorGroupTemplate(name), _deviceHandle(nullptr) {

	_driverID = 0;
	_deviceID = 0;
}

SysmanSensorGroup::SysmanSensorGroup(const SysmanSensorGroup &other)
    : SensorGroupTemplate(other), _deviceHandle(nullptr), _driverID(other._driverID), _deviceID(other._deviceID) {}

SysmanSensorGroup &SysmanSensorGroup::operator=(const SysmanSensorGroup &other) {
	SensorGroupTemplate::operator=(other);
	_deviceHandle.reset();
	_driverID = other._driverID;
	_deviceID = other._deviceID;
	return *this;
}

SysmanSensorGroup::~SysmanSensorGroup() {}

void SysmanSensorGroup::execOnInit() {
	putenv(const_cast<char *>("ZES_ENABLE_SYSMAN=1"));
	// Initializing ZES library
	zeInit(0);
}

bool SysmanSensorGroup::execOnStart() {
	ze_driver_handle_t pDriver = nullptr;
	ze_device_handle_t pDevice = nullptr;
	ze_result_t        status;

	// Getting available drivers and selecting the desired one
	uint32_t driverCount = 0;
	status = zeDriverGet(&driverCount, nullptr);
	if (status != ZE_RESULT_SUCCESS) {
		LOG(error) << _groupName << ": zeDriverGet failed with return code: " << ze_result_string(status);
		return false;
	} else if (_driverID >= driverCount) {
		LOG(error) << _groupName << ": invalid driver ID specified!";
		return false;
	}

	std::vector<ze_driver_handle_t> drivers(driverCount);
	status = zeDriverGet(&driverCount, drivers.data());
	if (status != ZE_RESULT_SUCCESS) {
		LOG(error) << _groupName << ": zeDriverGet failed with return code: " << ze_result_string(status);
		return false;
	}
	pDriver = drivers[_driverID];

	// Getting available devices (within the driver) and selecting one
	uint32_t deviceCount = 0;
	status = zeDeviceGet(pDriver, &deviceCount, nullptr);
	if (status != ZE_RESULT_SUCCESS) {
		LOG(error) << _groupName << ": zeDeviceGet failed with return code: " << ze_result_string(status);
		return false;
	} else if (_deviceID >= deviceCount) {
		LOG(error) << _groupName << ": invalid device ID specified!";
		return false;
	}

	std::vector<ze_device_handle_t> devices(deviceCount);
	status = zeDeviceGet(pDriver, &deviceCount, devices.data());
	if (status != ZE_RESULT_SUCCESS) {
		LOG(error) << _groupName << ": zeDeviceGet failed with return code: " << ze_result_string(status);
		return false;
	}
	_deviceHandle = std::make_shared<zes_device_handle_t>((zes_device_handle_t)devices[_deviceID]);

	for (auto &s : _sensors) {
		if (!setupHandle(*s)) {
			LOG(error) << _groupName << "::" << s->getName() << ": failed to setup metric handle!";
		}
	}

	return true;
}

void SysmanSensorGroup::execOnStop() {
	for (auto &s : _sensors) {
		s->resetHandle();
	}

	_deviceHandle.reset();
}

void SysmanSensorGroup::read() {
	if (!_deviceHandle.get()) {
		LOG(error) << _groupName << ": device is not initialized!";
		return;
	}

	ze_result_t status = ZE_RESULT_SUCCESS;
	uint64_t    ts = getTimestamp();
	for (auto &s : _sensors) {
		if (s->hasHandle() && (status = s->sampleSensor(ts)) != ZE_RESULT_SUCCESS) {
			LOG(warning) << _groupName << "::" << s->getName() << ": sensor sampling failed with return code: " << ze_result_string(status);
		}
	}
}

void SysmanSensorGroup::printGroupConfig(LOG_LEVEL ll, unsigned int leadingSpaces) {
	LOG_VAR(ll) << "            Driver ID:            " << _driverID;
	LOG_VAR(ll) << "            Device ID:            " << _deviceID;
}

bool SysmanSensorGroup::setupHandle(SysmanSensorBase &s) {
	if (!_deviceHandle.get()) {
		return false;
	}

	// Storing the sensor's domain ID for simplicity
	std::string sDom = s.getDomain();

	// Setting up metric handles - all code is unfortunately ad-hoc for each specific metric,
	// so there is a lot of duplication due to the different data structures.
	if (s.getFeatureType() == ZES_ENERGY) {
		if (sDom != "global") {
			LOG(error) << _groupName << "::" << s.getName() << ": domain \"" << s.getDomain() << "\" unsupported!";
			return false;
		}

		uint32_t numPowerDomains;
		if (zesDeviceEnumPowerDomains(*_deviceHandle, &numPowerDomains, NULL) == ZE_RESULT_SUCCESS) {
			std::vector<zes_pwr_handle_t> phPower(numPowerDomains);
			if (zesDeviceEnumPowerDomains(*_deviceHandle, &numPowerDomains, phPower.data()) == ZE_RESULT_SUCCESS) {
				for (size_t pwrIndex = 0; pwrIndex < numPowerDomains; pwrIndex++) {
					zes_power_properties_t props;
					if (zesPowerGetProperties(phPower[pwrIndex], &props) == ZE_RESULT_SUCCESS) {
						if (checkSubDevice(props.onSubdevice, props.subdeviceId, s.getSubDeviceID())) {
							s.setHandle((void *)new zes_pwr_handle_t(phPower[pwrIndex]));
							return true;
						}
					}
				}
			}
		}
	} else if (s.getFeatureType() == ZES_FREQUENCY || s.getFeatureType() == ZES_VOLTAGE || s.getFeatureType() == ZES_THROTTLE) {
		if (sDom != "gpu" && sDom != "mem") {
			LOG(error) << _groupName << "::" << s.getName() << ": domain \"" << s.getDomain() << "\" unsupported!";
			return false;
		}

		uint32_t numFrequencyDomains;
		if (zesDeviceEnumFrequencyDomains(*_deviceHandle, &numFrequencyDomains, NULL) == ZE_RESULT_SUCCESS) {
			std::vector<zes_freq_handle_t> phFrequency(numFrequencyDomains);
			if (zesDeviceEnumFrequencyDomains(*_deviceHandle, &numFrequencyDomains, phFrequency.data()) == ZE_RESULT_SUCCESS) {
				for (size_t freqIndex = 0; freqIndex < numFrequencyDomains; freqIndex++) {
					zes_freq_properties_t props;
					if (zesFrequencyGetProperties(phFrequency[freqIndex], &props) == ZE_RESULT_SUCCESS) {
						if (checkSubDevice(props.onSubdevice, props.subdeviceId, s.getSubDeviceID())) {
							if ((sDom == "gpu" && props.type == ZES_FREQ_DOMAIN_GPU) ||
							    (sDom == "mem" && props.type == ZES_FREQ_DOMAIN_MEMORY)) {
								s.setHandle((void *)new zes_freq_handle_t(phFrequency[freqIndex]));
								return true;
							}
						}
					}
				}
			}
		}
	} else if (s.getFeatureType() == ZES_TEMP) {
		if (sDom != "global" && sDom != "gpu" && sDom != "mem") {
			LOG(error) << _groupName << "::" << s.getName() << ": domain \"" << s.getDomain() << "\" unsupported!";
			return false;
		}

		uint32_t numTempSensors;
		if (zesDeviceEnumTemperatureSensors(*_deviceHandle, &numTempSensors, NULL) == ZE_RESULT_SUCCESS) {
			std::vector<zes_temp_handle_t> phTempSensors(numTempSensors);
			if (zesDeviceEnumTemperatureSensors(*_deviceHandle, &numTempSensors, phTempSensors.data()) == ZE_RESULT_SUCCESS) {
				for (size_t tempIndex = 0; tempIndex < numTempSensors; tempIndex++) {
					zes_temp_properties_t props;
					if (zesTemperatureGetProperties(phTempSensors[tempIndex], &props) == ZE_RESULT_SUCCESS) {
						if (checkSubDevice(props.onSubdevice, props.subdeviceId, s.getSubDeviceID())) {
							if ((sDom == "global" && props.type == ZES_TEMP_SENSORS_GLOBAL) ||
							    (sDom == "gpu" && props.type == ZES_TEMP_SENSORS_GPU) ||
							    (sDom == "mem" && props.type == ZES_TEMP_SENSORS_MEMORY)) {
								s.setHandle((void *)new zes_temp_handle_t(phTempSensors[tempIndex]));
								return true;
							}
						}
					}
				}
			}
		}
	} else if (s.getFeatureType() == ZES_FAN) {
		if (sDom != "global") {
			LOG(error) << _groupName << "::" << s.getName() << ": domain \"" << s.getDomain() << "\" unsupported!";
			return false;
		}

		uint32_t numFans;
		if (zesDeviceEnumFans(*_deviceHandle, &numFans, NULL) == ZE_RESULT_SUCCESS) {
			std::vector<zes_fan_handle_t> phFans(numFans);
			if (zesDeviceEnumFans(*_deviceHandle, &numFans, phFans.data()) == ZE_RESULT_SUCCESS) {
				for (size_t fanIndex = 0; fanIndex < numFans; fanIndex++) {
					zes_fan_properties_t props;
					if (zesFanGetProperties(phFans[fanIndex], &props) == ZE_RESULT_SUCCESS) {
						if (checkSubDevice(props.onSubdevice, props.subdeviceId, s.getSubDeviceID())) {
							s.setHandle((void *)new zes_fan_handle_t(phFans[fanIndex]));
							return true;
						}
					}
				}
			}
		}
	} else if (s.getFeatureType() == ZES_PCI_THRU || s.getFeatureType() == ZES_PCI_PACKETS || s.getFeatureType() == ZES_PCI_TX ||
		   s.getFeatureType() == ZES_PCI_RX) {
		if (sDom != "global") {
			LOG(error) << _groupName << "::" << s.getName() << ": domain \"" << s.getDomain() << "\" unsupported!";
			return false;
		} else if (s.getSubDeviceID() != -1) {
			LOG(error) << _groupName << "::" << s.getName() << ": sub-devices are not supported for this property!";
			return false;
		}

		s.setHandle((void *)new zes_device_handle_t(*_deviceHandle));
		return true;
	} else if (s.getFeatureType() == ZES_PSU_CURRENT || s.getFeatureType() == ZES_PSU_TEMP) {
		if (sDom != "global") {
			LOG(error) << _groupName << "::" << s.getName() << ": domain \"" << s.getDomain() << "\" unsupported!";
			return false;
		}

		uint32_t numPsus;
		if (zesDeviceEnumPsus(*_deviceHandle, &numPsus, NULL) == ZE_RESULT_SUCCESS) {
			std::vector<zes_psu_handle_t> phPsus(numPsus);
			if (zesDeviceEnumPsus(*_deviceHandle, &numPsus, phPsus.data()) == ZE_RESULT_SUCCESS) {
				for (size_t psuIndex = 0; psuIndex < numPsus; psuIndex++) {
					zes_psu_properties_t props;
					if (zesPsuGetProperties(phPsus[psuIndex], &props) == ZE_RESULT_SUCCESS) {
						if (checkSubDevice(props.onSubdevice, props.subdeviceId, s.getSubDeviceID())) {
							s.setHandle((void *)new zes_psu_handle_t(phPsus[psuIndex]));
							return true;
						}
					}
				}
			}
		}
	} else if (s.getFeatureType() == ZES_MEM_FREE || s.getFeatureType() == ZES_MEM_BANDWIDTH_R || s.getFeatureType() == ZES_MEM_BANDWIDTH_W) {
		if (sDom != "hbm" && sDom != "ddr") {
			LOG(error) << _groupName << "::" << s.getName() << ": domain \"" << s.getDomain() << "\" unsupported!";
			return false;
		}

		uint32_t numMem;
		if (zesDeviceEnumMemoryModules(*_deviceHandle, &numMem, NULL) == ZE_RESULT_SUCCESS) {
			std::vector<zes_mem_handle_t> phMem(numMem);
			if (zesDeviceEnumMemoryModules(*_deviceHandle, &numMem, phMem.data()) == ZE_RESULT_SUCCESS) {
				for (size_t memIndex = 0; memIndex < numMem; memIndex++) {
					zes_mem_properties_t props;
					if (zesMemoryGetProperties(phMem[memIndex], &props) == ZE_RESULT_SUCCESS) {
						if (checkSubDevice(props.onSubdevice, props.subdeviceId, s.getSubDeviceID())) {
							if ((sDom == "hbm" && props.type == ZES_MEM_TYPE_HBM) ||
							    (sDom == "ddr" && props.type == ZES_MEM_TYPE_DDR)) {
								s.setHandle((void *)new zes_mem_handle_t(phMem[memIndex]));
								return true;
							}
						}
					}
				}
			}
		}
	} else if (s.getFeatureType() == ZES_BUSY_TIME) {
		if (sDom != "global" && sDom != "compute" && sDom != "media" && sDom != "copy") {
			LOG(error) << _groupName << "::" << s.getName() << ": domain \"" << s.getDomain() << "\" unsupported!";
			return false;
		}

		uint32_t numEngines;
		if (zesDeviceEnumEngineGroups(*_deviceHandle, &numEngines, NULL) == ZE_RESULT_SUCCESS) {
			std::vector<zes_engine_handle_t> phEngine(numEngines);
			if (zesDeviceEnumEngineGroups(*_deviceHandle, &numEngines, phEngine.data()) == ZE_RESULT_SUCCESS) {
				for (size_t engIndex = 0; engIndex < numEngines; engIndex++) {
					zes_engine_properties_t props;
					if (zesEngineGetProperties(phEngine[engIndex], &props) == ZE_RESULT_SUCCESS) {
						if (checkSubDevice(props.onSubdevice, props.subdeviceId, s.getSubDeviceID())) {
							if ((sDom == "global" && props.type == ZES_ENGINE_GROUP_ALL) ||
							    (sDom == "compute" && props.type == ZES_ENGINE_GROUP_COMPUTE_ALL) ||
							    (sDom == "media" && props.type == ZES_ENGINE_GROUP_MEDIA_ALL) ||
							    (sDom == "copy" && props.type == ZES_ENGINE_GROUP_COPY_ALL)) {
								s.setHandle((void *)new zes_engine_handle_t(phEngine[engIndex]));
								return true;
							}
						}
					}
				}
			}
		}
	}

	LOG(error) << _groupName << "::" << s.getName() << ": cannot initialize metric handle, please check that domainID and subDeviceID are correct!";
	return false;
}

bool SysmanSensorGroup::checkSubDevice(ze_bool_t onSS, uint32_t idSS, short sensorSS) {
	bool valid = (!onSS && sensorSS == -1) || (onSS && (short)idSS == sensorSS);
	return valid;
}

std::string SysmanSensorGroup::ze_result_string(const ze_result_t result) {
	if (result == ZE_RESULT_SUCCESS) {
		return "ZE_RESULT_SUCCESS";
	} else if (result == ZE_RESULT_NOT_READY) {
		return "ZE_RESULT_NOT_READY";
	} else if (result == ZE_RESULT_ERROR_UNINITIALIZED) {
		return "ZE_RESULT_ERROR_UNINITIALIZED";
	} else if (result == ZE_RESULT_ERROR_DEVICE_LOST) {
		return "ZE_RESULT_ERROR_DEVICE_LOST";
	} else if (result == ZE_RESULT_ERROR_INVALID_ARGUMENT) {
		return "ZE_RESULT_ERROR_INVALID_ARGUMENT";
	} else if (result == ZE_RESULT_ERROR_OUT_OF_HOST_MEMORY) {
		return "ZE_RESULT_ERROR_OUT_OF_HOST_MEMORY";
	} else if (result == ZE_RESULT_ERROR_OUT_OF_DEVICE_MEMORY) {
		return "ZE_RESULT_ERROR_OUT_OF_DEVICE_MEMORY";
	} else if (result == ZE_RESULT_ERROR_MODULE_BUILD_FAILURE) {
		return "ZE_RESULT_ERROR_MODULE_BUILD_FAILURE";
	} else if (result == ZE_RESULT_ERROR_INSUFFICIENT_PERMISSIONS) {
		return "ZE_RESULT_ERROR_INSUFFICIENT_PERMISSIONS";
	} else if (result == ZE_RESULT_ERROR_NOT_AVAILABLE) {
		return "ZE_RESULT_ERROR_NOT_AVAILABLE";
	} else if (result == ZE_RESULT_ERROR_UNSUPPORTED_VERSION) {
		return "ZE_RESULT_ERROR_UNSUPPORTED_VERSION";
	} else if (result == ZE_RESULT_ERROR_UNSUPPORTED_FEATURE) {
		return "ZE_RESULT_ERROR_UNSUPPORTED_FEATURE";
	} else if (result == ZE_RESULT_ERROR_INVALID_NULL_HANDLE) {
		return "ZE_RESULT_ERROR_INVALID_NULL_HANDLE";
	} else if (result == ZE_RESULT_ERROR_HANDLE_OBJECT_IN_USE) {
		return "ZE_RESULT_ERROR_HANDLE_OBJECT_IN_USE";
	} else if (result == ZE_RESULT_ERROR_INVALID_NULL_POINTER) {
		return "ZE_RESULT_ERROR_INVALID_NULL_POINTER";
	} else if (result == ZE_RESULT_ERROR_INVALID_SIZE) {
		return "ZE_RESULT_ERROR_INVALID_SIZE";
	} else if (result == ZE_RESULT_ERROR_UNSUPPORTED_SIZE) {
		return "ZE_RESULT_ERROR_UNSUPPORTED_SIZE";
	} else if (result == ZE_RESULT_ERROR_UNSUPPORTED_ALIGNMENT) {
		return "ZE_RESULT_ERROR_UNSUPPORTED_ALIGNMENT";
	} else if (result == ZE_RESULT_ERROR_INVALID_SYNCHRONIZATION_OBJECT) {
		return "ZE_RESULT_ERROR_INVALID_SYNCHRONIZATION_OBJECT";
	} else if (result == ZE_RESULT_ERROR_INVALID_ENUMERATION) {
		return "ZE_RESULT_ERROR_INVALID_ENUMERATION";
	} else if (result == ZE_RESULT_ERROR_UNSUPPORTED_ENUMERATION) {
		return "ZE_RESULT_ERROR_UNSUPPORTED_ENUMERATION";
	} else if (result == ZE_RESULT_ERROR_UNSUPPORTED_IMAGE_FORMAT) {
		return "ZE_RESULT_ERROR_UNSUPPORTED_IMAGE_FORMAT";
	} else if (result == ZE_RESULT_ERROR_INVALID_NATIVE_BINARY) {
		return "ZE_RESULT_ERROR_INVALID_NATIVE_BINARY";
	} else if (result == ZE_RESULT_ERROR_INVALID_GLOBAL_NAME) {
		return "ZE_RESULT_ERROR_INVALID_GLOBAL_NAME";
	} else if (result == ZE_RESULT_ERROR_INVALID_KERNEL_NAME) {
		return "ZE_RESULT_ERROR_INVALID_KERNEL_NAME";
	} else if (result == ZE_RESULT_ERROR_INVALID_FUNCTION_NAME) {
		return "ZE_RESULT_ERROR_INVALID_FUNCTION_NAME";
	} else if (result == ZE_RESULT_ERROR_INVALID_GROUP_SIZE_DIMENSION) {
		return "ZE_RESULT_ERROR_INVALID_GROUP_SIZE_DIMENSION";
	} else if (result == ZE_RESULT_ERROR_INVALID_GLOBAL_WIDTH_DIMENSION) {
		return "ZE_RESULT_ERROR_INVALID_GLOBAL_WIDTH_DIMENSION";
	} else if (result == ZE_RESULT_ERROR_INVALID_KERNEL_ARGUMENT_INDEX) {
		return "ZE_RESULT_ERROR_INVALID_KERNEL_ARGUMENT_INDEX";
	} else if (result == ZE_RESULT_ERROR_INVALID_KERNEL_ARGUMENT_SIZE) {
		return "ZE_RESULT_ERROR_INVALID_KERNEL_ARGUMENT_SIZE";
	} else if (result == ZE_RESULT_ERROR_INVALID_KERNEL_ATTRIBUTE_VALUE) {
		return "ZE_RESULT_ERROR_INVALID_KERNEL_ATTRIBUTE_VALUE";
	} else if (result == ZE_RESULT_ERROR_INVALID_COMMAND_LIST_TYPE) {
		return "ZE_RESULT_ERROR_INVALID_COMMAND_LIST_TYPE";
	} else if (result == ZE_RESULT_ERROR_OVERLAPPING_REGIONS) {
		return "ZE_RESULT_ERROR_OVERLAPPING_REGIONS";
	} else if (result == ZE_RESULT_ERROR_UNKNOWN) {
		return "ZE_RESULT_ERROR_UNKNOWN";
	} else {
		return "Unknown ze_result_t value: " + std::to_string(static_cast<int>(result));
	}
}
