include ../config.mk

CXXFLAGS += -DBOOST_NETWORK_ENABLE_HTTPS \
           -I../common/include \
	   -I$(DCDBSRCPATH)/lib/include \
           -I$(DCDBDEPLOYPATH)/include \
           -I$(OPENSSL_INCL) \
           -I$(DCDBDEPSPATH)/bacnet-stack-$(BACNET-STACK_VERSION)/src \
           -Isensors/ear/EAR_4.0.1/src

ifdef NVML_INCLUDE
	CXXFLAGS+= -I$(NVML_INCLUDE)
endif

ifneq (,$(wildcard /usr/include/opamgt))
	CXXFLAGS += -I/usr/include/opamgt
endif
           
LIBS = -L$(DCDBDEPLOYPATH)/lib/ \
       -L$(OPENSSL_LIB) \
       -ldl \
       -lmosquitto \
       -lboost_system \
       -lboost_thread \
       -lboost_filesystem \
       -lboost_log_setup \
       -lboost_log \
       -lboost_regex \
       -lpthread \
       -lcrypto \
       -lssl \
       -rdynamic
       
OBJS = dcdbpusher.o \
       Configuration.o \
       MQTTPusher.o \
       RestAPI.o \
       PluginManager.o \
       ../analytics/OperatorManager.o \
       ../common/src/sensornavigator.o \
       ../common/src/globalconfiguration.o \
       ../common/src/logging.o \
       ../common/src/RESTHttpsServer.o

TARGET = dcdbpusher

ifeq ($(OS),Darwin)
	BACNET_PORT = bsd
	LIBEXT = dylib
	LIBFLAGS = $(LDFLAGS) -dynamiclib -install_name 
else
	LIBS+= -Wl,-rpath-link=$(DCDBDEPLOYPATH)/lib/
	BACNET_PORT = linux
	LIBEXT = so
	LIBFLAGS = $(LDFLAGS) -shared -Wl,-soname,
	PLUGINFLAGS = -fPIC
endif
PLUGIN_LIBS = $(foreach p,$(PLUGINS),libdcdbplugin_$(p).$(LIBEXT))

all:	$(TARGET) $(PLUGIN_LIBS)

$(TARGET): $(foreach f,$(DISTFILESPATHS),$(DCDBDEPSPATH)/$(f)/.installed) $(OBJS)
	$(CXX) $(LDFLAGS) -o $(TARGET) $(OBJS) $(LIBS)

debug: CXXFLAGS += -DDEBUG
debug: all

info:
	@echo $(PLUGINS) $(PLUGIN_LIBS)

clean:
	rm -f $(PLUGIN_LIBS) $(TARGET) $(OBJS) $(foreach p,$(PLUGINS),sensors/$(p)/*.o)

$(OBJS)	: %.o : %.cpp

install_lib: $(PLUGIN_LIBS)
	install $^ $(DCDBDEPLOYPATH)/lib/

install_conf: $(foreach p,dcdbpusher $(PLUGINS),config/$(p).conf)
	install -m 644 $^ $(DCDBDEPLOYPATH)/etc/

install: $(TARGET) install_lib
	install $(TARGET) $(DCDBDEPLOYPATH)/bin/
	@echo "Done with installation."
	@echo "====================================="
	@echo "To copy the configuration files type:"
	@echo "				     > make install_conf"

sensors/%.o: CXXFLAGS+= $(PLUGINFLAGS)
../common/src/sensornavigator.o: CXXFLAGS+= $(PLUGINFLAGS)

sensors/s7plc/%o: CXXFLAGS+= -std=c++14
sensors/snmp/%o: CXXFLAGS+= -std=c++14

libdcdbplugin_sysfs.$(LIBEXT): sensors/sysfs/SysfsSensorGroup.o sensors/sysfs/SysfsConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lboost_regex

libdcdbplugin_perfevent.$(LIBEXT): sensors/perfevent/PerfSensorGroup.o sensors/perfevent/PerfeventConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lstdc++fs

libdcdbplugin_ipmi.$(LIBEXT): sensors/ipmi/IPMISensorGroup.o sensors/ipmi/IPMIHost.o sensors/ipmi/IPMIConfigurator.o sensors/ipmi/LenovoXCC.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lboost_regex -lfreeipmi

libdcdbplugin_pdu.$(LIBEXT): sensors/pdu/PDUSensorGroup.o sensors/pdu/PDUUnit.o sensors/pdu/PDUConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -L$(OPENSSL_LIB) -lcrypto -lssl -lboost_log -lboost_system

libdcdbplugin_bacnet.$(LIBEXT): sensors/bacnet/BACnetSensorGroup.o sensors/bacnet/BACnetClient.o sensors/bacnet/BACnetConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lboost_thread -lbacnet-stack

libdcdbplugin_snmp.$(LIBEXT): sensors/snmp/SNMPSensorGroup.o sensors/snmp/SNMPConnection.o sensors/snmp/SNMPConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lnetsnmp -lnetsnmpagent

libdcdbplugin_procfs.$(LIBEXT): sensors/procfs/ProcfsSensorGroup.o sensors/procfs/ProcfsParser.o sensors/procfs/ProcfsConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lboost_regex

libdcdbplugin_tester.$(LIBEXT): sensors/tester/TesterSensorGroup.o sensors/tester/TesterConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system

libdcdbplugin_gpfsmon.$(LIBEXT): sensors/gpfsmon/GpfsmonSensorGroup.o sensors/gpfsmon/GpfsmonConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system

libdcdbplugin_opa.$(LIBEXT): sensors/opa/OpaSensorGroup.o sensors/opa/OpaConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -L$(OPENSSL_LIB) -lboost_log -lboost_system -lopamgt -lssl

libdcdbplugin_msr.$(LIBEXT): sensors/msr/MSRSensorGroup.o sensors/msr/MSRConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system

libdcdbplugin_caliper.$(LIBEXT): sensors/caliper/CaliperSensorGroup.o sensors/caliper/CaliperConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system

libdcdbplugin_nvml.$(LIBEXT): sensors/nvml/nvmlSensorGroup.o sensors/nvml/nvmlConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib -lboost_log -lboost_system -lnvidia-ml

libdcdbplugin_s7plc.$(LIBEXT): sensors/s7plc/s7plcSensorGroup.o sensors/s7plc/s7plcController.o sensors/s7plc/s7plcConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lsnap7

libdcdbplugin_modbus.$(LIBEXT): sensors/modbus/modbusSensorGroup.o sensors/modbus/modbusServer.o sensors/modbus/modbusConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lmodbus

libdcdbplugin_rest.$(LIBEXT): sensors/rest/RESTSensorGroup.o sensors/rest/RESTUnit.o sensors/rest/RESTConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -L$(DCDBSRCPATH)/lib -L$(OPENSSL_LIB) -lcrypto -lssl -lboost_regex -lboost_log -lboost_system -ldcdb

libdcdbplugin_sysman.$(LIBEXT): sensors/sysman/SysmanSensorGroup.o sensors/sysman/SysmanConfigurator.o
	$(CXX) -I$(INTEL_ZE_PATH)/include/level_zero -L$(INTEL_ZE_PATH)/lib64 $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lze_loader

libdcdbplugin_ear.$(LIBEXT): sensors/ear/earSensorGroup.o sensors/ear/earConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system

libdcdbplugin_variorum.$(LIBEXT): sensors/variorum/VariorumSensorGroup.o sensors/variorum/VariorumConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -lvariorum -lhwloc -ljansson

libdcdbplugin_likwid.$(LIBEXT): sensors/likwid/likwidSensorGroup.o sensors/likwid/likwidConfigurator.o
	$(CXX) $(LIBFLAGS)$@ -o $@ $^ -L$(DCDBDEPLOYPATH)/lib/ -lboost_log -lboost_system -llikwid
