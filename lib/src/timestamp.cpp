//================================================================================
// Name        : timestamp.cpp
// Author      : Axel Auweter, Michael Ott
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : C++ API for handling time stamps in libdcdb.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2011-2019 Leibniz Supercomputing Centre
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//================================================================================

#include <cinttypes>
#include <cstring>

#include "boost/date_time/c_local_time_adjustor.hpp"
#include "boost/date_time/local_time/local_time.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/regex.hpp"

#include "dcdb/timestamp.h"

using namespace DCDB;

/* Use this as our local to utc adjustor */
typedef boost::date_time::c_local_adjustor<boost::posix_time::ptime> local_adj;

/**
 * This function tries to guess a time stamp from a UNIX-like uint64 in s, ms, ns, or us
 */

void TimeStamp::guessFromUint(uint64_t ts) {
    /* Checker whether it is a date before 2070-12-31 */
    if (ts < 31872923400ull) { // s
	    raw = ts * 1000 * 1000 * 1000;
    } else if (ts < 31872923400000ull) { // ms
	    raw = ts * 1000 * 1000;
    } else if (ts < 31872923400000000ull) { // ns
	    raw = ts * 1000;
    } else { // us
	    raw = ts;
    }
}

/**
 * This function parses a string and tries to do a best guess at the contained
 * time information. Currently, it detects strings in the format "yyyy-mm-dd hh:mm:ss.000"
 * and posix time.
 */
void TimeStamp::guessFromString(std::string timestr, bool localTime) {
	boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
	uint64_t                 tmp;
	boost::posix_time::ptime t;

	/*
	 * Try to match it against a string containing "now".
	 * Note that we ignore the localTime flag in this case since
	 * we already get "now" in UTC.
	 */
	if (timestr.find("now") != std::string::npos) {

		/* If the string is just "now"... */
		if ((timestr.compare("now") == 0) && (timestr.length() == 3)) {
			setNow();
			return;
		}

		/* If the string is of the form "now-X" */
		boost::regex d("now-[0-9]*d", boost::regex::extended);
		boost::regex h("now-[0-9]*h", boost::regex::extended);
		boost::regex m("now-[0-9]*m", boost::regex::extended);
		boost::regex s("now-[0-9]*s", boost::regex::extended);

		if (boost::regex_match(timestr, d)) {
			sscanf(timestr.c_str(), "now-%" PRIu64, &tmp);
			setNow();
			raw -= tmp * 24 * 60 * 60 * 1000 * 1000 * 1000;
			return;
		}
		if (boost::regex_match(timestr, h)) {
			sscanf(timestr.c_str(), "now-%" PRIu64, &tmp);
			setNow();
			raw -= tmp * 60 * 60 * 1000 * 1000 * 1000;
			return;
		}
		if (boost::regex_match(timestr, m)) {
			sscanf(timestr.c_str(), "now-%" PRIu64, &tmp);
			setNow();
			raw -= tmp * 60 * 1000 * 1000 * 1000;
			return;
		}
		if (boost::regex_match(timestr, s)) {
			sscanf(timestr.c_str(), "now-%" PRIu64, &tmp);
			setNow();
			raw -= tmp * 1000 * 1000 * 1000;
			return;
		}

		/* "now" keyword is in the timestamp but does not match one of the predefined formats */
		throw TimeStampConversionException();
	}

	/* Try to match it against a POSIX time */
	if ((timestr.find_first_not_of("0123456789") == std::string::npos) && (sscanf(timestr.c_str(), "%" PRIu64, &tmp) == 1)) {
		guessFromUint(tmp);
		return;
	}

	/* First try to match it against a time string */
	const std::locale formats[] = {
	    std::locale(std::locale::classic(), new boost::posix_time::time_input_facet("%Y%m%dT%H%M%S%F")),
	    std::locale(std::locale::classic(), new boost::posix_time::time_input_facet("%Y-%m-%d %H:%M:%S%F")),
	    std::locale(std::locale::classic(), new boost::posix_time::time_input_facet("%Y-%m-%dT%H:%M:%S%F")),
	    std::locale(std::locale::classic(), new boost::posix_time::time_input_facet("%H:%M:%S%F")),
	};
	for (size_t i = 0; i < sizeof(formats) / sizeof(formats[0]); ++i) {
		std::istringstream is(timestr);
		is.imbue(formats[i]);
		is >> t;
		if (t != boost::posix_time::not_a_date_time) {
			if (t.date() == boost::gregorian::date(1400, 1, 1)) {
				if (localTime) {
					t = boost::posix_time::ptime(boost::gregorian::day_clock::local_day(), t.time_of_day());
				} else {
					t = boost::posix_time::ptime(boost::gregorian::day_clock::universal_day(), t.time_of_day());
				}
			}
			break;
		}
	}

	if (t != boost::posix_time::not_a_date_time) {
		if (localTime) {
			/*
			 * Unfortunately BOOST c_local_adjustor doesn't support local-to-utc conversion.^
			 * Therefore, we first check the offset of t during utc-to-local conversion
			 * and then apply this offset in the other direction.
			 */

			/* Convert t to UTC */
			boost::posix_time::ptime t2 = local_adj::utc_to_local(t);

			/* Check the difference of the two times */
			boost::posix_time::time_duration diff = t - t2;

			/* Write back adjusted value to raw */
			t += diff;
		}
		boost::posix_time::time_duration diff = t - epoch;
		raw = diff.total_nanoseconds();
		return;
	}

	/* Not successful - throw exception */
	throw TimeStampConversionException();
}

/**
 * This constructor is implemented by calling setNow().
 */
TimeStamp::TimeStamp() {
	setNow();
}


/**
 * This constructor initializes the object from an UNIX-like time stamp.
 */
TimeStamp::TimeStamp(uint64_t ts) {
	guessFromUint(ts);
}

/**
 * This constructor sets the time using the magic implemented in guessFromString.
 */
TimeStamp::TimeStamp(std::string ts, bool localTime) {
	guessFromString(ts, localTime);
}

/**
 * Currently, the destructor doesn't need to do anything.
 */
TimeStamp::~TimeStamp() {}

/**
 * This function sets the value of raw to the nanoseconds since epoch
 * using some magic help of boost::posix_time.
 */
void TimeStamp::setNow(void) {
	boost::posix_time::ptime         epoch(boost::gregorian::date(1970, 1, 1));
	boost::posix_time::ptime         now = boost::posix_time::microsec_clock::universal_time();
	boost::posix_time::time_duration diff = now - epoch;

	raw = diff.total_nanoseconds();
}

/**
 * Return the raw time value (nanoseconds since Unix epoch).
 */
uint64_t TimeStamp::getRaw(void) const {
	return raw;
}

/**
 * Return the current value as string.
 */
std::string TimeStamp::getString(bool localTime) const {
#ifndef BOOST_DATE_TIME_HAS_NANOSECONDS
#error Needs nanoseconds support in boost.
#endif
	boost::posix_time::ptime t(boost::gregorian::date(1970, 1, 1));
	t += boost::posix_time::nanoseconds(raw);
	if (localTime) {
		t = local_adj::utc_to_local(t);
	}
	std::string timeStr = boost::posix_time::to_iso_extended_string(t);
	if (raw % NS_PER_S == 0) {
		return timeStr + ".000000000";
	} else {
		return timeStr;
	}
}

/**
 * Return the "weekstamp" of the current value.
 */
uint16_t TimeStamp::getWeekstamp(void) const {
	uint16_t week = raw / 604800000000000;
	return week;
}
