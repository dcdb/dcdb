//================================================================================
// Name        : sensor.cpp
// Author      : Michael Ott, Daniele Tafani
// Contact     : info@dcdb.it
// Copyright   : Leibniz Supercomputing Centre
// Description : Source file for Sensor class.
//================================================================================

//================================================================================
// This file is part of DCDB (DataCenter DataBase)
// Copyright (C) 2017-2019 Leibniz Supercomputing Centre
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//================================================================================

#include <algorithm>
#include <dcdb/sensor.h>
#include <dcdb/virtualsensor.h>
#include <functional>
#include <iostream>
#include <numeric>

namespace DCDB {

Sensor::Sensor(Connection *connection, const std::string &publicName) {
	/* Initialize the SensorConfig interface */
	sensorConfig = new SensorConfig(connection);

	this->connection = connection;

	/* Retrieve publicSensor info */
	switch (sensorConfig->getPublicSensorByName(publicSensor, publicName.c_str())) {
		case DCDB::SC_OK:
			break;
		case DCDB::SC_INVALIDSESSION:
			std::cout << "Invalid session." << std::endl;
			return;
		case DCDB::SC_UNKNOWNSENSOR:
			std::cout << "Unknown sensor: " << publicName << std::endl;
			return;
		default:
			std::cout << "Unknown error." << std::endl;
			return;
	}
}

Sensor::Sensor(Connection *connection, const PublicSensor &sensor) {
	/* Initialize the SensorConfig interface */
	sensorConfig = new SensorConfig(connection);

	this->connection = connection;
	publicSensor = sensor;
}

Sensor::~Sensor() {
	delete sensorConfig;
}

void Sensor::query(std::list<SensorDataStoreReading> &result, TimeStamp start, TimeStamp end, QueryAggregate aggregate, int64_t interval_ns, uint64_t tol_ns) {
	SensorDataStore sensorDataStore(connection);

	if (start.getRaw() == end.getRaw()) {
		SensorId sid(publicSensor.name);
		sid.setRsvd(start.getWeekstamp());
		sensorDataStore.fuzzyQuery(result, sid, start, tol_ns);
		return;
	}

	if (interval_ns == 0) {
		interval_ns = end.getRaw() - start.getRaw();
	}

	TimeStamp intervalStart = start;
	TimeStamp intervalEnd = TimeStamp(start.getRaw() + interval_ns);
	while (intervalEnd <= end) {
		std::list<SensorDataStoreReading> tempResult;

		if (publicSensor.is_virtual) {
			VSensor vSen(connection, publicSensor);
			vSen.query(tempResult, intervalStart, intervalEnd);
		} else {
			uint16_t wsStart = intervalStart.getWeekstamp();
			uint16_t wsEnd = intervalEnd.getWeekstamp();
			SensorId sid(publicSensor.name);
#ifndef SENSORDATA_ASC
			for (uint16_t ws = wsEnd; ws >= wsStart; ws--)
#else
			for (uint16_t ws = wsStart; ws <= wsEnd; ws++)
#endif
			{
				sid.setRsvd(ws);
				sensorDataStore.query(tempResult, sid, intervalStart, intervalEnd, aggregate);
			}
		}

		if ((tempResult.size() > 1) && (aggregate != AGGREGATE_NONE)) {
#ifndef SENSORDATA_ASC
			SensorDataStoreReading reading = tempResult.front();
#else
			SensorDataStoreReading reading = tempResult.back();
#endif
			switch (aggregate) {
				case AGGREGATE_MIN:
					reading.value = std::min_element(tempResult.begin(), tempResult.end())->value;
					;
					break;
				case AGGREGATE_MAX:
					reading.value = std::max_element(tempResult.begin(), tempResult.end())->value;
					;
					break;
				case AGGREGATE_AVG: {
					int64_t sum = 0;
					for (auto r : tempResult) {
						sum += r.value;
					}
					reading.value = sum / tempResult.size();
					break;
				}
				case AGGREGATE_SUM: {
					int64_t sum = 0;
					for (auto r : tempResult) {
						sum += r.value;
					}
					reading.value = sum;
					break;
				}
				case AGGREGATE_COUNT:
					reading.value = tempResult.size();
					break;
				default:
					std::cout << "Unknown aggregate " << aggregate << " in Sensor::query()" << std::endl;
					break;
			}
			result.push_back(reading);
		} else {
			result.splice(result.end(), tempResult);
		}

		intervalStart = TimeStamp(intervalEnd.getRaw() + 1);
		intervalEnd = TimeStamp(intervalEnd.getRaw() + interval_ns);
	}
}
} /* namespace DCDB */
